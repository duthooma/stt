Guidelines for commits:

Each commit should have the following structure and information:

+ [new module/feature name] Details
- [module name] Details
* [Bug id][module name/key word] Details
! Notes
!R Reviewer's Name : notes


Adding Module/Feature:

+ [module] Details

When you add a new module or feature, you should add the "+" symbol and between [...] the name of that module, followed by the description.

Example: + [Feed] Adding feed fragment to navigation menu. More description data...


Modification:

- [module name] Details

When you just do a modification, you should have to add the "-" symbol and between [...] the name of that module, followed by the description.

Example: - [Challenges] Challenges layout changed for feed. More description data...


Buf Fix:

* [Bug id] Details

If a bug fix is done, you have to write "*" followed by the module or a key word that describes the main issue. In case there a bug data base, you should write between brackets the bug Id.

Examples: 	* [521][sound system] No sound in main menu is fixed.
			* [crash] Crash after any interruption is fixed. 


Notes:

! Notes

In case you want to add a note about the commit or code, you should write the note after of "!" symbol.

Example: ! This module is in debug mode...


Code Review:

!R Reviewer's Name : notes

If there is a code review before doing the commit, you have to write the name of the reviewer after of "!R" and in case there is some note, it should be written before ":".

Example: !R Maxence : It's pending to clean the Invite Friends Activity.
