
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});




//Feed cloud implementation 
//Create/Modify/Delete Feed Object

Parse.Cloud.afterSave("Training", function(request, response) {

    var Feed = Parse.Object.extend("Feed");
    var feed = new Feed();

    feed.set("referenceName", "Training");
    feed.set("trainingReference", request.object);
    feed.set("userReference", request.object.get("userReference"));
    feed.save();
});

Parse.Cloud.afterSave("Challenge", function(request, response) {


   // destroyin previous objects
    query = new Parse.Query("Feed");
    query.equalTo("challengeReference", request.object);
    query.find().then(function(results) {
        for (var i = 0; i < results.length; ++i) {
            results[i].destroy();
        }
        //creating new ones
        var Feed = Parse.Object.extend("Feed");
        var feed = new Feed();

        feed.set("referenceName", "Challenge");
        feed.set("challengeReference", request.object);
        feed.set("userReference", request.object.get("user_from"));
        feed.save();
    });

    var GCM_API_KEY, userReference, message;
    
    GCM_API_KEY = "AIzaSyA0sQaAZCF2Fl_0v9HEwWcg5ktELU2EU6w";

    userReference = request.object.get("user_to");
    message = request.object.get("description");

    userReference.fetch().then(function(userObj) {
        var gcmId = userObj.get('gcmId');
        var userName = userObj.get('username');
        //we have to sen the GCM id in array form
        var gcmRegIds = [gcmId];
        if(gcmId)
        {
            //request to Google Cloud Messaging
            Parse.Cloud.httpRequest({
              method: 'POST',
              url: 'https://android.googleapis.com/gcm/send',
              headers: {
                'Authorization': 'key=' + GCM_API_KEY,
                'Content-Type': 'application/json'
              },
              body: {
                registration_ids: gcmRegIds,
                data: {
                    'type': 'challenge',
                    'notification': 1,
                    'message': message
                }
              },
              success: function(httpResponse) {
                console.log('GCM message success to '+userName);
              },
              error: function(httpResponse) {
                console.error('GCM Request failed' + JSON.stringify(httpResponse));
              }
            });
       } 
    }).then(
        function () {
            // fetch() & Push were successful
        },
        function (error) {
            console.log(error);
        }
    );
});



//Delete the feed related objects
Parse.Cloud.afterDelete("Training", function(request, response) {
    query = new Parse.Query("Feed");
    query.equalTo("trainingReference", request.object);

    query.find().then(function(results) {
        for (var i = 0; i < results.length; ++i) {
            results[i].destroy();
    }
    });
});

//Delete the feed related objects
Parse.Cloud.afterDelete("Challenge", function(request, response) {
    query = new Parse.Query("Feed");
    query.equalTo("challengeReference", request.object);

    query.find().then(function(results) {
        for (var i = 0; i < results.length; ++i) {
            results[i].destroy();
    }
    });
});





Parse.Cloud.job("testAddTraining", function(request, status) {

	// Set up to modify user data
	Parse.Cloud.useMasterKey();

	var GameScore = Parse.Object.extend("GameScore");
	var gameScore = new GameScore();

	gameScore.set("score", 1337);
	gameScore.set("playerName", "Sean Plott");
	gameScore.set("cheatMode", false);

	gameScore.save(null, {
		success: function(gameScore) {
		// Execute any logic that should take place after the object is saved.
			alert('New object created with objectId: ' + gameScore.id);
		},
		error: function(gameScore, error) {
		// Execute any logic that should take place if the save fails.
		// error is a Parse.Error with an error code and message.
			alert('Failed to create new object, with error code: ' + error.message);
		}
		}).then(function() {
			// Set the job's success status
			status.success("Adding training success");
		}, function(error) {
		// Set the job's error status
		status.error("Uh oh, something went wrong while adding a training");
		})
});

Parse.Cloud.job("ExpireChallenge", function(request, status) {
	// Set up to modify user data
	Parse.Cloud.useMasterKey();
    
    var ChallengeClass = Parse.Object.extend("Challenge");
    var query = new Parse.Query(ChallengeClass);
    
    var d = new Date();
    var now = d.getTime();
    
    query.notEqualTo ("result", 2);
    query.lessThan("end_date",now);
    query.find({
      success: function(results) {
        
          alert('Yeah! We found challenges : ' + results.length);
          
        // Do something with the returned Parse.Object values
        for (var i = 0; i < results.length; i++) { 
            var challenge = results[i];
            challenge.set("result",2);
            
        }
        Parse.Object.saveAll(results);
      },
      error: function(error) {
        alert('Failed.. We couldnt find challenge' + error.code + ' :: ' + error.message);
      }
    }).then(function() {
			// Set the job's success status
			status.success("Challenges set as Expired");
		}, function(error) {
		// Set the job's error status
		      status.error("sh**.. challenges not set as expired..." + error.code + ' :: ' + error.message);
		});   
});

/*

        Calendar cal = Calendar.getInstance();

        ParseQuery<Challenge> challengesQuery = ParseQuery.getQuery(Challenge.class);
        challengesQuery.whereLessThan(Challenge.PARSEFIELD_END_DATE, cal.getTime().getTime());
        challengesQuery.whereNotEqualTo(Challenge.PARSEFIELD_RESULT, Challenge.CHALLENGE_RESULT_EXPIRED);

        final List<Challenge> challengesList = new ArrayList<>();
        challengesQuery.findInBackground(new FindCallback<Challenge>() {
            @Override
            public void done(List<Challenge> list, ParseException e) {
                challengesList.addAll(list);
                for (Challenge c : challengesList){
                    c.setResult(Challenge.CHALLENGE_RESULT_EXPIRED);
                }
                ParseObject.saveAllInBackground(challengesList, new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Log.e(Constants.LOG_TAG,"success of SaveCallBack");
                    }
                });
                Log.e(Constants.LOG_TAG,"expired Challenge size() == " + list.size());
            }
        });

*/