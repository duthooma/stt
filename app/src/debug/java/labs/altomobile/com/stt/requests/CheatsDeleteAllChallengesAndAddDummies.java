package labs.altomobile.com.stt.requests;

import android.util.Log;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.common.DateUtil;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.DummiesConstants;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnSaveCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by maxence on 4/22/15.
 */
public class CheatsDeleteAllChallengesAndAddDummies extends ParseRequest<Challenge> {

    @Override
    protected void request(final List<Challenge> challenges, final ParseObjectMap parseObjectMap) {

        Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: request()");

        ParseQuery<Challenge> challengeParseQuery = ParseQuery.getQuery(Challenge.class);
        challenges.addAll(request(challengeParseQuery));


        ParseObject.deleteAllInBackground(request(challengeParseQuery), new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                Log.w(STTConstants.LOG_TAG, "DeleteAllChallengesAndAddDummies :: request() :: done");


                if (e == null) {

                    Log.w(STTConstants.LOG_TAG, "DeleteAllChallengesAndAddDummies :: request() :: done :: e == null");

                    addChallengesDummies();
                } else {
                    Log.w(STTConstants.LOG_TAG, "DeleteAllChallengesAndAddDummies :: request() :: done :: e != null : " + e.getMessage());
                }
            }
        });

    }

    private void addChallengesDummies(){

        Log.w(STTConstants.LOG_TAG, "DeleteAllChallengesAndAddDummies :: addChallengesDummies()");


        long dateInAWeek = new DateUtil().removeDays(-7).getTime().getTime();
        long dateIn2Weeks = new DateUtil().removeDays(-14).getTime().getTime();
        long dateIn3WeeksAnd2Days = new DateUtil().removeDays(-23).getTime().getTime();
        long dateTomorrow= new DateUtil().removeDays(-1).getTime().getTime();
        long dateYesterday= new DateUtil().removeDays(1).getTime().getTime();
        long dateTodayIn2Hours= new DateUtil().addMinutes(120).getTime().getTime();
        long dateTodayIn20Hours= new DateUtil().addMinutes(1200).getTime().getTime();


        //Flavio to Daniel
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_DANIEL, 1, "Going to fight against Pacquiao", "because you can do it man! ", dateInAWeek);
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_DANIEL, 2, "Going to fight against MayWeather", "You gonna win both heee! ", dateIn2Weeks);
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_DANIEL, 4, "And finally gonna fight againt Mike Tyson!", "You rockkkk! ", dateIn3WeeksAnd2Days);

        //Flavio to Max
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_MAX, 1, "Going to fight against Pacquiao", "because you can do it man2! ", dateInAWeek);
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_MAX, 3, "Going to fight against MayWeather", "You gonna win both heee2! ", dateIn2Weeks);
        createAChallenge(DummiesConstants.USERID_FLAVIO,DummiesConstants.USERID_MAX, 5, "And finally gonna fight againt Mike Tyson!", "You rockkkk2! ", dateIn3WeeksAnd2Days);

        //Daniel to Flavio
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_FLAVIO, 2, "Going to fight against Pacquiao", "because you can do it man3! ", dateInAWeek);
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_FLAVIO, 3, "Going to fight against MayWeather", "You gonna win both heee3! ", dateIn2Weeks);
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_FLAVIO, 4, "And finally gonna fight againt Mike Tyson!", "You rockkkk3! ", dateIn3WeeksAnd2Days);


        //Daniel to Max
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_MAX, 1, "Daniel to Max", "Daniel to Max DESC 1", dateTomorrow);
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_MAX, 2, "Daniel to Max", "Daniel to Max DESC 2", dateTodayIn2Hours);
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_MAX, 3, "Daniel to Max", "Daniel to Max DESC 3", dateTodayIn20Hours);
        createAChallenge(DummiesConstants.USERID_DANIEL,DummiesConstants.USERID_MAX, 3, "Daniel to Max", "YESTERDAY", dateYesterday);
    }

    private void createAChallenge(String fromS, String toS, int sportId, String title, String desc, long date){
        ParseUser from = ParseObject.createWithoutData(ParseUser.class,fromS);
        ParseUser to = ParseObject.createWithoutData(ParseUser.class,toS);
        Challenge challenge = new Challenge(from,to,sportId,title,desc,date, Challenge.NO_REWARD, "comment placeholder");
        ParseHelper parseHelper = new ParseHelper();
        parseHelper.saveChallenge(challenge,new OnSaveCallback<Challenge>() {
            @Override
            public void onSaveObject(Challenge challenge) {

            }
        });
    }
}

