package labs.altomobile.com.stt.requests;

import android.util.Log;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.DummiesConstants;
import labs.altomobile.com.stt.helpers.FriendRequestHelper;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by maxence on 4/22/15.
 */
public class CheatsDeleteAllFriendRequestsAndAddDummies extends ParseRequest<FriendRequest> {

    @Override
    protected void request(final List<FriendRequest> friendRequests, final ParseObjectMap parseObjectMap) {

        Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: request()");

        ParseQuery<FriendRequest> friendRequestParseQuery = ParseQuery.getQuery(FriendRequest.class);
        friendRequests.addAll(request(friendRequestParseQuery));


        ParseObject.deleteAllInBackground(request(friendRequestParseQuery), new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: request() :: done");


                if (e == null) {

                    Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: request() :: done :: e == null");

                    addFriendRequestsDummies();
                } else {
                    Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: request() :: done :: e != null : " + e.getMessage());
                }
            }
        });

    }

    private void addFriendRequestsDummies(){

        Log.w(STTConstants.LOG_TAG, "DeleteAllFriendRequestsAndAddDummies :: addFriendRequestsDummies()");

        FriendRequestHelper friendRequestHelper = new FriendRequestHelper();
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_MAX, DummiesConstants.FB_ID_DANIEL);
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_MAX, DummiesConstants.FB_ID_FLAVIO);
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_MAX, DummiesConstants.FB_ID_MAX2);
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_DANIEL, DummiesConstants.FB_ID_MAX2);
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_DANIEL, DummiesConstants.FB_ID_FLAVIO);
        friendRequestHelper.sendFriendRequestAutoAccepted(DummiesConstants.FB_ID_MAX2, DummiesConstants.FB_ID_FLAVIO);

    }
}

