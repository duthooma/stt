package labs.altomobile.com.stt.utils;

/**
 * Created by danielmonterocervantes on 04/06/15.
 */
public class BuildInfo {
    public static boolean isDebug()
    {
        return  true;
    }
    public static  boolean isRelease()
    {
        return !isDebug();
    }
}
