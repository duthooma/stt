package labs.altomobile.com.stt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.bumptech.glide.request.target.ViewTarget;
import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import labs.altomobile.com.stt.activities.MainMenuActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.Notification;
import labs.altomobile.com.stt.objects.Place;
import labs.altomobile.com.stt.objects.Feed;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.objects.UserLevel;
import labs.altomobile.com.stt.objects.Comment;
import labs.altomobile.com.stt.services.RegistrationIntentService;
import labs.altomobile.com.stt.utils.BuildInfo;

public class STTApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        //Needed to support Glide and setTag :http://stackoverflow.com/questions/34833627/error-you-must-not-call-settag-on-a-view-glide-is-targeting-when-use-glide/35096552#35096552
        ViewTarget.setTagId(R.id.glide_tag);

        if(STTConstants.USE_FIREBASE) {
            Firebase.setAndroidContext(this);
        } else {

            // Enable Local Datastore.
            Parse.enableLocalDatastore(this);
            ParseObject.registerSubclass(Place.class);
            ParseObject.registerSubclass(Training.class);
            ParseObject.registerSubclass(Feed.class);
            ParseObject.registerSubclass(Group.class);
            ParseObject.registerSubclass(FriendRequest.class);
            ParseObject.registerSubclass(GroupVsUser.class);
            ParseObject.registerSubclass(Challenge.class);
            ParseObject.registerSubclass(UserLevel.class);
            ParseObject.registerSubclass(Comment.class);
            ParseObject.registerSubclass(Notification.class);


            if (BuildInfo.isDebug()) {
                Parse.initialize(this, "PZid04frw7DQqQgvEfY7vsL2BpzN0qPMrLIP0nf9", "vA8CGr4wIoHf1Hc12grfsJjR7sZfgzYoEzrWKvgb");
            } else {

                Parse.initialize(this, "aHtPbt2S5XYi4ThxtEgDULA5QF8BH22GfKcd7CIK", "QfOjNvCvRyukYM6w5QJzBoAUCQ7WmBObSCpKQsaQ");

            }
            ParseInstallation.getCurrentInstallation().saveInBackground();

            ParseFacebookUtils.initialize(this);
        }

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        if(!is_GCM_IdSent())
        {
            Log.d(STTConstants.LOG_TAG, "no token for GCM sent ");
            startService(new Intent(this, RegistrationIntentService.class));
        }

        //detect if this is the first time the user launches the app
        /*final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        if (settings.getBoolean("first_time_launch", true)) {
            //the app is being launched for first time, do something

            // first time task
            addShortcutIcon();
            // record the fact that the app has been started at least once
            settings.edit().putBoolean("first_time_launch", false).commit();
        }*/
    }

    private boolean is_GCM_IdSent()
    {
        SharedPreferences sharedpreferences = getSharedPreferences(STTConstants.FILE_NAME_GCM_CONFIG, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(STTConstants.GCM_ID))
        {
            return true;
        }
        return false;
    }

    private void addShortcutIcon() {
        //shorcutIntent object
        Intent shortcutIntent = new Intent(getApplicationContext(), MainMenuActivity.class);

        shortcutIntent.setAction(Intent.ACTION_MAIN);
        //shortcutIntent is added with addIntent
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                        R.mipmap.ic_launcher));

        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        // finally broadcast the new Intent
        getApplicationContext().sendBroadcast(addIntent);
    }
}
