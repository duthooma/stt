package labs.altomobile.com.stt.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.bumptech.glide.Glide;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.common.DateUtil;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnSaveCallback;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.utils.STTUtils;

public class AddTrainingSelectDateActivity extends AppCompatActivity implements
    OnSaveCallback<Training>, View.OnClickListener, Firebase.CompletionListener {

    private TypedArray mSportsIconIds;

    private ImageView imageView;
    private RadioButton mTodayBtn;
    private RadioButton mYesterdayBtn;
    private RadioButton mCustomDayBtn;
    private Button mAddTrainingBtn;
    SweetAlertDialog mProgressDialog;
    private DatePickerDialog mCustomDatePickerDialog;
    private SimpleDateFormat mSdf;

    private String mPlaceId = "-1";
    private Date mTrainingDate = null;
    private int mSportId = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_training_select_date);

        // Inflate your custom layout
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.actionbar_custom_one_button,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        TextView actionBarTitleTextView = (TextView)findViewById(R.id.action_bar_custom_title);
        actionBarTitleTextView.setText(getString(R.string.title_activity_add_training));

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            mSportsIconIds = getResources().obtainTypedArray(R.array.sportIconIds);

            mSportId = bundle.getInt(AddTrainingSelectSportActivity.TRAINED_SPORT_SELECTED, -1);

            if (mSportId > -1) {

                imageView = (ImageView) findViewById(R.id.imageView_sport_ic);

                Glide.with(this).load(mSportsIconIds.getResourceId(mSportId, -1)).into(imageView);

                mTodayBtn = (RadioButton) findViewById(R.id.button_today);
                mYesterdayBtn = (RadioButton) findViewById(R.id.button_yesterday);
                mCustomDayBtn = (RadioButton) findViewById(R.id.button_custom_date);
                mAddTrainingBtn = (Button) findViewById(R.id.action_bar_action_button);

                mTodayBtn.setOnClickListener(this);
                mYesterdayBtn.setOnClickListener(this);
                mCustomDayBtn.setOnClickListener(this);
                mAddTrainingBtn.setOnClickListener(this);

                mTodayBtn.setChecked(true);
                Calendar cal = Calendar.getInstance();
                mTrainingDate = cal.getTime();

                mSdf = new SimpleDateFormat("MMM dd", Locale.US);

                Calendar newCalendar = Calendar.getInstance();
                mCustomDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        mCustomDayBtn.setText(mSdf.format(newDate.getTime()));
                        mTrainingDate = newDate.getTime();
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                mCustomDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());

                //datePicker is limited: last 30days only
                mCustomDatePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTime().getTime() - 30L * 24L * 3600L * 1000L);

            } else {

                Log.w(STTConstants.LOG_TAG, "WARNING :: AddTrainingActivity :: onActivityResult :: sportId == " + mSportId);

                STTUtils.showSomethingWentWrongAlert(this);

            }
        }
    }

    @Override
    public void onSaveObject(Training training) {

        if(training==null) {
            mProgressDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            mProgressDialog.setTitleText(getString(R.string.something_went_wrong));
            mProgressDialog.showCancelButton(true);
            mProgressDialog.setCancelText(getString(R.string.ok));
            Log.e(STTConstants.LOG_TAG, "we couldn't update to Cloud the Training for User [" + ParseUser.getCurrentUser().getObjectId() + "] and Sport [" + mSportId + "] and Place [" + mPlaceId + "]");
        }
        else
        {
            mProgressDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            mProgressDialog.setTitleText(getString(R.string.saved));
            mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Log.i(STTConstants.LOG_TAG, "We successfully added the Training for User [" + ParseUser.getCurrentUser().getObjectId() + "] and Sport [" + mSportId + "] and Place [" + mPlaceId + "]");
                    Intent intent = new Intent(AddTrainingSelectDateActivity.this, MainMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    mProgressDialog.dismiss();
                }

            }, 1300);

        }
    }

    @Override
    public void onClick(View v) {

        if(v == mTodayBtn){

            Calendar cal = Calendar.getInstance();
            mTrainingDate = cal.getTime();

        }else if(v == mYesterdayBtn){

            DateUtil dateUtil = new DateUtil();
            mTrainingDate = dateUtil.removeDays(1).getTime();

        }else if(v == mCustomDayBtn){

            mCustomDatePickerDialog.show();
        }else if(v == mAddTrainingBtn){
            if(mTrainingDate != null){
                //add training to cloud
                if(!STTConstants.USE_FIREBASE)
                Log.i(STTConstants.LOG_TAG, "AddTrainingActivity :: saving the training " +
                        "for User [" + ParseUser.getCurrentUser().getObjectId() + "] and Sport [" + mSportId + "] and Place [" + mPlaceId + "]");

                mProgressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                mProgressDialog.getProgressHelper().setBarColor(R.color.colorPrimary);
                mProgressDialog.setTitleText(getString(R.string.saving));
                mProgressDialog.setCancelable(false);
                mProgressDialog.showCancelButton(false);
                mProgressDialog.show();

                if(STTConstants.USE_FIREBASE)
                    FirebaseHelper.createTraining(mSportId,mTrainingDate.getTime(),mPlaceId,this);
                else
                    ParseHelper.createTrainingInBackground(mSportId, mTrainingDate.getTime(), mPlaceId, this);
            }else{
                //display error feedback to user
                showError(mTodayBtn);
            }
        }/*else if(v == mButtonAddPlace){
        Intent i = new Intent(AddTrainingActivity.this, MyPlacesActivity.class);
        i.putExtra("selectMode", true);
        startActivityForResult(i, SELECT_TRAINED_PLACE_REQUEST);
    }*/
    }

    private void showError(Button button) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        button.startAnimation(shake); }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
        if(firebaseError!=null) {
            mProgressDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            mProgressDialog.setTitleText(getString(R.string.something_went_wrong));
            mProgressDialog.showCancelButton(true);
            mProgressDialog.setCancelText(getString(R.string.ok));
            Log.e(STTConstants.LOG_TAG, "we couldn't update to Cloud the Training for User [" + FirebaseHelper.getUserUid() + "] and Sport [" + mSportId + "] and Place [" + mPlaceId + "]");
        }
        else
        {
            mProgressDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            mProgressDialog.setTitleText(getString(R.string.saved));
            mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Log.i(STTConstants.LOG_TAG, "We successfully added the Training for User [" + FirebaseHelper.getUserUid() + "] and Sport [" + mSportId + "] and Place [" + mPlaceId + "]");
                    Intent intent = new Intent(AddTrainingSelectDateActivity.this, MainMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    mProgressDialog.dismiss();
                }

            }, 1300);

        }
    }
}