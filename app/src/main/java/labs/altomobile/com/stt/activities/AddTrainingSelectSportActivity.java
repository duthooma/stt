package labs.altomobile.com.stt.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.adapters.SportGridAdapter;

public class AddTrainingSelectSportActivity extends AppCompatActivity {

    static final String TRAINED_SPORT_SELECTED = "TRAINED_SPORT_SELECTED";
    private SportGridAdapter adapter;

    private GridView gridview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_trained_sport);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        gridview = (GridView) findViewById(R.id.gridview_sports);
        adapter = new SportGridAdapter(this, R.layout.grid_sport_item, sportClickListener);
        gridview.setAdapter(adapter);

    }

    View.OnClickListener sportClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int sportSelectedIndex = (Integer)v.getTag();
            Intent intent = new Intent(AddTrainingSelectSportActivity.this,AddTrainingSelectDateActivity.class);
            intent.putExtra(TRAINED_SPORT_SELECTED, sportSelectedIndex);
            startActivity(intent);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}