package labs.altomobile.com.stt.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.adapters.ChallengeAdapter;
import labs.altomobile.com.stt.adapters.FirebaseChallengeAdapater;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetMyChallenges;
import labs.altomobile.com.stt.requests.firebase.GetMyFriendsRequest;

public class ChallengesActivity extends AppCompatActivity implements OnRequestCallback<Challenge>, SwipeRefreshLayout.OnRefreshListener, OnFirebaseRequestCallback<ChallengeFirebase> {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Challenge> mList = new ArrayList<>();
    private List<ChallengeFirebase> mFirebaseChallengesList = new ArrayList<>();

    private ProgressBar progressBar;
    private TextView mEmptyListTV;

    private ChallengeAdapter mAdapter;
    private FirebaseChallengeAdapater mFirebaseAdapater;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_challenges);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);



        progressBar = (ProgressBar) findViewById(R.id.loading);
        progressBar.setVisibility(View.VISIBLE);

        mEmptyListTV = (TextView) findViewById(R.id.tv_no_challenge_yet);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        if (STTConstants.USE_FIREBASE){
            if(mFirebaseAdapater== null){
                mFirebaseAdapater = new FirebaseChallengeAdapater(mFirebaseChallengesList);
            }
            mRecyclerView.setAdapter(mFirebaseAdapater);
            mFirebaseAdapater.setContext(this);
            mFirebaseAdapater.setViewAsOwner(true);
            new GetMyChallenges().onBackground(this);
        } else {
            if(mAdapter == null) {
                mAdapter = new ChallengeAdapter(mList);
            }

            mRecyclerView.setAdapter(mAdapter);
            mAdapter.setContext(this);
            mAdapter.setViewAsOwner(true);
            RequestManager.GET_CHALLENGES_OF_CURRENT_USER.onBackground(this);
        }
    }

    @Override
    public void onRefresh() {
        if (STTConstants.USE_FIREBASE){
            new GetMyChallenges().onBackground(this);
        } else {
            RequestManager.GET_CHALLENGES_OF_CURRENT_USER.onBackground(this);
        }
    }

    @Override
    public void onRequestBegin() {
    }

    @Override
    public void onRequestFinished(List<Challenge> list, ParseObjectMap parseObjectMapmap) {

        if(list.size() > 0){

            mList.clear();
            mList.addAll(list);
            mAdapter.notifyDataSetChanged();
            mAdapter.setParseObjectMap(parseObjectMapmap);
        }else{
            mEmptyListTV.setVisibility(View.VISIBLE);
        }

        progressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestFinished(List<ChallengeFirebase> list) {
        if(list.size() > 0){

            mFirebaseChallengesList.clear();
            mFirebaseChallengesList.addAll(list);
            mFirebaseAdapater.notifyDataSetChanged();
        }else{
            mEmptyListTV.setVisibility(View.VISIBLE);
        }

        progressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
