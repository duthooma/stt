package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.baoyz.actionsheet.ActionSheet;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;

public class CheatsActivity extends AppCompatActivity implements ActionSheet.ActionSheetListener {

    ChallengeCheats challengeCheats = new ChallengeCheats();
    FriendRequestCheats friendRequestCheats = new FriendRequestCheats();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cheats);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button resetAndAddFriendRequests = (Button)findViewById(R.id.button_reset_friendrequests);
        resetAndAddFriendRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetFriendRequestClass();
            }
        });

        Button resetAndAddChallenges = (Button)findViewById(R.id.button_reset_challenges);
        resetAndAddChallenges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetChallengesClass();
            }
        });

        Button seeOldMainMenu = (Button)findViewById(R.id.button_see_old_mainmenu);
        seeOldMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheatsActivity.this, NavigationMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button actionSheet = (Button)findViewById(R.id.button_actionsheet);
        actionSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionSheet.createBuilder(CheatsActivity.this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Item1", "Item2", "Item3", "Item4")
                        .setCancelableOnTouchOutside(true)
                        .setListener(CheatsActivity.this).show();
            }
        });


    }

    private void resetFriendRequestClass(){
        Log.w(STTConstants.LOG_TAG,"DBCheatsActivity :: resetFriendRequestClass :: clearing all entries in FriendRequest PARSE class");
        RequestManager.CHEATS_DELETE_ALL_FRIEND_REQUESTS_AND_ADD_DUMMIES.onBackground(friendRequestCheats);
    }

    private void resetChallengesClass(){
        Log.w(STTConstants.LOG_TAG,"DBCheatsActivity :: resetChallengesClass :: clearing all entries in Challenge PARSE class");
        RequestManager.CHEATS_DELETE_ALL_CHALLENGES_AND_ADD_DUMMIES.onBackground(challengeCheats);
    }

    class ChallengeCheats implements OnRequestCallback<Challenge>{

        @Override
        public void onRequestBegin() {

        }

        @Override
        public void onRequestFinished(List<Challenge> list, ParseObjectMap parseObjectMapmap) {
            Toast.makeText(getApplicationContext(),"Challenges cleared and dummies added",Toast.LENGTH_LONG).show();
        }
    }

    class FriendRequestCheats implements OnRequestCallback<FriendRequest>{

        @Override
        public void onRequestBegin() {

        }

        @Override
        public void onRequestFinished(List<FriendRequest> list, ParseObjectMap parseObjectMapmap) {
            Toast.makeText(getApplicationContext(),"FriendRequests cleared and dummies added",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        Toast.makeText(getApplicationContext(), "click item index = " + index,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancle) {
        Toast.makeText(getApplicationContext(), "dismissed isCancle = " + isCancle, Toast.LENGTH_LONG).show();
    }
}
