package labs.altomobile.com.stt.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.parse.ParseUser;

import cn.pedant.SweetAlert.SweetAlertDialog;
import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.fragments.ChallengeDetailsFragment;
import labs.altomobile.com.stt.fragments.ChallengeSetDateFragment;
import labs.altomobile.com.stt.fragments.ChallengeSetNameFragment;
import labs.altomobile.com.stt.fragments.ChallengeSetRewardFragment;
import labs.altomobile.com.stt.fragments.InviteFriendsFragment;
import labs.altomobile.com.stt.fragments.SelectSportsFragment;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnSaveCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.utils.Debug;
import labs.altomobile.com.stt.utils.STTUtils;

public class CreateChallengeActivity extends AppCompatActivity
        implements OnSaveCallback<Challenge>, SelectSportsFragment.OnSportSelectedListener, InviteFriendsFragment.OnFriendSelected,
        ChallengeSetNameFragment.OnNameSelectedListener, ChallengeSetDateFragment.OnDateSelectedListener, ChallengeSetRewardFragment.OnRewardSelectedListener, ChallengeDetailsFragment.OnAddRewardListener, Firebase.CompletionListener {

    //fragments in this Activity
    private static final String FRAGMENT_STEP1_SELECT_FRIENDS = "FRAGMENT_STEP1_SELECT_FRIENDS";
    private static final String FRAGMENT_STEP2_SELECT_SPORT = "FRAGMENT_STEP2_SELECT_SPORT";
    private static final String FRAGMENT_STEP3_SET_NAME = "FRAGMENT_STEP3_SET_NAME";
    private static final String FRAGMENT_STEP4_SET_DATE = "FRAGMENT_STEP4_SET_DATE";
    private static final String FRAGMENT_STEP5_PREVIEW_NO_REWARD = "FRAGMENT_STEP5_PREVIEW_NO_REWARD";
    private static final String FRAGMENT_STEP6_ADD_REWARD = "FRAGMENT_STEP6_ADD_REWARD";
    private static final String FRAGMENT_STEP7_PREVIEW_WITH_REWARD = "FRAGMENT_STEP7_PREVIEW_WITH_REWARD";

    private TextView mInfoTV;
    public Button mActionButton;
    SweetAlertDialog mProgressDialog;

    private ParseUser mToUser = null;
    private User mFirebaseToUser = null;
    private int mSportId = -1;
    private String mName = "";
    private String mDesc = "";
    private long mDate = 0;
    int mRewardId = -1;
    String mRewardComment = "";
    private Challenge mChallenge = null;
    private ChallengeFirebase mFirebaseChallenge = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_challenge);

        // Inflate your custom layout
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.actionbar_custom_one_button,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        TextView actionBarTitleTextView = (TextView) findViewById(R.id.action_bar_custom_title);
        actionBarTitleTextView.setText(getString(R.string.title_activity_create_challenge));
        mActionButton = (Button) findViewById(R.id.action_bar_action_button);
        mInfoTV = (TextView) findViewById(R.id.textview_header);

        showStep1SelectFriends();
    }

    /* Callback of Step 1 : friend selected */
    @Override
    public void onFriendSelected(ParseUser parseUser) {
        mToUser = parseUser;
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: friend selected to be challenged :: User[" + mToUser.getObjectId() + " - " + mToUser.getUsername() + "]");
        showStep2SelectSport();
    }

    /* Callback of Step 1 : friend selected */
    @Override
    public void onFirebaseFriendSelected(User user) {
        mFirebaseToUser = user;
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: friend selected to be challenged :: User[" + mFirebaseToUser.getUid() + " - " + mFirebaseToUser.getUserName() + "]");
        showStep2SelectSport();
    }

    /* Callback of Step 2 : sport selected */
    @Override
    public void onSportSelected(int sportId) {
        if(!STTConstants.USE_FIREBASE)
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: sport selected for the challenge :: " + "User[" + mToUser.getObjectId() + " - " + mToUser.getUsername() + "] :: " + "Sport [" + sportId + "]");
        mSportId = sportId;
        showStep3SetName();
    }

    /* Callback of Step 3 : name and description set */
    @Override
    public void onNameSelected(String name, String desc) {
        if(!STTConstants.USE_FIREBASE)
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: name selected for the challenge :: " + "User[" + mToUser.getObjectId() + " - " + mToUser.getUsername() + "] :: " + "Sport [" + mSportId + "] :: name [" + mName + "]");
        mName = name;
        mDesc = desc;
        showStep4SelectEndDate();
    }

    /* Callback of Step 4 : date selected */
    @Override
    public void onDateSelected(long date) {
        mDate = date;
        showStep5PreviewChallengeWithoutReward();
    }

    /* Callback of Step 5 : user wants to set a reward */
    @Override
    public void onAddRewardListener() {
        showStep6AddAReward();
    }

    /* Callback of Step 6 : reward selected */
    @Override
    public void onRewardSelectedListener(int rewardId, String rewardComment) {

        mRewardId = rewardId;
        mRewardComment = rewardComment;

        STTUtils.hideKeyboard(this);

        showStep7PreviewWithReward();
    }

    /* Callback final : challenged created */
    @Override
    public void onSaveObject(Challenge challenge) {
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: Challenge successfully created for :: " +
                "User[" + mToUser.getObjectId() + " - " + mToUser.getUsername() + "] :: " +
                "Sport [" + mSportId + "] :: name [" + mName + "] :: date[" + mDate + "]");

        mProgressDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        mProgressDialog.setTitleText(getString(R.string.sent));
        mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Intent intent = new Intent(CreateChallengeActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                mProgressDialog.dismiss();
            }

        }, 1300);
    }

    /* Step 1 : select FRIEND to challenge */
    private void showStep1SelectFriends() {
        setHeaderAndActionButton(1);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        InviteFriendsFragment friendsFragment = new InviteFriendsFragment();
        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);
        friendsFragment.setArguments(args);

        ft.replace(R.id.fragment_container, friendsFragment, FRAGMENT_STEP1_SELECT_FRIENDS);
        ft.commit();
    }

    /* Step 2 : select SPORT of challenge */
    private void showStep2SelectSport() {
        setHeaderAndActionButton(2);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        SelectSportsFragment sportsFragment = new SelectSportsFragment();
        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);
        sportsFragment.setArguments(args);

        ft.replace(R.id.fragment_container, sportsFragment, FRAGMENT_STEP2_SELECT_SPORT);
        ft.commit();
    }

    /* Step 3 : set NAME and DESCRIPTION of challenge */
    private void showStep3SetName() {
        setHeaderAndActionButton(3);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ChallengeSetNameFragment setNameFragment = new ChallengeSetNameFragment();
        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);
        setNameFragment.setArguments(args);

        ft.replace(R.id.fragment_container, setNameFragment, FRAGMENT_STEP3_SET_NAME);
        ft.commit();
    }

    /* Step 4 : set END DATE of challenge */
    void showStep4SelectEndDate() {
        setHeaderAndActionButton(4);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ChallengeSetDateFragment setDateFragment = new ChallengeSetDateFragment();
        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);
        setDateFragment.setArguments(args);

        ft.replace(R.id.fragment_container, setDateFragment, FRAGMENT_STEP4_SET_DATE);
        ft.commit();
    }

    /* Step 5 : preview the challenge. Create it or go to next step: set a reward */
    void showStep5PreviewChallengeWithoutReward() {
        setHeaderAndActionButton(5);

        mActionButton.setOnClickListener(createChallengeListener);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);


        if ((mToUser != null && mName != null && !mName.isEmpty() && mDate != -1 && mSportId != -1) || mFirebaseToUser!= null) {
            if(STTConstants.USE_FIREBASE){
                mFirebaseChallenge = new ChallengeFirebase(FirebaseHelper.getUserUid(),mFirebaseToUser.getUid(),mSportId,mName,mDesc,mDate,-1,"");
                args.putParcelable(FirebaseConstants.CLASS_NAME_CHALLENGE_FIREBASE, mFirebaseChallenge);
                args.putParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE, mFirebaseToUser);
                args.putParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE+"from", FirebaseHelper.getCurrentUser());
            } else {
                mChallenge = new Challenge(ParseUser.getCurrentUser(), mToUser, mSportId, mName, mDesc, mDate, -1, "");
                args.putSerializable(ParseConstants.CLASS_NAME_CHALLENGE, mChallenge.getObjectId());
                ParseObjectMap.getInstance().add(mChallenge);
            }
        } else {
            if(!STTConstants.USE_FIREBASE)
                Log.e(STTConstants.LOG_TAG, "ERROR :: mChallenge couldn´ be created because of one of the following value :: User[" + mToUser.getObjectId() + " - " + mToUser.getString("username") + "] :: " +
                    "Sport [" + mSportId + "] :: name [" + mName + "] :: date[" + mDate + "] ");
        }

        ChallengeDetailsFragment detailFragment = new ChallengeDetailsFragment();
        detailFragment.setArguments(args);

        ft.replace(R.id.fragment_container, detailFragment, FRAGMENT_STEP5_PREVIEW_NO_REWARD);
        ft.commit();
    }

    /* Step 6 : set a REWARD and reward DESCRIPTION */
    void showStep6AddAReward() {
        setHeaderAndActionButton(6);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Bundle args = new Bundle();

        ChallengeSetRewardFragment setRewardFragment = new ChallengeSetRewardFragment();
        setRewardFragment.setArguments(args);

        ft.replace(R.id.fragment_container, setRewardFragment, FRAGMENT_STEP6_ADD_REWARD);
        ft.commit();
    }

    /* Step 7 : preview the challenge with a reward set. Only action possible : create challenge */
    void showStep7PreviewWithReward() {
        setHeaderAndActionButton(7);

        mActionButton.setOnClickListener(createChallengeListener);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Bundle args = new Bundle();
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE);


        if ((mToUser != null && mName != null && !mName.isEmpty() && mDate != -1 && mSportId != -1) || mFirebaseToUser!= null) {
            if(STTConstants.USE_FIREBASE){
                mFirebaseChallenge = new ChallengeFirebase(FirebaseHelper.getUserUid(),mFirebaseToUser.getUid(),mSportId,mName,mDesc,mDate,mRewardId,mRewardComment);
                args.putParcelable(FirebaseConstants.CLASS_NAME_CHALLENGE_FIREBASE, mFirebaseChallenge);
                args.putParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE, mFirebaseToUser);
                args.putParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE+"from", FirebaseHelper.getCurrentUser());
            } else {
                mChallenge = new Challenge(ParseUser.getCurrentUser(), mToUser, mSportId, mName, mDesc, mDate, mRewardId, mRewardComment);
                args.putSerializable(ParseConstants.CLASS_NAME_CHALLENGE, mChallenge.getObjectId());
                ParseObjectMap.getInstance().add(mChallenge);
            }
        } else {
            if(!STTConstants.USE_FIREBASE)
                Log.e(STTConstants.LOG_TAG, "ERROR :: mChallenge couldn´ be created because of one of the following value :: User[" + mToUser.getObjectId() + " - " + mToUser.getString("username") + "] :: " +
                    "Sport [" + mSportId + "] :: name [" + mName + "] :: date[" + mDate + "] ::  rewardId [" + mRewardId + "] ::  rewardComment [" + mRewardComment + "]");
        }

        ChallengeDetailsFragment detailFragment = new ChallengeDetailsFragment();
        detailFragment.setArguments(args);

        ft.replace(R.id.fragment_container, detailFragment, FRAGMENT_STEP7_PREVIEW_WITH_REWARD);
        ft.commit();
    }

    private void setHeaderAndActionButton(int currentStep){
        switch (currentStep){
            case 1:
                mActionButton.setVisibility(View.GONE);
                mInfoTV.setVisibility(View.VISIBLE);
                mInfoTV.setText(getString(R.string.friend_to_challenge));
                break;
            case 2:
                mActionButton.setVisibility(View.GONE);
                mInfoTV.setVisibility(View.VISIBLE);
                mInfoTV.setText(getString(R.string.sport_of_challenge));
                break;
            case 3:
                mActionButton.setVisibility(View.VISIBLE);
                mInfoTV.setVisibility(View.VISIBLE);
                mActionButton.setText(getString(R.string.next));
                mInfoTV.setText(getString(R.string.some_words_about_challenge));
                break;
            case 4:
                mActionButton.setVisibility(View.VISIBLE);
                mInfoTV.setVisibility(View.GONE);
                mActionButton.setText(getString(R.string.next));
                break;
            case 5:
                mActionButton.setVisibility(View.VISIBLE);
                mInfoTV.setVisibility(View.GONE);
                mActionButton.setText(getString(R.string.send));
                break;
            case 6:
                //do nothing. the fragment does the job because there are 2 steps in this fragment
                break;
            case 7:
                mActionButton.setVisibility(View.VISIBLE);
                mInfoTV.setVisibility(View.GONE);
                mActionButton.setText(getString(R.string.send));
                break;
            default:
                mActionButton.setVisibility(View.GONE);
                mInfoTV.setVisibility(View.GONE);
        }
    }

    private void createFirebaseChallenge(ChallengeFirebase challenge) {
        FirebaseHelper.saveChallenge(challenge,this);
    }
    private void createChallenge(Challenge challenge) {
        ParseHelper.saveChallenge(challenge, this);
    }

    private View.OnClickListener createChallengeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mProgressDialog = new SweetAlertDialog(CreateChallengeActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            mProgressDialog.getProgressHelper().setBarColor(R.color.colorPrimary);
            mProgressDialog.setTitleText(getString(R.string.sending));
            mProgressDialog.setCancelable(false);
            mProgressDialog.showCancelButton(false);
            mProgressDialog.show();
            if(STTConstants.USE_FIREBASE)
                createFirebaseChallenge(mFirebaseChallenge);
            else
                createChallenge(mChallenge);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        FragmentManager fm = getFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.fragment_container);

        switch (currentFragment.getTag()){
            case FRAGMENT_STEP1_SELECT_FRIENDS:
                super.onBackPressed();
                finish();
                break;
            case FRAGMENT_STEP2_SELECT_SPORT:
                mToUser = null;
                showStep1SelectFriends();
                break;
            case FRAGMENT_STEP3_SET_NAME:
                mSportId = -1;
                showStep2SelectSport();
                STTUtils.hideKeyboard(this);
                break;
            case FRAGMENT_STEP4_SET_DATE:
                mName = "";
                mDesc = "";
                showStep3SetName();
                break;
            case FRAGMENT_STEP5_PREVIEW_NO_REWARD:
                mDate = 0;
                showStep4SelectEndDate();
                break;
            case FRAGMENT_STEP6_ADD_REWARD:
                showStep5PreviewChallengeWithoutReward();
                break;
            case FRAGMENT_STEP7_PREVIEW_WITH_REWARD:
                STTUtils.hideKeyboard(this);
                mRewardId = -1;
                mRewardComment = "";
                showStep6AddAReward();
                break;
            default:
                Debug.Error("CreateChallengeActivity : Backkey options NOT SUPPORTED ");
                startActivity(new Intent(this, MainMenuActivity.class));
                finish();
        }
    }

    @Override
    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
        Log.i(STTConstants.LOG_TAG, this.getClass().getSimpleName() + " :: Challenge successfully created for :: " +
                "User[" + mFirebaseToUser.getUid() + " - " + mFirebaseToUser.getUserName() + "] :: " +
                "Sport [" + mSportId + "] :: name [" + mName + "] :: date[" + mDate + "]");

        mProgressDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        mProgressDialog.setTitleText(getString(R.string.sent));
        mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Intent intent = new Intent(CreateChallengeActivity.this, MainMenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                mProgressDialog.dismiss();
            }

        }, 1300);
    }
}
