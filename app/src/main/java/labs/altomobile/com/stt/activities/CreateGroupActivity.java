package labs.altomobile.com.stt.activities;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.fragments.CreateGroupSetPictureFragment;
import labs.altomobile.com.stt.fragments.InviteFriendsFragment;
import labs.altomobile.com.stt.fragments.SelectSportsFragment;
import labs.altomobile.com.stt.fragments.ChallengeSetNameFragment;
import labs.altomobile.com.stt.fragments.SportsOfGroupFragment;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;

public class CreateGroupActivity extends AppCompatActivity implements SelectSportsFragment.OnSportsSelectedListener, CreateGroupSetPictureFragment.OnPictureSelected {
    /*public class CreateGroupActivity extends AppCompatActivity implements ChallengeSetNameFragment.OnGroupCreatedListener,
        SelectSportsFragment.OnSportsSelectedListener, CreateGroupSetPictureFragment.OnPictureSelected {*/

    public static final String GROUP_SERIALIZABLE = "GROUP_SERIALIZABLE";
    public static final String SPORTS_SELECTED = "SPORTS_SELECTED";

    //fragments in this Activity
    private static final String SET_NAME_FRAGMENT = "SET_NAME_FRAGMENT";
    private static final String SELECT_SPORT_FRAGMENT = "SELECT_SPORT_FRAGMENT";
    public static final String SELECT_PICTURE_FRAGMENT = "SELECT_PICTURE_FRAGMENT";
    public static final String INVITE_FRIENDS_FRAGMENT = "INVITE_FRIENDS_FRAGMENT";
    public static final String HEADER_FRAGMENT = "HEADER_FRAGMENT";

    Group mGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        // Inflate your custom layout
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.actionbar_create_group,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(actionBarLayout);

//        final int actionBarColor = getResources().getColor(R.color.wallet_highlighted_text_holo_light);
//        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ChallengeSetNameFragment setNameFragment = (ChallengeSetNameFragment)fm.findFragmentByTag(SET_NAME_FRAGMENT);

        if (setNameFragment == null && savedInstanceState == null){

            setNameFragment = new ChallengeSetNameFragment();
            ft.add(R.id.fragment_container, setNameFragment, SET_NAME_FRAGMENT);
            ft.commit();
        }
    }

    /*
    @Override
    public void onGroupCreated(Group group) {

        mGroup = group;

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        SelectSportsFragment selectSportFragment = new SelectSportsFragment();
        Bundle args = new Bundle();

        SerializableParseObject sgroup = new SerializableParseObject();
        sgroup.setParseObject(group);
        args.putSerializable(GROUP_SERIALIZABLE, sgroup);
        args.putInt(AppSettingsConstants.DISPLAY_OPTION, AppSettingsConstants.DISPLAY_OPTION_CREATE_GROUP);

        selectSportFragment.setArguments(args);

        ft.replace(R.id.fragment_container, selectSportFragment, SELECT_SPORT_FRAGMENT);
        ft.commit();
    }*/

    @Override
    public void onSportsSelected(List<Integer> sportsSelected) {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();


        //add header fragment
        SportsOfGroupFragment sportsFragment = new SportsOfGroupFragment();

        Bundle argsHeader = new Bundle();
        argsHeader.putIntegerArrayList(SPORTS_SELECTED, (ArrayList<Integer>)sportsSelected);
        sportsFragment.setArguments(argsHeader);
        ft.add(R.id.fragment_header, sportsFragment, HEADER_FRAGMENT);

        //add SelectedSport Fragment
        CreateGroupSetPictureFragment setPictureFragment = new CreateGroupSetPictureFragment();

        Bundle args = new Bundle();
        SerializableParseObject sgroup = new SerializableParseObject();
        sgroup.setParseObject(mGroup);
        args.putSerializable(GROUP_SERIALIZABLE, sgroup);

        setPictureFragment.setArguments(args);

        ft.replace(R.id.fragment_container, setPictureFragment, SELECT_PICTURE_FRAGMENT);

        ft.commit();
    }

    @Override
    public void onPictureSelected() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        InviteFriendsFragment inviteFriendsFragment = new InviteFriendsFragment();
        Bundle args = new Bundle();
        SerializableParseObject sgroup = new SerializableParseObject();
        sgroup.setParseObject(mGroup);
        args.putSerializable(GROUP_SERIALIZABLE, sgroup);
        args.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_CREATE_GROUP);
        inviteFriendsFragment.setArguments(args);

        ft.replace(R.id.fragment_container, inviteFriendsFragment, INVITE_FRIENDS_FRAGMENT);

        ft.commit();
    }
}
