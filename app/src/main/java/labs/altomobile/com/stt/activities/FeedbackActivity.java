package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;

public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //open email default app
        // ACTION_SENDTO filters for email apps (discard bluetooth and others)
        Button emailButton = (Button)findViewById(R.id.button_email);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmailAction(STTConstants.EMAIL_CONTACT);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendEmailAction(String email){
        String uriText =
                "mailto:" + email +
                        "?subject=" + Uri.encode(getString(R.string.feedback_email_subject)) +
                        "&body=" + Uri.encode("");

        Uri uri = Uri.parse(uriText);

        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        startActivity(Intent.createChooser(sendIntent, "Send email"));
    }
}
