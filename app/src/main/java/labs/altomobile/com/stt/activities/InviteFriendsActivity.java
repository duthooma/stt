package labs.altomobile.com.stt.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.adapters.InviteUserAdapter;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.FriendRequestHelper;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.NestedListView;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetFriendsWithRequestsFirebase;

public class InviteFriendsActivity extends AppCompatActivity implements OnRequestCallback<FriendRequest>,OnFirebaseRequestCallback<String> {

    //list of friend that can be invited.
    List<JSONObject> invitableFriendsList = null;

    private ProgressBar mLoading;
    private Button  mActionButton;
    private NestedListView mListView;
    private InviteUserAdapter mArrarAdapter;
    private TextView mNoResultTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set up custom actionbar
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.actionbar_custom_one_button, null);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(actionBarLayout);
        TextView actionBarTitleTextView = (TextView)findViewById(R.id.action_bar_custom_title);
        actionBarTitleTextView.setText(getString(R.string.title_activity_invite_friends));

        loadFriendsFromFacebook(new FriendsLoadedCallback() {
            @Override
            public void afterFriendsLoaded() {
                processFacebookFriendsResult();
            }
        });

        setContentView(R.layout.activity_invite_friends);

        mLoading = (ProgressBar) findViewById(R.id.loading);
        mActionButton = (Button) findViewById(R.id.action_bar_action_button);
        mListView = (NestedListView) findViewById(android.R.id.list);
        mNoResultTextView = (TextView) findViewById(R.id.textview_no_result);

        mActionButton.setText(getString(R.string.invite));
        mActionButton.setVisibility(View.INVISIBLE);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(STTConstants.USE_FIREBASE){
                    FirebaseHelper.createFriendRequest(mArrarAdapter.getIdsToInvite());
                }else {
                    String facebookId = ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID);
                    FriendRequestHelper.getInstance().sendFriendRequestList(facebookId, mArrarAdapter.getIdsToInvite());
                }
                //toast to tell that the invitation was sent
                Toast.makeText(getBaseContext(), getString(R.string.invitation_sent), Toast.LENGTH_LONG).show();
                //close the activity
                onBackPressed();
            }
        });
    }

    /*
     * Now that user_friends is granted, load /me/friends to get
     * friends who have installed the game (if using Platform v2.0).
     */
    private void loadFriendsFromFacebook(final FriendsLoadedCallback callback) {

        GraphRequestBatch requestBatch = new GraphRequestBatch();

        final AccessToken accessToken = AccessToken.getCurrentAccessToken();

        // Get a list of friends who have _not installed_ the game.
        GraphRequest invitableFriendsRequest = GraphRequest.newGraphPathRequest(accessToken,
                "/me/friends", new GraphRequest.Callback() {

                    @Override
                    public void onCompleted(GraphResponse response) {

                        FacebookRequestError error = response.getError();
                        if (error != null) {
                            Log.e(STTConstants.LOG_TAG, error.toString());
                            mLoading.setVisibility(View.GONE);
                            //handleError(error, true);
                        } else  {
                            if (response != null) {
                                // Get the result
                                JSONObject jsonObject = response.getJSONObject();
                                JSONArray dataArray = null;
                                try {
                                    dataArray = jsonObject.getJSONArray("data");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if(dataArray != null) {
                                    List<JSONObject> invitableFriends = new ArrayList<JSONObject>();
                                    if (dataArray.length() > 0) {
                                        mNoResultTextView.setVisibility(View.GONE);
                                        // Ensure the user has at least one friend ...
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            invitableFriends.add(dataArray.optJSONObject(i));
                                        }
                                    }else{
                                        mNoResultTextView.setVisibility(View.VISIBLE);
                                    }
                                    invitableFriendsList = invitableFriends;
                                    callback.afterFriendsLoaded();
                                }
                                else
                                {
                                    //doesn't have any friend with this app
                                    mNoResultTextView.setVisibility(View.VISIBLE);
                                    mLoading.setVisibility(View.GONE);
                                }
                            }else{
                                mLoading.setVisibility(View.GONE);
                            }
                        }
                    }

                });
        Bundle friendsParams = new Bundle();
        friendsParams.putString("fields", "id,first_name,picture");
        invitableFriendsRequest.setParameters(friendsParams);
        requestBatch.add(invitableFriendsRequest);

        // Execute the batch of requests asynchronously
        requestBatch.executeAsync();
    }

    private void processFacebookFriendsResult() {
        if (STTConstants.USE_FIREBASE){
            new GetFriendsWithRequestsFirebase().onBackground(this);
        } else {
            RequestManager.GET_FRIEND_REQUEST_OF_CURRENT_USER.onBackground(this);
        }
    }

    private void showFriendsForInvitation(List<FriendRequest> invitedFriendsList) {
        if(mLoading != null)
            mLoading.setVisibility(View.GONE);

        boolean hideActionButton = true;

        final List<InvitableFriend> friendsList = new ArrayList<>();

        //Fill a list with the facebook ids of friends that were invited
        ArrayList<String> invitedFacebookIds= new ArrayList<String>();
        for(FriendRequest friendRequest: invitedFriendsList)
            invitedFacebookIds.add(friendRequest.getString(FriendRequest.PARSEFIELD_USER2));


        //filter friends that were not invited previously
        Iterator<JSONObject> jsonObjectIterator = invitableFriendsList.iterator();
        while(jsonObjectIterator.hasNext())
        {
            JSONObject json = jsonObjectIterator.next();
            int index = invitedFacebookIds.indexOf(json.optString("id"));
            if(index == -1) {
                friendsList.add(new InvitableFriend(json, false));
                hideActionButton = false;
            }
            else
                friendsList.add(new InvitableFriend(json, true));



        }

        if(friendsList.size() > 0) {
            mArrarAdapter = new InviteUserAdapter(this, R.layout.row_invite_friends_with_checkbox, friendsList);
            mListView.setAdapter(mArrarAdapter);
            if (!hideActionButton)
                mActionButton.setVisibility(View.VISIBLE);
        }
    }

    private void showFriendsForInvitationFirebase(List<String> friendsKeysList) {
        if(mLoading != null)
            mLoading.setVisibility(View.GONE);

        boolean hideActionButton = true;

        final List<InvitableFriend> friendsList = new ArrayList<>();

        //Fill a list with the facebook ids of friends that were invited
        ArrayList<String> invitedFacebookIds= new ArrayList<>();
        for(String key: friendsKeysList) {
            if(key.contains(STTConstants.FACEBOOK_KEY_PREFIX)) {
                invitedFacebookIds.add(key.replace(STTConstants.FACEBOOK_KEY_PREFIX,""));
            }
        }

        //filter friends that were not invited previously
        Iterator<JSONObject> jsonObjectIterator = invitableFriendsList.iterator();
        while(jsonObjectIterator.hasNext())
        {
            JSONObject json = jsonObjectIterator.next();
            int index = invitedFacebookIds.indexOf(json.optString("id"));
            if(index == -1) {
                friendsList.add(new InvitableFriend(json, false));
                hideActionButton = false;
            }
            else
                friendsList.add(new InvitableFriend(json, true));



        }

        if(friendsList.size() > 0) {
            mArrarAdapter = new InviteUserAdapter(this, R.layout.row_invite_friends_with_checkbox, friendsList);
            mListView.setAdapter(mArrarAdapter);
            if (!hideActionButton)
                mActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<FriendRequest> list, ParseObjectMap parseObjectMap) {
        showFriendsForInvitation(list);
        mLoading.setVisibility(View.GONE);
    }

    @Override
    public void onRequestFinished(List<String> list) {
        //here we get the list of request to friends
        showFriendsForInvitationFirebase(list);
        mLoading.setVisibility(View.GONE);
    }

    interface FriendsLoadedCallback {
        void afterFriendsLoaded();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class InvitableFriend {
        JSONObject friend;
        boolean alreadyInvited = false;

        InvitableFriend(JSONObject friend, boolean alreadyInvited){
            this.friend = friend;
            this.alreadyInvited = alreadyInvited;
        }

        public JSONObject getFriend() {
            return friend;
        }


        public boolean isAlreadyInvited() {
            return alreadyInvited;
        }

    }
}
