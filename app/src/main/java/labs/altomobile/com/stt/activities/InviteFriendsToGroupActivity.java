package labs.altomobile.com.stt.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.RequestParams;


public class InviteFriendsToGroupActivity extends Activity implements OnRequestCallback<ParseUser> {

    //Get friends that aren't in a given group and haven't send invitation

    private ListView friendsLV;
    private FriendsAdapter adapter;
    private List<ParseUser> allFriends;
    private String mGroupId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends_to_training);

        friendsLV = (ListView) findViewById(R.id.list_friends);
        allFriends = new ArrayList<ParseUser>();
        adapter = new FriendsAdapter(getApplicationContext(), R.layout.row_invite_friends_with_checkbox, allFriends);
        friendsLV.setAdapter(adapter);

        if (getIntent().getExtras() != null) {
            mGroupId = getIntent().getExtras().getString(Group.GROUP_ID);
            RequestManager.GET_FRIENDS_TO_INVITE_A_GROUP.onBackground(RequestParams.create().add(Group.GROUP_ID, mGroupId), this);
        }

    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<ParseUser> list, ParseObjectMap parseObjectMapmap) {
        allFriends.clear();
        allFriends.addAll(list);
        adapter.notifyDataSetChanged();
    }

    private class FriendsAdapter extends ArrayAdapter<ParseUser> {


        public FriendsAdapter(Context context, int resource,
                              List<ParseUser> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_invite_friends_with_checkbox, parent, false);
            }

            final ParseUser user = getItem(position);

            Log.w(STTConstants.LOG_TAG, "Adaptor::getView:: user.getId() == " + user.getObjectId());

            TextView nameTextV = (TextView) view.findViewById(R.id.friendtoinvite_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.friendtoinvite_picture);
            //final Button button = (Button) view.findViewById(R.id.btn_invite);

            profilePictureView.setProfileId(user.getString(STTConstants.FACEBOOK_ID));
            nameTextV.setText(user.getUsername());


            //this section is commented because I had to comment the previous line because R.id.btn_invite couldn't be found.
            //R.id.btn_invite is NOT in the layout "row_invite_friends_with_checkbox" ==> something is wrong here
            //Note that this activity isn't used anymore: is should probably be cleaned
            /*button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    createGroupInvitation(user);
                }
            });*/

            return view;
        }
        private void createGroupInvitation(ParseUser parseUser)
        {
            ParseHelper.createGroupInvitation(parseUser.getObjectId(),mGroupId);

        }
    }
}
