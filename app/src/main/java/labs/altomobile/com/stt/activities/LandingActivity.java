package labs.altomobile.com.stt.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import org.json.JSONObject;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.objects.UserLevel;


public class LandingActivity extends Activity implements LogInCallback, View.OnClickListener {

    //private ProgressBar loading;
    //private Button mTryAgainButton;
    AccessTokenTracker accessTokenTracker;
    private int ACTIVITY_LOGIN_FACEBOOK_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_landing);

        loading = (ProgressBar) findViewById(R.id.loading);
        mTryAgainButton = (Button) findViewById(R.id.button_try_again);
        mTryAgainButton.setOnClickListener(this);*/

        logIn();
    }

    private void logIn(){
        //mTryAgainButton.setVisibility(View.INVISIBLE);
        //loading.setVisibility(View.VISIBLE);
        if(AccessToken.getCurrentAccessToken() == null) {
            Intent newIntent = new Intent(getApplicationContext(), LoginFacebookActivity.class);
            startActivityForResult(newIntent, ACTIVITY_LOGIN_FACEBOOK_CODE);
        }
        else
            facebookSuccess();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged( AccessToken oldAccessToken, AccessToken currentAccessToken) {
                //was log out?
                if(oldAccessToken != null && currentAccessToken== null)
                {
                    Intent newIntent = new Intent(getApplicationContext(), LoginFacebookActivity.class);
                    startActivityForResult(newIntent, ACTIVITY_LOGIN_FACEBOOK_CODE);
                }
            }
        };
    }

    private void facebookSuccess()
    {

        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(STTConstants.USE_FIREBASE){
            authWithFirebase(accessToken);
        } else {
            ParseFacebookUtils.logInInBackground(accessToken, this);
        }
        //startService(new Intent(this, LocationTrackerService.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ACTIVITY_LOGIN_FACEBOOK_CODE)
        {
            if(resultCode  == RESULT_OK)
                facebookSuccess();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }

    //called when the Parse logInInBackground is done.
    @Override
    public void done(ParseUser parseUser, ParseException e) {
        //loading.setVisibility(View.GONE);
        if (e != null){
            //mTryAgainButton.setVisibility(View.VISIBLE);
            return;
        }

        if (parseUser == null) {
            Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");

        } else if (parseUser.isNew()) {
            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            // make request to the /me API
            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback(){

                // callback after Graph API response with user object
                @Override
                public void onCompleted(JSONObject user, GraphResponse response) {

                    String facebookUsername = user.optString("name");//user.getName();
                    String facebookId = user.optString("id");//user.getId();
                    if (facebookUsername != null) {
                        ParseUser.getCurrentUser().setUsername(facebookUsername);
                        ParseUser.getCurrentUser().put(STTConstants.FACEBOOK_ID, facebookId);

                        //saving gcm id
                        String gcmId = get_CGM_Id();
                        if(gcmId != null)
                            ParseUser.getCurrentUser().put(STTConstants.GCM_ID, gcmId);

                        ParseUser.getCurrentUser().saveInBackground();
                    }

                    Intent intent = new Intent(LandingActivity.this, MainMenuActivity.class);
                    startActivity(intent);
                }
            });
            request.executeAsync();

            //Create the UserLevel linked to this User
            UserLevel userLevel = new UserLevel(parseUser.getObjectId());
            userLevel.setLevel(UserLevel.INITIAL_LEVEL);
            ParseHelper.save(userLevel);

        } else {
            Log.d("MyApp", "User logged in through Facebook!");

            //check the gcm id is different from the stored in parce
            String parse_GCM_id = ParseUser.getCurrentUser().getString(STTConstants.GCM_ID);
            String gcmId = get_CGM_Id();
            Log.d(STTConstants.LOG_TAG,"GCM ID:"+gcmId);
            if(gcmId != null )
            {
                if(parse_GCM_id == null || !gcmId.equals(parse_GCM_id))
                {
                    Log.d(STTConstants.LOG_TAG,"Saving gcm ID");
                    ParseUser.getCurrentUser().put(STTConstants.GCM_ID, gcmId);
                    ParseUser.getCurrentUser().saveInBackground();
                }
            }

            Intent intent = new Intent(LandingActivity.this, MainMenuActivity.class);
            startActivity(intent);
        }
    }



    private String get_CGM_Id()
    {
        SharedPreferences sharedpreferences = getSharedPreferences(STTConstants.FILE_NAME_GCM_CONFIG, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(STTConstants.GCM_ID))
        {
            return sharedpreferences.getString(STTConstants.GCM_ID, "");
        }

        return null;
    }

    @Override
    public void onClick(View v) {
        /*if (v == mTryAgainButton){
            logIn();
        }*/
    }

    private void authWithFirebase(AccessToken token) {
        if (token != null) {
            Firebase  firebaseRef = FirebaseHelper.getSharedFirebase();
            firebaseRef.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    FirebaseHelper.setLoggedUser(authData, new Firebase.CompletionListener() {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            // The Facebook user is now authenticated with your Firebase app
                            Intent intent = new Intent(LandingActivity.this, MainMenuActivity.class);
                            startActivity(intent);
                        }
                    });
                }
                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    //loading.setVisibility(View.GONE);
                    //mTryAgainButton.setVisibility(View.VISIBLE);
                    // there was an error
                    //TODO: show firebase login error
                    Log.d("MyApp", "Firebase onAuthenticationError");
                }
            });
        } else {
            Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
        }
    }


}
