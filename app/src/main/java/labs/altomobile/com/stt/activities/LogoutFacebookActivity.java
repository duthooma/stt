package labs.altomobile.com.stt.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.parse.ParseUser;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.utils.STTUtils;

/**
 * Created by flavioreyes on 6/5/15.
 */
public class LogoutFacebookActivity extends Activity {

    AccessTokenTracker accessTokenTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.logout_facebook);

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged( AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(currentAccessToken== null)
                {
                    ParseUser.getCurrentUser().logOut();
                    onBackPressed();
                }
            }
        };
    }

    public void onCancelClicked(View view)
    {
        onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }

}
