package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.fragments.FeedFragment;
import labs.altomobile.com.stt.fragments.MyActivityFragment;

public class MainMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawer;
    private FloatingActionButton fabChallenge;
    private FloatingActionButton fabTraining;
    private FloatingActionMenu fabMenu;

    private int[] tabIcons = {
            R.drawable.tab_home_selector,
            R.drawable.tab_myactivity_selector
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //set up the Drawer Layout button
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Button drawerButton = (Button)findViewById(R.id.button_drawer_menu);
        fabChallenge = (FloatingActionButton) findViewById(R.id.fab_challenge);
        fabTraining = (FloatingActionButton) findViewById(R.id.fab_training);
        fabMenu = (FloatingActionMenu) findViewById(R.id.fab_menu);

        drawerButton.setOnClickListener(openDrawerListener);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

        //FloatingAction menu
        fabMenu.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fabMenu.toggle(true);
            }
        });
        fabMenu.setClosedOnTouchOutside(true);

        //FloatingAction Buttons
        fabChallenge = (FloatingActionButton) findViewById(R.id.fab_challenge);
        fabTraining = (FloatingActionButton) findViewById(R.id.fab_training);

        fabChallenge.setOnClickListener(fabChallengeListener);
        fabTraining.setOnClickListener(fabTrainingListener);

        //feedback buttons
        Button feedbackButton = (Button)findViewById(R.id.button_feedback);
        Button feedbackImageButton = (Button)findViewById(R.id.button_feedback_image);
        feedbackButton.setOnClickListener(feedbackListener);
        feedbackImageButton.setOnClickListener(feedbackListener);
    }

    View.OnClickListener openDrawerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.openDrawer(GravityCompat.START);
            }
        }
    };

    View.OnClickListener feedbackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this,FeedbackActivity.class));
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
    };

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FeedFragment(), "");
        adapter.addFragment(new MyActivityFragment(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);

            // return null to display only the icon
            return null;
        }
    }

    //TODO: I suspect the following function onCreateOptionsMenu is NOT used anymore

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_challenges) {
            startActivity(new Intent(this,ChallengesActivity.class));
        } else if (id == R.id.nav_teams) {
            startActivity(new Intent(this,TeamsActivity.class));
        } else if (id == R.id.nav_friends) {
            startActivity(new Intent(this,MyFriendsActivity.class));
        } else if (id == R.id.nav_account) {
            startActivity(new Intent(this,MyAccountActivity.class));
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this,SettingsActivity.class));
        }else if (id == R.id.nav_cheats) {
            startActivity(new Intent(this,CheatsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private View.OnClickListener fabChallengeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this,CreateChallengeActivity.class));
            fabMenu.close(false);
        }
    };

    private View.OnClickListener fabTrainingListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainMenuActivity.this, AddTrainingSelectSportActivity.class));
            fabMenu.close(false);
        }
    };

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (fabMenu.isOpened()){
            fabMenu.close(true);
        }
        else {
            moveTaskToBack(true);
        }
    }
}
