package labs.altomobile.com.stt.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import labs.altomobile.com.stt.R;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMapLongClickListener, View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker mMarker;
    private LatLng mLatLng;
    private Button mButtonOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mButtonOk = (Button) findViewById(R.id.button_maps_ok);
        mButtonOk.setOnClickListener(this);
        initializePosition(getIntent().getExtras());
        setUpMapIfNeeded();
    }


    private void initializePosition(Bundle params) {
        double lat = params.getDouble("LAT");
        double lng = params.getDouble("LNG");
        mLatLng = new LatLng(lat, lng);
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.

        }
        if (mMap != null) {
            setUpMap();
        }
    }

    private void setUpMap() {

        mMap.setOnMapLongClickListener(this);
        setMarkerAt(mLatLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, MyPlaceInfoActivity.CAMERA_ZOOM));

    }

    private void setMarkerAt(LatLng latLng) {
        mLatLng = latLng;
        if (mMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(mLatLng);
            mMarker = mMap.addMarker(markerOptions);
        } else {

            mMarker.setPosition(mLatLng);
        }

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        setMarkerAt(latLng);
    }

    @Override
    public void onBackPressed() {
        mButtonOk.performClick();
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonOk) {
            setResult(RESULT_OK, getIntent());
            getIntent().putExtra("LAT", mLatLng.latitude);
            getIntent().putExtra("LNG", mLatLng.longitude);
            finish();
        }
    }
}
