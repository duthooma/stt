package labs.altomobile.com.stt.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.facebook.login.widget.ProfilePictureView;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetFriendsFirebase;
import mehdi.sakout.fancybuttons.FancyButton;

public class MyFriendsActivity extends AppCompatActivity implements OnRequestCallback<ParseUser>, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, OnFirebaseRequestCallback<User> {

    private TextView mNoFriendTV;
    private ListView mList;

    private ProgressBar mLoading;

    private MyFriendsAdapter mAdapter;
    private MyFirebaseFriendsAdapter mFirebaseAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<ParseUser> mFriends;
    private List<User> mFirebaseFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friends);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNoFriendTV = (TextView) findViewById(R.id.no_friend_request_to_display);
        mList = (ListView) findViewById(R.id.list_friends);
        mLoading = (ProgressBar) findViewById(R.id.loading);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);

        LinearLayout container = (LinearLayout)findViewById(R.id.layout_fancy_btn_container);
        FancyButton friendRequestButton = new FancyButton(this);
        friendRequestButton.setIconResource("\uf234");
        friendRequestButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        friendRequestButton.setFocusBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        friendRequestButton.setRadius(10);
        friendRequestButton.setPadding(35, 30, 35, 30);
        friendRequestButton.setIconPadding(10, 0, 10, 0);
        friendRequestButton.setCustomTextFont("robotothin.ttf");
        friendRequestButton.setText(getString(R.string.you_received_X_friendrequests, 2));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        container.addView(friendRequestButton, layoutParams);

        friendRequestButton.setOnClickListener(friendRequestListener);

        mSwipeRefreshLayout.setOnRefreshListener(this);


        findViewById(R.id.menu_fab_feed_add_friends).setOnClickListener(this);

        if (STTConstants.USE_FIREBASE) {
            mFirebaseFriends = new ArrayList<>();
            mFirebaseAdapter = new MyFirebaseFriendsAdapter(getApplicationContext(), R.layout.row_my_friends, mFirebaseFriends);
            mList.setAdapter(mFirebaseAdapter);
            new GetFriendsFirebase().onBackground(this);
        } else {
            mFriends = new ArrayList<>();
            mAdapter = new MyFriendsAdapter(getApplicationContext(), R.layout.row_my_friends, mFriends);
            mList.setAdapter(mAdapter);
            RequestManager.GET_FRIENDS_OF_CURRENT_USER.onBackground(this);
        }
        mList.setOnItemClickListener(userProfileListener);
    }

    AdapterView.OnItemClickListener userProfileListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MyFriendsActivity.this,UserProfileActivity.class);
            intent.putExtra(UserProfileActivity.USER_ID,mFriends.get(position).getObjectId());
            startActivity(intent);
        }
    };

    @Override
    public void onRefresh() {
        if (STTConstants.USE_FIREBASE) {
            new GetFriendsFirebase().onBackground(this);
        } else {
            RequestManager.GET_FRIENDS_OF_CURRENT_USER.onBackground(this);
        }
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<ParseUser> list, ParseObjectMap parseObjectMapmap) {

        if (list.size() > 0){
            mFriends.clear();
            mFriends.addAll(list);
            mAdapter.notifyDataSetChanged();
            mNoFriendTV.setVisibility(View.GONE);
        }else{
            mNoFriendTV.setVisibility(View.VISIBLE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
        mLoading.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.menu_fab_feed_add_friends:
            {
                Intent intent = new Intent(this, InviteFriendsActivity.class);
                startActivity(intent);
            }
            break;
        }
    }

    @Override
    public void onRequestFinished(List<User> list) {
        if (list.size() > 0){
            mFirebaseFriends.clear();
            mFirebaseFriends.addAll(list);
            mFirebaseAdapter.notifyDataSetChanged();
            mNoFriendTV.setVisibility(View.GONE);
        }else{
            mNoFriendTV.setVisibility(View.VISIBLE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
        mLoading.setVisibility(View.GONE);
    }

    private class MyFriendsAdapter extends ArrayAdapter<ParseUser> {


        public MyFriendsAdapter(Context context, int resource,
                                List<ParseUser> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_my_friends, parent, false);
            }

            ParseUser friend = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.textview_friend_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profilepictureview_friend);

            profilePictureView.setProfileId(friend.getString(STTConstants.FACEBOOK_ID));

            nameTextV.setText(friend.getUsername());

            return view;
        }
    }

    private class MyFirebaseFriendsAdapter extends ArrayAdapter<User> {


        public MyFirebaseFriendsAdapter(Context context, int resource,
                                List<User> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_my_friends, parent, false);
            }

            User friend = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.textview_friend_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profilepictureview_friend);

            profilePictureView.setProfileId(friend.getFacebookId());

            nameTextV.setText(friend.getUserName());

            return view;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    View.OnClickListener friendRequestListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MyFriendsActivity.this,PendingFriendRequestActivity.class));
        }
    };
}
