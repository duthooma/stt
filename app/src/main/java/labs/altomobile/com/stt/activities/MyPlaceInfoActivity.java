package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseObject;
import com.parse.ParseUser;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.objects.Place;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;

public class MyPlaceInfoActivity extends AppCompatActivity implements GoogleMap.OnMapClickListener, View.OnClickListener {


    public static float CAMERA_ZOOM = 15.0f;
    boolean mEditMode;
    private GoogleMap mMap;
    private LatLng mLatLng;
    private Marker mMarker;
    private Button mButtonAdd;
    private Place mPlace;
    private EditText mEditTextName;
    private EditText mEditTextDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_place_info);
        mButtonAdd = (Button) findViewById(R.id.button_add_place_ok);
        mButtonAdd.setOnClickListener(this);
        mEditMode = getIntent().getBooleanExtra("editMode", false);
        mEditTextName = (EditText) findViewById(R.id.editTextPlaceName);
        mEditTextDescription = (EditText) findViewById(R.id.editTextPlaceDescription);
        initialize(getIntent().getExtras());

    }

    @Override
    public void onClick(View v) {
        Log.i("LALALA", "onClick");
        if (v == mButtonAdd) {
            String name = mEditTextName.getText().toString();
            String desc = mEditTextDescription.getText().toString();

            if (mPlace == null) {
                mPlace = ParseObject.create(Place.class);
                mPlace.setUserId(ParseUser.getCurrentUser().getObjectId());

            }
            mPlace.setName(name);
            mPlace.setDescription(desc);
            mPlace.setLocation(mLatLng);
            ParseHelper.save(mPlace);
            finish();

        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Intent i = new Intent(MyPlaceInfoActivity.this, MapsActivity.class);
        i.putExtra("LAT", mLatLng.latitude);
        i.putExtra("LNG", mLatLng.longitude);
        startActivityForResult(i, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            initializePosition(data.getExtras());
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map_info))
                    .getMap();
            if (mMap != null) {
                mMap.setOnMapClickListener(this);
            }

        }
        if (mMap != null) {
            setUpMap();
        }
    }


    private void setUpMap() {
        mMap.getUiSettings().setAllGesturesEnabled(false);
        if (mMarker == null) {
            mMarker = mMap.addMarker(new MarkerOptions().position(mLatLng));
        }
        mMarker.setDraggable(false);
        mMarker.setPosition(mLatLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, CAMERA_ZOOM));
    }

    private void initialize(Bundle params) {
        if (mEditMode) {
            SerializableParseObject splace = (SerializableParseObject) params.get("placeObject");
            mPlace = (Place) splace.getParseObject("places");
            mEditTextName.setText(mPlace.getName());
            mEditTextDescription.setText(mPlace.getDescription());
            mButtonAdd.setText("Save changes");
            mLatLng = mPlace.getLatLng();
            setUpMapIfNeeded();

        } else {
            initializePosition(params);
        }

    }

    private void initializePosition(Bundle params) {
        double lat = params.getDouble("LAT");
        double lng = params.getDouble("LNG");
        mLatLng = new LatLng(lat, lng);
        setUpMapIfNeeded();
    }


}
