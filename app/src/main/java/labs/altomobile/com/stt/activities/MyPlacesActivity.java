package labs.altomobile.com.stt.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.interfaces.Refreshable;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Place;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.utils.DeleteParseObjectDialog;

public class MyPlacesActivity extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, OnRequestCallback<Place>, Refreshable {

    private ProgressBar mProgressBar;
    private List<Place> mListPlaces;
    private ListView mListView;
    private PlacesAdapter mAdapter;
    private boolean mSelectionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_places);
        mProgressBar = (ProgressBar) findViewById(R.id.places_progressBar);
        mListView = (ListView) findViewById(R.id.listView_places);
        mListView.setOnItemClickListener(this);
        if (getIntent().getExtras() != null) {
            mSelectionMode = getIntent().getExtras().getBoolean("selectMode", false);
        }
        RequestManager.GET_PLACES_OF_CURRENT_USER.onBackground(this);
        if (!mSelectionMode)
            mListView.setOnItemLongClickListener(this);

    }


    @Override
    public void onClick(View v) {
            Intent i = new Intent(MyPlacesActivity.this, MyPlaceInfoActivity.class);
            i.putExtra("LAT", 20.666667);
            i.putExtra("LNG", -103.333333);
            startActivity(i);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mSelectionMode) {
            setResult(RESULT_OK, getIntent());
            getIntent().putExtra(Place.PLACE_ID, mListPlaces.get(position).getObjectId());
            finish();

        } else { // EDIT MODE
            SerializableParseObject serParseObject = new SerializableParseObject();
            serParseObject.setParseObject(mListPlaces.get(position));
            Intent intent = new Intent(MyPlacesActivity.this, MyPlaceInfoActivity.class);
            intent.putExtra("placeObject", serParseObject);
            intent.putExtra("editMode", true);
            startActivity(intent);

        }
    }

    @Override
    public void onRequestBegin() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestFinished(List<Place> list, ParseObjectMap parseObjectMapmap) {
        mListPlaces = list;
        mAdapter = new PlacesAdapter(this, mListPlaces);
        mListView.setAdapter(mAdapter);
        mProgressBar.setVisibility(View.GONE);


    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ParseObject toDelete = mListPlaces.get(position);
        DeleteParseObjectDialog deleteParseObjectDialog = new DeleteParseObjectDialog(this, toDelete, this);
        deleteParseObjectDialog.show();
        return true;
    }

    @Override
    public void refresh() {
        RequestManager.GET_PLACES_OF_CURRENT_USER.onBackground(this);
    }

    class PlacesAdapter extends ArrayAdapter<Place> {
        PlacesAdapter(Context context, List<Place> places) {
            super(context, R.layout.list_places_layout, places);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Place place = getItem(position);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View customView = inflater.inflate(R.layout.list_places_layout, parent, false);
            TextView title = (TextView) customView.findViewById(R.id.text_place_row_title);
            TextView description = (TextView) customView.findViewById(R.id.text_place_row_description);
            title.setText(place.getName());
            description.setText(place.getDescription());
            return customView;

        }
    }
}
