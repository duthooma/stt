package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.widget.ProfilePictureView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.parse.ParseUser;

import java.lang.reflect.Field;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.common.OnNavigationPageListener;
import labs.altomobile.com.stt.common.SlidingTabLayout;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.utils.BuildInfo;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.UserLevel;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.RequestParams;

public class NavigationMenuActivity extends AppCompatActivity implements OnNavigationPageListener, View.OnClickListener {

    private FloatingActionMenu mfloatingActionMenuFeed;
    private FloatingActionButton mfloatingActionMenuChallenges;
    private FloatingActionMenu mfloatingActionMenuGroups;

    private FloatingActionButton mfloatingActionMenuMyTraining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_menu);

        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_navigation_feed,
                null);
        ProfilePictureView pictureView = (ProfilePictureView) actionBarLayout.findViewById(R.id.action_bar_profile_view);
        final TextView userLevelTextView = (TextView)actionBarLayout.findViewById(R.id.tv_user_level);
        pictureView.setProfileId(ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID));
        RequestParams params = RequestParams.create();
        params.add("user_idd", ParseUser.getCurrentUser().getObjectId());

        RequestManager.GET_USER_LEVEL_OF_USER.onBackground(params, new OnRequestCallback<UserLevel>() {
            @Override
            public void onRequestBegin() {

            }

            @Override
            public void onRequestFinished(List<UserLevel> list, ParseObjectMap parseObjectMapmap) {
                if (list != null && list.size() > 0) {
                    userLevelTextView.setText(String.valueOf(list.get(0).getLevel()));
                }
            }
        });

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setCustomView(actionBarLayout);

       // SlidingTabsBasicFragment slidingTabsBasicFragment = (SlidingTabsBasicFragment) getSupportFragmentManager().findFragmentById(R.id.slidingTabFragment);

       // slidingTabsBasicFragment.setOnNavigationPageListener(this);

        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setOnNavigationPageListener(this);



        mfloatingActionMenuFeed = (FloatingActionMenu)findViewById(R.id.fab_feed);
        findViewById(R.id.menu_fab_feed_add_friends).setOnClickListener(this);
        findViewById(R.id.menu_fab_feed_friend_request).setOnClickListener(this);
        findViewById(R.id.menu_fab_feed_my_friends).setOnClickListener(this);
        mfloatingActionMenuGroups = (FloatingActionMenu)findViewById(R.id.fab_groups);
        findViewById(R.id.menu_fab_groups_item_create).setOnClickListener(this);
        findViewById(R.id.menu_fab_groups_item_see_request).setOnClickListener(this);

        mfloatingActionMenuChallenges = (FloatingActionButton)findViewById(R.id.fab_challenge);
        mfloatingActionMenuChallenges.setOnClickListener(this);


        mfloatingActionMenuMyTraining = (FloatingActionButton)findViewById(R.id.fab_training);
        mfloatingActionMenuMyTraining.setOnClickListener(this);


        mfloatingActionMenuFeed.setClosedOnTouchOutside(true);
        mfloatingActionMenuGroups.setClosedOnTouchOutside(true);

        OnMenuToggleListener menuToggleListener = new OnMenuToggleListener((ImageView)findViewById(R.id.fader_imageView));
        mfloatingActionMenuFeed.setOnMenuToggleListener(menuToggleListener );
        mfloatingActionMenuGroups.setOnMenuToggleListener(menuToggleListener);

        onNavigationPageSelected(0);

        /**
        Hacking the sHasPermanentMenuKey attribute
        */
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
        }

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.cheats_item).setVisible(BuildInfo.isDebug());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.my_friends:

                intent = new Intent(NavigationMenuActivity.this, MyFriendsActivity.class);
                startActivity(intent);

                break;
           /* case R.id.my_places:

                intent = new Intent(NavigationMenuActivity.this, MyPlacesActivity.class);
                startActivity(intent);

                return true;*/
            case R.id.my_pending_friend_request: {
                intent = new Intent(NavigationMenuActivity.this, PendingFriendRequestActivity.class);
                startActivity(intent);

                break;
            }
           
              case R.id.cheats_item:

                intent = new Intent(NavigationMenuActivity.this, CheatsActivity.class);
                startActivity(intent);

                break;
            case R.id.logout_facebook:

                intent = new Intent(NavigationMenuActivity.this, LogoutFacebookActivity.class);
                startActivity(intent);

                break;


            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onNavigationPageSelected(int position) {

        mfloatingActionMenuFeed.setVisibility(View.INVISIBLE);
        mfloatingActionMenuGroups.setVisibility(View.INVISIBLE);
        mfloatingActionMenuChallenges.setVisibility(View.INVISIBLE);
        mfloatingActionMenuMyTraining.setVisibility(View.INVISIBLE);

        switch (position)
        {
            case 0:
            {
                mfloatingActionMenuFeed.setVisibility(View.VISIBLE);

            }
            break;
            case 1:
            {
                mfloatingActionMenuGroups.setVisibility(View.VISIBLE);
            }
            break;
            case 2:
            {
                mfloatingActionMenuChallenges.setVisibility(View.VISIBLE);
            }
            break;
            case 3:
            {
                mfloatingActionMenuMyTraining.setVisibility(View.VISIBLE);
            }
            break;
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {

            case R.id.menu_fab_feed_add_friends:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, InviteFriendsActivity.class);
                startActivity(intent);


            }
            break;
            case R.id.menu_fab_feed_friend_request:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, PendingFriendRequestActivity.class);
                startActivity(intent);


            }
            break;
            case R.id.menu_fab_feed_my_friends:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, MyFriendsActivity.class);
                startActivity(intent);
            }
            break;

            case R.id.menu_fab_groups_item_create:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, CreateGroupActivity.class);
                startActivity(intent);


            }
            break;
            case R.id.menu_fab_groups_item_see_request:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, SeeGroupDetailActivity.class);
                startActivity(intent);


            }
            break;
            case R.id.fab_challenge:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, CreateChallengeActivity.class);
                startActivity(intent);


            }
            break;
            case R.id.fab_training:
            {
                Intent intent = new Intent(NavigationMenuActivity.this, AddTrainingSelectSportActivity.class);
                startActivity(intent);


            }
            break;

        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        //If facebook or parse user/session is null, this activity is closed
        if(AccessToken.getCurrentAccessToken() == null) {
            finish();
        }
    }

     class OnMenuToggleListener implements FloatingActionMenu.OnMenuToggleListener,View.OnClickListener {

        ImageView fader;
        OnMenuToggleListener(ImageView fader)
        {
            this.fader = fader;
            fader.setOnClickListener(this);
        }
        public void onMenuToggle(boolean toggle) {
            if(toggle)
            {
                fader.setVisibility(View.VISIBLE);
            }
            else
            {
                fader.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        public void onClick(View v) {
            mfloatingActionMenuFeed.close(true);
            mfloatingActionMenuGroups.close(true);

        }
    }


}
