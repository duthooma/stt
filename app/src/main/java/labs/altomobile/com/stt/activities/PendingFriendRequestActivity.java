package labs.altomobile.com.stt.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.login.widget.ProfilePictureView;
import com.firebase.client.Firebase;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.FriendRequestHelper;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetMyFriendsRequest;

public class PendingFriendRequestActivity extends AppCompatActivity implements OnRequestCallback<FriendRequest>, SwipeRefreshLayout.OnRefreshListener, OnFirebaseRequestCallback<User> {

    private TextView noPendingRequestTView;
    private ListView pendingRequestListV;
    private ProgressBar mLoading;
    private PendingFriendRequestAdapter adapter;
    private FirebaseFriendRequestAdapter adapterFirebase;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_pending_friend_request);

        Log.w(STTConstants.LOG_TAG, "About to launch Session.openActiveSession()");


        noPendingRequestTView = (TextView) findViewById(R.id.no_friend_request_to_display);
        noPendingRequestTView.setVisibility(View.GONE);

        pendingRequestListV = (ListView) findViewById(R.id.list_pending_request);


        mLoading = (ProgressBar) findViewById(R.id.loading);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        if (STTConstants.USE_FIREBASE){
            new GetMyFriendsRequest().onBackground(this);
        } else {
            RequestManager.GET_PENDING_FRIEND_REQUEST_OF_CURRENT_USER.onBackground(this);
        }
    }

    @Override
    public void onRefresh() {
        if (STTConstants.USE_FIREBASE){
            new GetMyFriendsRequest().onBackground(this);
        } else {
            RequestManager.GET_PENDING_FRIEND_REQUEST_OF_CURRENT_USER.onBackground(this);
        }
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<labs.altomobile.com.stt.objects.FriendRequest> list, ParseObjectMap parseObjectMapmap) {
        mLoading.setVisibility(View.GONE);

        mSwipeRefreshLayout.setRefreshing(false);
        if(list.isEmpty())
        {
            noPendingRequestTView.setVisibility(View.VISIBLE);
            pendingRequestListV.setAdapter(adapter);
            if(adapter!=null)
                adapter.clear();

        }
        else {

            noPendingRequestTView.setVisibility(View.GONE);

            adapter = new PendingFriendRequestAdapter(getApplicationContext(), R.layout.row_friendrequest, list, parseObjectMapmap);

            pendingRequestListV.setAdapter(adapter);

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestFinished(List<User> list) {
        mLoading.setVisibility(View.GONE);

        mSwipeRefreshLayout.setRefreshing(false);
        if(list.isEmpty())
        {
            noPendingRequestTView.setVisibility(View.VISIBLE);
            pendingRequestListV.setAdapter(adapter);
            if(adapter!=null)
                adapter.clear();

        }
        else {

            noPendingRequestTView.setVisibility(View.GONE);

            adapterFirebase = new FirebaseFriendRequestAdapter(getApplicationContext(), R.layout.row_friendrequest, list);

            pendingRequestListV.setAdapter(adapterFirebase);

        }
    }

    private class PendingFriendRequestAdapter extends ArrayAdapter<labs.altomobile.com.stt.objects.FriendRequest> {

        private ParseObjectMap mParseObjectMap;

        public PendingFriendRequestAdapter(Context context, int resource,
                                           List<labs.altomobile.com.stt.objects.FriendRequest> objects,
                                           ParseObjectMap parseObjectMap) {
            super(context, resource, objects);
            mParseObjectMap = parseObjectMap;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_friendrequest, parent, false);
            }

            final FriendRequest friendRequest = getItem(position);
            ParseUser friend = (ParseUser)mParseObjectMap.get(friendRequest.getUser1());

            TextView nameTextV = (TextView) view.findViewById(R.id.friendrequest_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profilepictureview_friend);

            profilePictureView.setProfileId(friend.getString(STTConstants.FACEBOOK_ID));
            nameTextV.setText(friend.getUsername());
            final Button btnAccept = (Button) view.findViewById(R.id.btn_accept);

            btnAccept.setOnClickListener(new OnClickListener() {

                //Run when button is clicked
                @Override
                public void onClick(View v) {

                    FriendRequestHelper.getInstance().acceptDuplicatedFriendRequest(friendRequest.getUser1(), friendRequest.getUser2());

                    friendRequest.put(FriendRequest.PARSEFIELD_ACCEPTATION_STATUS, 1);
                    ParseHelper.save(friendRequest);


                    Toast.makeText(PendingFriendRequestActivity.this, getString(R.string.request_accepted),
                            Toast.LENGTH_SHORT).show();

                    adapter.notifyDataSetChanged();
                    //btnAccept.setVisibility(View.GONE);
                    btnAccept.setEnabled(false);
                    btnAccept.setTextColor(ContextCompat.getColor(PendingFriendRequestActivity.this, R.color.fontGrey));
                    btnAccept.setText(getString(R.string.accepted));


                }
            });

            return view;
        }
    }

    private class FirebaseFriendRequestAdapter extends ArrayAdapter<User> {

        public FirebaseFriendRequestAdapter(Context context, int resource,
                                           List<User> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_friendrequest, parent, false);
            }

            User friend = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.friendrequest_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profilepictureview_friend);

            profilePictureView.setProfileId(friend.getFacebookId());
            nameTextV.setText(friend.getUserName());
            final Button btnAccept = (Button) view.findViewById(R.id.btn_accept);
            final String friendUid = friend.getUid();

            btnAccept.setOnClickListener(new OnClickListener() {

                //Run when button is clicked
                @Override
                public void onClick(View v) {

                    FirebaseHelper.acceptFriendRequest(friendUid);


                    Toast.makeText(PendingFriendRequestActivity.this, getString(R.string.request_accepted),
                            Toast.LENGTH_SHORT).show();

                    adapterFirebase.notifyDataSetChanged();
                    //btnAccept.setVisibility(View.GONE);
                    btnAccept.setEnabled(false);
                    btnAccept.setTextColor(ContextCompat.getColor(PendingFriendRequestActivity.this, R.color.fontGrey));
                    btnAccept.setText(getString(R.string.accepted));


                }
            });

            return view;
        }
    }
}
