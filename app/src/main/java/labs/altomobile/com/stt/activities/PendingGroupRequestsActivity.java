package labs.altomobile.com.stt.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;


public class PendingGroupRequestsActivity extends Activity implements OnRequestCallback<Group> {

    private TextView noPendingRequestTV;
    private ListView pendingRequestLV;
    private List<Group> pendingGroups;
    private ParseObjectMap parseObjectMap;

    private ProgressBar spinner;
    private PendingTrainingRequestAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pending_training_request);

        Log.w(STTConstants.LOG_TAG, "About to launch Session.openActiveSession()");


        noPendingRequestTV = (TextView) findViewById(R.id.tv_no_training_request);
        noPendingRequestTV.setVisibility(View.GONE);

        pendingRequestLV = (ListView) findViewById(R.id.list_pending_request);


        spinner = (ProgressBar) findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);


        pendingGroups = new ArrayList<Group>();
        adapter = new PendingTrainingRequestAdapter(getApplicationContext(), R.layout.row_training_request, pendingGroups);

        pendingRequestLV.setAdapter(adapter);
        RequestManager.GET_PENDING_GROUP_REQUEST_OF_CURRENT_USER.onBackground(this);


    }

    private void acceptGroup(Group group) { //TODO group isn´ t used here. why?
        ParseObject groupVsUser = parseObjectMap.get("group.getObjectId()");
        groupVsUser.put(FriendRequest.PARSEFIELD_ACCEPTATION_STATUS, 1);
        ParseHelper.save(groupVsUser);
    }

    @Override
    public void onRequestBegin() {
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestFinished(List<Group> list, ParseObjectMap parseObjectMapmap) {
        spinner.setVisibility(View.GONE);
        pendingGroups.clear();
        pendingGroups.addAll(list);
        parseObjectMap = parseObjectMapmap;
        adapter.notifyDataSetChanged();
    }

    private class PendingTrainingRequestAdapter extends ArrayAdapter<Group> {


        public PendingTrainingRequestAdapter(Context context, int resource,
                                             List<Group> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_training_request, parent, false);
            }

            final Group group = getItem(position);


            TextView nameTextV = (TextView) view.findViewById(R.id.tv_training_name);
            Button btnAccept = (Button) view.findViewById(R.id.btn_accept);
            btnAccept.setText("Accept");
            btnAccept.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(PendingGroupRequestsActivity.this, "Request accepted",
                            Toast.LENGTH_LONG).show();

                    acceptGroup(group);
                    ParseHelper.save(group);
                    pendingGroups.remove(group);
                    adapter.notifyDataSetChanged();

                }
            });

            nameTextV.setText(group.getName());

            return view;
        }
    }
}
