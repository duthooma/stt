package labs.altomobile.com.stt.activities;

//[MIGRATION]
public class SearchImageActivity{ //extends AppCompatActivity {
/*
    private final String maxImageResult = "8"; //for parameter 'rsz=[1-8]'.

	EditText etQuery;
	GridView gvResults;
	Button btnSearch;
	Button btnFilters;
	ArrayList<ImageResult> imageResults = new ArrayList<ImageResult>();
	ImageResultsArrayAdapter imageAdapter;
	
	String size;
	String color;
	String type;
	String siteFilter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_image);
		
		setupViews();
		imageAdapter = new ImageResultsArrayAdapter(this, imageResults);
		gvResults.setAdapter(imageAdapter);
		gvResults.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View parent, int position,
					long rowId) {
				Intent intent = new Intent();
				ImageResult imageResult = imageResults.get(position);
				intent.putExtra("result", imageResult);
                setResult(Activity.RESULT_OK,intent);
				finish();
			}
		});
	}

	private void setupViews() {
		etQuery = (EditText) findViewById(R.id.etQuery);
		gvResults = (GridView) findViewById(R.id.gvResults);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnFilters = (Button) findViewById(R.id.btnFilters);
		
	}
	
	public void onFilter(View v) {
		//Intent i = new Intent(getApplicationContext(), ImageFilterActivity.class);
		///startActivityForResult(i, 1);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		  if (requestCode == 1) {

		     if(resultCode == RESULT_OK){      
		         size = data.getStringExtra("size"); 
		         color = data.getStringExtra("color");
		         type = data.getStringExtra("type");
		         siteFilter = data.getStringExtra("siteFilter");
		     }
		     if (resultCode == RESULT_CANCELED) {    
		         size = "";
		         color = "";
		         type = "";
		         siteFilter = "";
		     }
		  }
		}

	public void onImageSearch(View v) {
		String query = etQuery.getText().toString();
        
        if (!query.isEmpty()) {
            Toast.makeText(this, "Searching for " + query, Toast.LENGTH_LONG).show();

            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etQuery.getWindowToken(), 0);

            AsyncHttpClient client = new AsyncHttpClient();
            String queryString = "https://ajax.googleapis.com/ajax/services/search/images?rsz="+maxImageResult+"&"
                    + "start=" + 0 + "&v=1.0&q=" + Uri.encode(query);
            if (!isNullOrEmpty(size)) {
                queryString = queryString + "&imgsz=" + size;
            }
            if (!isNullOrEmpty(color)) {
                queryString = queryString + "&imgcolor=" + color;
            }
            if (!isNullOrEmpty(type)) {
                queryString = queryString + "&imgtype=" + type;
            }
            if (!isNullOrEmpty(siteFilter)) {
                queryString = queryString + "&as_sitesearch=" + siteFilter;
            }
            client.get(queryString, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    JSONArray imageJsonResults = null;
                    try {
                        imageJsonResults = response.getJSONObject("responseData").getJSONArray("results");
                        imageResults.clear();
                        imageAdapter.addAll(ImageResult.fromJSONArray(imageJsonResults));
                        Log.d("DEBUG", imageResults.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            showError(etQuery);
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	boolean isNullOrEmpty(String s) {
		return (s == null || s.isEmpty());
	}


    private void showError(EditText editText) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        editText.startAnimation(shake); }
*/
}
