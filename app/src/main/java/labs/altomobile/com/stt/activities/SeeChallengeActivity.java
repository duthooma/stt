package labs.altomobile.com.stt.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.fragments.ChallengeDetailsFragment;

public class SeeChallengeActivity extends AppCompatActivity {

    private static final String SEE_DETAILS_FRAGMENT = "SEE_DETAILS_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_challenge);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null){

            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            ChallengeDetailsFragment challengeFragment = new ChallengeDetailsFragment();
            bundle.putInt(STTConstants.DISPLAY_OPTION, STTConstants.DISPLAY_OPTION_SEE_CHALLENGE);

            //Challenge object is supposed to be already in the bundle object. Will check in the Fragment if the Challenge object is there
            challengeFragment.setArguments(bundle);

            ft.replace(R.id.fragment_container, challengeFragment, SEE_DETAILS_FRAGMENT);
            ft.commit();

        }else{
            Log.w(STTConstants.LOG_TAG, " WARNING :: " + SeeChallengeActivity.class.getSimpleName() + " :: Bundle is NULL");
        }
    }
}
