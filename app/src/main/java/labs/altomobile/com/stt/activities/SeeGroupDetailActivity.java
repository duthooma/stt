package labs.altomobile.com.stt.activities;

import android.app.Activity;
import android.os.Bundle;

import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;

public class SeeGroupDetailActivity extends Activity implements OnRequestCallback<ParseUser> {

    /*
    private Group group;
    private TextView trainingNameTV;
    private TextView footer1TV;
    private TextView footer2TV;
    private Button inviteBtn;
    private ListView peopleLV;

    private ProgressBar progressBar;

    private List<ParseUser> people;

    private PeopleAdapter adapter;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_see_group_detail);
/*
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            SerializableParseObject sgroup = (SerializableParseObject) bundle.get(CreateGroupActivity.GROUP_SERIALIZABLE);
            if (sgroup != null) {
                group = (Group) sgroup.getParseObject(ParseConstants.CLASS_NAME_GROUP);
                if (group != null) {

                    progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                    trainingNameTV = (TextView) findViewById(R.id.training_name);
                    trainingNameTV.setText(group.getName());
                    peopleLV = (ListView) findViewById(R.id.list_people);
                    inviteBtn = (Button) findViewById(R.id.btn_invite);

                    inviteBtn.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            Log.i("LALALA", "invite");
                            Intent intent = new Intent(SeeGroupDetailActivity.this, InviteFriendsToGroupActivity.class);
                            intent.putExtra(Group.GROUP_ID, group.getObjectId());
                            startActivity(intent);
                        }
                    });

                    boolean justCreated = false;
                    if (bundle.get("justCreated") != null) {
                        justCreated = (Boolean) bundle.get("justCreated");
                    }
                    people = new ArrayList<ParseUser>();
                    if (justCreated) {
                        //we just created this group. So there is no user linked to it except the currentUser
                        if (bundle.get("currentUser") != null) {
                            people.add(ParseUser.getCurrentUser());
                        }
                    } else {

                        RequestManager.GET_USER_LIST_OF_A_GIVEN_GROUP.onBackground(RequestParams.create().add(Group.GROUP_ID, group.getObjectId()), this);
                    }
                    adapter = new PeopleAdapter(getApplicationContext(), R.layout.row_people, people);
                    peopleLV.setAdapter(adapter);

                } else {
                    TextView somethingWentWrongTV = (TextView) findViewById(R.id.something_went_wrong);
                    somethingWentWrongTV.setVisibility(View.GONE);
                    Log.e(Constants.LOG_TAG, "Error: SeeGroupActivity: we couldn't retrieve from intent the Group Object");
                }
            }else {
                TextView somethingWentWrongTV = (TextView) findViewById(R.id.something_went_wrong);
                somethingWentWrongTV.setVisibility(View.GONE);
                Log.e(Constants.LOG_TAG, "Error: SeeGroupActivity: we couldn't retrieve from intent the Group Object");
            }
        } else {
            TextView somethingWentWrongTV = (TextView) findViewById(R.id.something_went_wrong);
            somethingWentWrongTV.setVisibility(View.GONE);
            Log.e(Constants.LOG_TAG, "Error: SeeGroupActivity: we couldn't retrieve from intent the Group Object");
        } */
    }

    @Override
    public void onRequestBegin() {
        //progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestFinished(List<ParseUser> list, ParseObjectMap parseObjectMapmap) {
        /*people.clear();
        people.addAll(list);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);*/
    }
/*
    private class PeopleAdapter extends ArrayAdapter<ParseUser> {


        public PeopleAdapter(Context context, int resource,
                             List<ParseUser> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_people, parent, false);
            }

            ParseUser parseUser = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.friendrequest_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.friendrequest_picture);
            profilePictureView.setProfileId(parseUser.getString(Constants.FACEBOOK_ID));

            nameTextV.setText(parseUser.getUsername());

            return view;
        }
    }*/
}
