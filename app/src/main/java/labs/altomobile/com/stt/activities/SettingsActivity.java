package labs.altomobile.com.stt.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import labs.altomobile.com.stt.R;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{

    TextView aboutTextView;
    TextView contactUsTextView;
    TextView logOutTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aboutTextView = (TextView)findViewById(R.id.textview_about);
        contactUsTextView = (TextView)findViewById(R.id.textview_contactus);
        logOutTextView = (TextView)findViewById(R.id.textview_logout);

        aboutTextView.setOnClickListener(this);
        contactUsTextView.setOnClickListener(this);
        logOutTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == aboutTextView){
            startActivity(new Intent(SettingsActivity.this, SettingsAboutActivity.class));
        }else if (v == contactUsTextView){
            startActivity(new Intent(SettingsActivity.this, FeedbackActivity.class));
        }else if (v == logOutTextView){
            startActivity(new Intent(SettingsActivity.this, SettingsLogOutActivity.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
