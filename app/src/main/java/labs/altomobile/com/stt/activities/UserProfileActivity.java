package labs.altomobile.com.stt.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import labs.altomobile.com.stt.R;

public class UserProfileActivity extends AppCompatActivity {

    public static String USER_ID = "USER_ID";

    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set up the action bar Up Navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_user_profile);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey(USER_ID)){
            mUserId = bundle.getString(USER_ID);

            Toast.makeText(this,"User ID = " + mUserId,Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(this,getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
