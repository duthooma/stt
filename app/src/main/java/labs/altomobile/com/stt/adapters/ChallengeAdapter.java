package labs.altomobile.com.stt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by MaxenceD on 5/31/15.
 */
public class  ChallengeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    static private List<Challenge> mList;
    static private ParseObjectMap mParseObjectMap;
    static private Context mContext;
    static private boolean mViewAsOwner = false;

    public ChallengeAdapter(List<Challenge> list){
        mList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_feed_challenge, parent, false);

        return new FeedAdapter.ChallengesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Challenge challenge = mList.get(position);
        FeedAdapter.ChallengesViewHolder challengesViewHolder = (FeedAdapter.ChallengesViewHolder)holder;

        ParseUser from = challenge.getFrom();
        ParseUser to;
        if (mViewAsOwner){
            to = ParseUser.getCurrentUser();
        }else {
            to = challenge.getTo();
        }

        challengesViewHolder.setData(challenge,from,to);
        challengesViewHolder.setInfo(mContext, challenge, from, to);
    }

    @Override
    public int getItemCount() {
        if(mList==null)
            return 0;
        return mList.size();
    }

    public void setViewAsOwner(boolean viewAsOwner){
        mViewAsOwner = viewAsOwner;
    }

    public void setContext(Context context)
    {
        mContext = context;
    }
    public void setParseObjectMap(ParseObjectMap parseObjectMap)
    {
        mParseObjectMap = parseObjectMap;
    }

}
