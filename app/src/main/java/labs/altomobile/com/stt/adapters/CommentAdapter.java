package labs.altomobile.com.stt.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.Comment;

/**
 * Created by danielmonterocervantes on 09/06/15.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder> {

    private List<Comment> mList;


    public CommentAdapter()
    {
        mList = new ArrayList<>();
    }
    public void updateInfo(List<Comment> list){
        mList=list;
    }

    public static class CommentHolder extends RecyclerView.ViewHolder {

        public ProfilePictureView mPictureViewUser;
        public TextView mTitle;
        public TextView mContent;

        CommentHolder(View v)
        {
            super(v);
            mPictureViewUser = (ProfilePictureView)v.findViewById(R.id.comment_profile_picture);
            mTitle = (TextView)v.findViewById(R.id.comment_title);
            mContent = (TextView)v.findViewById(R.id.comment_text);

        }
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        Comment comment = mList.get(position);
        holder.mTitle.setText(comment.getUser().getUsername());
        holder.mContent.setText(comment.getContent());
        holder.mPictureViewUser.setProfileId(comment.getUser().getString(STTConstants.FACEBOOK_ID));

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_comment, parent, false);
        return new CommentHolder(v);
    }
}
