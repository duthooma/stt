package labs.altomobile.com.stt.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.widget.ProfilePictureView;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.SeeChallengeActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Feed;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Place;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.utils.STTUtils;


public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Feed> mList;
    private ParseObjectMap mParseObjectMap;
    static private Context mContext;
    private static final int FEED_CHALLENGE = 1;
    private static final int FEED_TRAINING = 3;


    public static class TrainingViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mName;
        public TextView mDate;
        public ProfilePictureView mPictureViewUser1;

        TrainingViewHolder(View v)
        {
            super(v);
            mImageView = (ImageView)v.findViewById(R.id.imageViewSport);
            mName = (TextView) v.findViewById(R.id.textViewName);
            mDate =(TextView) v.findViewById(R.id.textViewTrainingDate);
            mPictureViewUser1 = (ProfilePictureView) v.findViewById(R.id.feed_row_user1);

        }
    }


    public FeedAdapter(List<Feed> list)
    {
        mList = list;

    }

    public void setContext(Context context)
    {
        mContext = context;
    }
    public void setParseObjectMap(ParseObjectMap parseObjectMap)
    {
        mParseObjectMap = parseObjectMap;
    }

    @Override
    public int getItemViewType(int position) {
        Feed object = mList.get(position);
        switch (object.getRelatedObjectClassName())
        {
            case ParseConstants.CLASS_NAME_TRAINING:
            {
                return FEED_TRAINING;
            }
            case ParseConstants.CLASS_NAME_CHALLENGE:
            {
                return FEED_CHALLENGE;
            }
        }
        return -1;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                //SET INFORMATION FROM mList to the view
        Feed object = mList.get(position);
        switch (getItemViewType(position))
        {
            case FEED_TRAINING:
            {
                TrainingViewHolder trainingViewHolder = (TrainingViewHolder)holder;
                Training training = object.getTraining();
                ParseUser parseUser = training.getUser();
                trainingViewHolder.mName.setText(parseUser.getUsername());
                if(mContext!=null) {
                    String resourceName = STTConstants.FILE_NAME_ICONSPORT + training.getSportId();
                    int resourceId = STTUtils.getResourceId(mContext, resourceName, STTConstants.RESOURCE_DRAWABLE);
                    Glide.with(mContext).load(resourceId).into(trainingViewHolder.mImageView);
                }
                String description;
                description = "Trained "+ training.getDateStr(mContext);
                if(training.getPlaceId()!=null && !training.getPlaceId().isEmpty())
                {
                    Place place = mParseObjectMap.getAsPlaces(training.getPlaceId());
                    if(place !=null)
                        description+= "\nIn: "+ place.getName();
                }
                trainingViewHolder.mDate.setText(description);
                trainingViewHolder.mPictureViewUser1.setProfileId(parseUser.getString(STTConstants.FACEBOOK_ID));

            }
            break;
            case FEED_CHALLENGE:
            {

                ChallengesViewHolder challengesViewHolder = (ChallengesViewHolder) holder;

                Challenge challenge = object.getChallenge();
                ParseUser from = challenge.getFrom();
                ParseUser to = challenge.getTo();

                challengesViewHolder.setData(challenge,from,to);
                challengesViewHolder.setInfo(mContext, challenge,from,to);
                challengesViewHolder.setContext(mContext);

            }
            break;

        }
    }

    @Override
    public int getItemCount() {
        if(mList==null)
            return 0;
        return mList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        switch (viewType)
        {
            case FEED_CHALLENGE:
            {
                 v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_feed_challenge, parent, false);
                return new ChallengesViewHolder(v);


            }
            case FEED_TRAINING:
            {
                 v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_feed_training, parent, false);
                return new TrainingViewHolder(v);


            }
        }
        return null;
    }

    public static class ChallengesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView mChallengeStatus;
        public ProfilePictureView mProfilePictureViewTo;
        public TextView textViewTo;
        public ImageView sportImageView;
        public TextView nameTV;
        public TextView descTV;
        public TextView dateTV;
        public ImageView rewardIV;
        public TextView rewardCommentTV;
        public View separatorTV;

        private Challenge lChallenge;
        private ParseUser lFrom;
        private ParseUser lTo;


        public RelativeLayout layout;

        public ChallengesViewHolder(View v)
        {
            super(v);
            v.setOnClickListener(this);

            mChallengeStatus = (TextView)v.findViewById(R.id.textview_challenge_status);

            mProfilePictureViewTo = (ProfilePictureView)v.findViewById(R.id.imageview_friend_to_challenge);
            textViewTo = (TextView) v.findViewById(R.id.textview_to_name);

            v.findViewById(R.id.textview_separator_challenge).setVisibility(View.VISIBLE);

            sportImageView = (ImageView) v.findViewById(R.id.imageView_sport_ic);

            nameTV = (TextView) v.findViewById(R.id.textview_challenge_name);

            descTV = (TextView) v.findViewById(R.id.textview_challenge_desc);

            dateTV = (TextView) v.findViewById(R.id.textview_challenge_date);

            layout = (RelativeLayout) v.findViewById(R.id.layout_full_row);

            rewardIV = (ImageView)v.findViewById(R.id.imageview_reward_icon);

            rewardCommentTV = (TextView)v.findViewById(R.id.textview_reward_comment);

            separatorTV = v.findViewById(R.id.textview_separator2_challenge);

        }

        public void setData(Challenge challenge, ParseUser from, ParseUser to)
        {
            lChallenge = challenge;
            lFrom = from;
            lTo = to;
        }

        public void setInfo(Context context, Challenge challenge,ParseUser from, ParseUser to)
        {

            switch (challenge.getResult()){

                case Challenge.CHALLENGE_RESULT_PENDING:
                    mChallengeStatus.setText(mContext.getString(R.string.new_challenge_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_light_blue);
                    break;

                case Challenge.CHALLENGE_RESULT_ACCEPTED:
                    mChallengeStatus.setText(mContext.getString(R.string.challenge_accepted_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_blue);
                    break;

                case Challenge.CHALLENGE_RESULT_REFUSED:
                    mChallengeStatus.setText(mContext.getString(R.string.challenge_rejected_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

                case Challenge.CHALLENGE_RESULT_SUCCEED:
                    mChallengeStatus.setText(mContext.getString(R.string.challenge_succeed_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_green);
                    break;

                case Challenge.CHALLENGE_RESULT_FAILED:
                    mChallengeStatus.setText(mContext.getString(R.string.challenge_failed_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

                case Challenge.CHALLENGE_RESULT_EXPIRED:
                    mChallengeStatus.setText(mContext.getString(R.string.challenge_expired_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

            }
            String text = "<b>" + to.getUsername() + "</b> " + mContext.getString(R.string.is_challenged_by) + " <b>" + from.getUsername() + "</b>";
            textViewTo.setText(Html.fromHtml(text));


            mProfilePictureViewTo.setProfileId(to.getString(STTConstants.FACEBOOK_ID));

            String sportResourceId = STTConstants.FILE_NAME_ICONSPORT + challenge.getSport();
            Glide.with(mContext).load(STTUtils.getResourceId(context, sportResourceId, STTConstants.RESOURCE_DRAWABLE)).into(sportImageView);

            nameTV.setText(challenge.getName());
            descTV.setText(challenge.getDesc());
            dateTV.setText(challenge.getTimeLeftStr(context));

            if (challenge.getReward() >= 0) {

                String rewardResourceId = STTConstants.FILE_NAME_ICONREWARD + challenge.getReward();

                Glide.with(context).load(STTUtils.getResourceId(context, rewardResourceId, STTConstants.RESOURCE_DRAWABLE)).into(rewardIV);

                rewardCommentTV.setText(challenge.getRewardComment());

                separatorTV.setVisibility(View.VISIBLE);
                rewardIV.setVisibility(View.VISIBLE);
                rewardCommentTV.setVisibility(View.VISIBLE);
            }
        }


        public void setContext(Context context){
            mContext = context;
        }

        private void redirectToChallenge(){
            Intent intent = new Intent(mContext, SeeChallengeActivity.class);

            Bundle args = new Bundle();

            //put Challenge in the ParseObjectMap
            args.putSerializable(ParseConstants.CLASS_NAME_CHALLENGE, lChallenge.getObjectId());
            ParseObjectMap.getInstance().add(lChallenge);


            intent.putExtras(args);
            mContext.startActivity(intent);
        }

        @Override
        public void onClick(View view) {
            redirectToChallenge();
        }
    }
}
