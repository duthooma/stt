package labs.altomobile.com.stt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.User;

/**
 * Created by Flavio on 08/04/2016.
 */
public class FirebaseChallengeAdapater extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        static private List<ChallengeFirebase> mList;
        static private Context mContext;
        static private boolean mViewAsOwner = false;

        public FirebaseChallengeAdapater(List<ChallengeFirebase> list){
            mList = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_feed_challenge, parent, false);

            return new FirebaseFeedAdapter.FirebaseChallengesViewHolder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ChallengeFirebase challenge = mList.get(position);
            FirebaseFeedAdapter.FirebaseChallengesViewHolder challengesViewHolder = (FirebaseFeedAdapter.FirebaseChallengesViewHolder)holder;

            String from = challenge.getFrom();
            String to;
            if (mViewAsOwner){
                to = FirebaseHelper.getUserUid();
            }else {
                to = challenge.getTo();
            }

            challengesViewHolder.setData(challenge);
            challengesViewHolder.setInfo(mContext, challenge, from, to);
        }

        @Override
        public int getItemCount() {
            if(mList==null)
                return 0;
            return mList.size();
        }

        public void setViewAsOwner(boolean viewAsOwner){
            mViewAsOwner = viewAsOwner;
        }

        public void setContext(Context context)
        {
            mContext = context;
        }

    }
