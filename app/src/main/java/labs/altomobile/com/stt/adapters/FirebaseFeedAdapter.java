package labs.altomobile.com.stt.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.widget.ProfilePictureView;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.SeeChallengeActivity;
import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.objects.Place;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.FeedFirebase;
import labs.altomobile.com.stt.objects.firebase.TrainingFirebase;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.requests.firebase.GetAChallenge;
import labs.altomobile.com.stt.requests.firebase.GetAnUser;
import labs.altomobile.com.stt.utils.STTUtils;

/**
 * Created by Flavio on 08/04/2016.
 */
public class FirebaseFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<FeedFirebase> mList;
    static private Context mContext;

    public static class TrainingViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mName;
        public TextView mDate;
        public ProfilePictureView mPictureViewUser1;

        TrainingViewHolder(View v)
        {
            super(v);
            mImageView = (ImageView)v.findViewById(R.id.imageViewSport);
            mName = (TextView) v.findViewById(R.id.textViewName);
            mDate =(TextView) v.findViewById(R.id.textViewTrainingDate);
            mPictureViewUser1 = (ProfilePictureView) v.findViewById(R.id.feed_row_user1);

        }
    }


    public FirebaseFeedAdapter(List<FeedFirebase> list)
    {
        mList = list;

    }

    public void setContext(Context context)
    {
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        FeedFirebase object = mList.get(position);
        return object.getActivityType();
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //SET INFORMATION FROM mList to the view
        FeedFirebase object = mList.get(position);
        switch (object.getActivityType())
        {
            case FirebaseConstants.FEED_TRAINING:
            {
                TrainingViewHolder trainingViewHolder = (TrainingViewHolder)holder;
                //TrainingFirebase training = object.getTraining();
                /*ParseUser parseUser = training.getUser();
                trainingViewHolder.mName.setText(parseUser.getUsername());
                if(mContext!=null) {
                    String resourceName = STTConstants.FILE_NAME_ICONSPORT + training.getSportId();
                    int resourceId = STTUtils.getResourceId(mContext, resourceName, STTConstants.RESOURCE_DRAWABLE);
                    Glide.with(mContext).load(resourceId).into(trainingViewHolder.mImageView);
                }
                String description;
                description = "Trained "+ training.getDateStr(mContext);
                if(training.getPlaceId()!=null && !training.getPlaceId().isEmpty())
                {
                    Place place = mParseObjectMap.getAsPlaces(training.getPlaceId());
                    if(place !=null)
                        description+= "\nIn: "+ place.getName();
                }
                trainingViewHolder.mDate.setText(description);
                trainingViewHolder.mPictureViewUser1.setProfileId(parseUser.getString(STTConstants.FACEBOOK_ID));
                */
            }
            break;
            case FirebaseConstants.FEED_CHALLENGE:
            {

                FirebaseChallengesViewHolder challengesViewHolder = (FirebaseChallengesViewHolder) holder;

                String challengeId = object.getRelatedSourceId();
                challengesViewHolder.setContext(mContext);
                challengesViewHolder.setData(challengeId,object.getUserId());
            }
            break;

        }
    }

    @Override
    public int getItemCount() {
        if(mList==null)
            return 0;
        return mList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        switch (viewType)
        {
            case FirebaseConstants.FEED_CHALLENGE:
            {
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_feed_challenge, parent, false);
                return new FirebaseChallengesViewHolder(v);


            }
            case FirebaseConstants.FEED_TRAINING:
            {
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_feed_training, parent, false);
                return new TrainingViewHolder(v);


            }
        }
        return null;
    }

    public static class FirebaseChallengesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView mChallengeStatus;
        public ProfilePictureView mProfilePictureViewTo;
        public TextView textViewTo;
        public ImageView sportImageView;
        public TextView nameTV;
        public TextView descTV;
        public TextView dateTV;
        public ImageView rewardIV;
        public TextView rewardCommentTV;
        public View separatorTV;

        private ChallengeFirebase lChallenge;
        private User lObjectFrom;
        private User lObjectTo;


        public RelativeLayout layout;

        public FirebaseChallengesViewHolder(View v)
        {
            super(v);
            v.setOnClickListener(this);

            mChallengeStatus = (TextView)v.findViewById(R.id.textview_challenge_status);

            mProfilePictureViewTo = (ProfilePictureView)v.findViewById(R.id.imageview_friend_to_challenge);
            textViewTo = (TextView) v.findViewById(R.id.textview_to_name);

            v.findViewById(R.id.textview_separator_challenge).setVisibility(View.VISIBLE);

            sportImageView = (ImageView) v.findViewById(R.id.imageView_sport_ic);

            nameTV = (TextView) v.findViewById(R.id.textview_challenge_name);

            descTV = (TextView) v.findViewById(R.id.textview_challenge_desc);

            dateTV = (TextView) v.findViewById(R.id.textview_challenge_date);

            layout = (RelativeLayout) v.findViewById(R.id.layout_full_row);

            rewardIV = (ImageView)v.findViewById(R.id.imageview_reward_icon);

            rewardCommentTV = (TextView)v.findViewById(R.id.textview_reward_comment);

            separatorTV = v.findViewById(R.id.textview_separator2_challenge);

        }

        public void setData(String challengeId, String userTo)
        {
            new GetAChallenge(challengeId,userTo).onBackground(challengeCallback);
        }

        public void setData(ChallengeFirebase challenge)
        {
            lChallenge = challenge;
        }

        public void setInfo(Context context, ChallengeFirebase challenge,String from, String to)
        {
            mContext = context;
            switch (challenge.getResult()){

                case ChallengeFirebase.CHALLENGE_RESULT_PENDING:
                    mChallengeStatus.setText(context.getString(R.string.new_challenge_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_light_blue);
                    break;

                case ChallengeFirebase.CHALLENGE_RESULT_ACCEPTED:
                    mChallengeStatus.setText(context.getString(R.string.challenge_accepted_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_blue);
                    break;

                case ChallengeFirebase.CHALLENGE_RESULT_REFUSED:
                    mChallengeStatus.setText(context.getString(R.string.challenge_rejected_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

                case ChallengeFirebase.CHALLENGE_RESULT_SUCCEED:
                    mChallengeStatus.setText(context.getString(R.string.challenge_succeed_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_green);
                    break;

                case ChallengeFirebase.CHALLENGE_RESULT_FAILED:
                    mChallengeStatus.setText(context.getString(R.string.challenge_failed_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

                case ChallengeFirebase.CHALLENGE_RESULT_EXPIRED:
                    mChallengeStatus.setText(context.getString(R.string.challenge_expired_label));
                    mChallengeStatus.setBackgroundResource(R.drawable.challenge_rounded_label_yellow);
                    break;

            }

            new GetAnUser(from).onBackground(userFromCallback);
            new GetAnUser(to).onBackground(userToCallback);

            String sportResourceId = STTConstants.FILE_NAME_ICONSPORT + challenge.getSport();
            Glide.with(context).load(STTUtils.getResourceId(context, sportResourceId, STTConstants.RESOURCE_DRAWABLE)).into(sportImageView);

            nameTV.setText(challenge.getName());
            descTV.setText(challenge.getDesc());
            dateTV.setText(challenge.getTimeLeftStr(context));

            if (challenge.getReward() >= 0) {

                String rewardResourceId = STTConstants.FILE_NAME_ICONREWARD + challenge.getReward();

                Glide.with(context).load(STTUtils.getResourceId(context, rewardResourceId, STTConstants.RESOURCE_DRAWABLE)).into(rewardIV);

                rewardCommentTV.setText(challenge.getRewardComment());

                separatorTV.setVisibility(View.VISIBLE);
                rewardIV.setVisibility(View.VISIBLE);
                rewardCommentTV.setVisibility(View.VISIBLE);
            }
        }

        final OnFirebaseRequestCallback<ChallengeFirebase> challengeCallback = new OnFirebaseRequestCallback<ChallengeFirebase>() {
            @Override
            public void onRequestFinished(List<ChallengeFirebase> list) {
                if(list.size() > 0) {
                    lChallenge = list.get(0);
                    setInfo(mContext, lChallenge,lChallenge.getFrom(), lChallenge.getTo());
                }
            }
        };

        final OnFirebaseRequestCallback<User> userFromCallback = new OnFirebaseRequestCallback<User>() {
            @Override
            public void onRequestFinished(List<User> list) {
                if(list.size() > 0) {
                    lObjectFrom = list.get(0);
                    if (lObjectTo != null)
                        fillUserInfo();
                }
            }
        };

        final OnFirebaseRequestCallback<User> userToCallback = new OnFirebaseRequestCallback<User>() {
            @Override
            public void onRequestFinished(List<User> list) {
                if(list.size() > 0) {
                    lObjectTo = list.get(0);
                    if (lObjectFrom != null)
                        fillUserInfo();
                }
            }
        };

        private void fillUserInfo(){
            String text = "<b>" + lObjectTo.getUserName() + "</b> " + mContext.getString(R.string.is_challenged_by) + " <b>" + lObjectFrom.getUserName() + "</b>";
            textViewTo.setText(Html.fromHtml(text));
            mProfilePictureViewTo.setProfileId(lObjectTo.getFacebookId());
        }


        public void setContext(Context context){
            mContext = context;
        }

        private void redirectToChallenge(){
            Intent intent = new Intent(mContext, SeeChallengeActivity.class);

            Bundle args = new Bundle();

            args.putParcelable(FirebaseConstants.CLASS_NAME_CHALLENGE_FIREBASE, lChallenge);


            intent.putExtras(args);
            mContext.startActivity(intent);
        }

        @Override
        public void onClick(View view) {
            redirectToChallenge();
        }
    }
}
