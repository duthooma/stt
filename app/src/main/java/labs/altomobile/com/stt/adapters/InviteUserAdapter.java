package labs.altomobile.com.stt.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.InviteFriendsActivity;
import labs.altomobile.com.stt.utils.STTUtils;

public class InviteUserAdapter extends ArrayAdapter<InviteFriendsActivity.InvitableFriend> {
    private final Context mContext;
    private final List<InviteFriendsActivity.InvitableFriend> invitableFriends;
    private int layoutResourceId;

    //list of ids selected to invite
    private List<String> idsToInvite = new ArrayList<>();


    public InviteUserAdapter(Context context, int layoutResourceId, List<InviteFriendsActivity.InvitableFriend> invitableFriends) {
        super(context, layoutResourceId, invitableFriends);
        this.mContext = context;
        this.invitableFriends = invitableFriends;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        ProfilePictureView picfacebook = (ProfilePictureView) convertView.findViewById(R.id.friendtoinvite_picture);

        TextView nameView = (TextView) convertView.findViewById(R.id.friendtoinvite_name);
        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
        TextView alreadyInvitedTextView = (TextView)convertView.findViewById(R.id.textview_already_invited);

        JSONObject currentUser = invitableFriends.get(position).getFriend();

        nameView.setText(currentUser.optString("first_name"));

        final String tokenID = currentUser.optString("id");
        picfacebook.setProfileId(tokenID);

        if (invitableFriends.get(position).isAlreadyInvited()){
            checkBox.setVisibility(View.GONE);
            alreadyInvitedTextView.setVisibility(View.VISIBLE);
        }else{
            checkBox.setVisibility(View.VISIBLE);
            alreadyInvitedTextView.setVisibility(View.GONE);
            checkBox.setClickable(false);

            final RelativeLayout layout = (RelativeLayout)convertView.findViewById(R.id.layout_row_invite_friends);

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (idsToInvite.contains(tokenID)) {
                        idsToInvite.remove(tokenID);
                        checkBox.setChecked(false);
                        layout.setBackgroundColor(0x00000000);
                    } else {
                        idsToInvite.add(tokenID);
                        checkBox.setChecked(true);
                        layout.setBackgroundColor(mContext.getResources().getColor(R.color.very_light_blue));
                    }
                }
            });
        }

        return convertView;
    }

    public List<String> getIdsToInvite()
    {
        return idsToInvite;
    }

    class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public ImageDownloader(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            result = STTUtils.getCircleClip(result,0,0,null);
            bmImage.setImageBitmap(result);
        }
    }
}