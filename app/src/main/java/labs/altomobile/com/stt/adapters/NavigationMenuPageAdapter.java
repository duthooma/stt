package labs.altomobile.com.stt.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.fragments.ChallengesFragment;
import labs.altomobile.com.stt.fragments.FeedFragment;
import labs.altomobile.com.stt.fragments.MyActivityFragment;
import labs.altomobile.com.stt.fragments.GroupsFragment;

/**
 * Created by maxence on 3/23/15.
 */
public class NavigationMenuPageAdapter extends FragmentPagerAdapter {

    private String[] tabList;
    private Context mContext;

    public NavigationMenuPageAdapter(FragmentManager fm,Context context) {
        super(fm);
        mContext = context;
        tabList = context.getResources().getStringArray(R.array.tabs_navigation_menu);
    }

    @Override
    public Fragment getItem(int idx) {

        Fragment currentFragment;

        switch (idx){
            case 0:
                currentFragment = new FeedFragment();
                break;
            case 1:
                currentFragment = new GroupsFragment();
                break;
            case 2:
                currentFragment = new ChallengesFragment();
                break;
            default:
                currentFragment = new MyActivityFragment();
        }

        return currentFragment;
    }

    @Override
    public int getCount() {
        return tabList.length;
    }

    @Override
    public CharSequence getPageTitle(int idx) {
        return tabList[idx];
    }
}
