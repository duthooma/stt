package labs.altomobile.com.stt.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.utils.Debug;

/**
 * Created by maxence on 3/9/16.
 */
public class SportGridAdapter extends ArrayAdapter<TypedArray>{

        private Context mContext;
        private int layoutResourceId;
        private TypedArray mGridData;
        private View.OnClickListener mClickListener;

        public SportGridAdapter(Context mContext, int layoutResourceId, View.OnClickListener onClickListener) {
            super(mContext, layoutResourceId);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.mClickListener = onClickListener;

            this.mGridData = mContext.getResources().obtainTypedArray(R.array.sportIconIds_100dp);

            notifyDataSetChanged();
        }

    @Override
    public int getCount() {
        return mGridData.length();
    }

    @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ViewHolder holder;

            if (row == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.imageView = (ImageView) row.findViewById(R.id.grid_item_image);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            int resourceId = mGridData.getResourceId(position, -1);
            Glide.with(mContext).load(resourceId).into(holder.imageView);

            //Set up click listener
            holder.imageView.setTag(position);
            holder.imageView.setOnClickListener(mClickListener);

            return row;
        }

        static class ViewHolder {
            ImageView imageView;
        }
    }