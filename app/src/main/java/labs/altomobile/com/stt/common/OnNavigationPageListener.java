package labs.altomobile.com.stt.common;

/**
 * Created by danielmonterocervantes on 02/06/15.
 */
public interface OnNavigationPageListener {
      void onNavigationPageSelected(int position);
}
