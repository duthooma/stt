package labs.altomobile.com.stt.common;

/**
 * Created by flavioreyes on 5/28/15.
 */
public class SerializableParseException extends RuntimeException {
    public SerializableParseException(String message)
    {
        super(message);
    }
}
