package labs.altomobile.com.stt.constants;

/**
 * Created by maxence on 4/22/15.
 */
public class DummiesConstants {

    public static final String FB_ID_MAX = "10204598502186605";
    public static final String FB_ID_MAX2 = "763611733703526";
    public static final String FB_ID_DANIEL = "1746224165603998";
    public static final String FB_ID_FLAVIO = "10153162782962139";

    public static final String USERID_MAX = "QZtjEHAE9x";
    public static final String USERID_DANIEL = "ryH5579ImC";
    public static final String USERID_FLAVIO = "HJwEYCRKCt";
    public static final String USERID_MAX2 = "xLbsCvsqKq";



}
