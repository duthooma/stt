package labs.altomobile.com.stt.constants;

/**
 * Created by Flavio on 08/04/2016.
 */
public class FirebaseConstants {
    //Children Names
    public static final String CHILD_USERS= "users";
    public static final String CHILD_USER_PROFILE = "profile";
    public static final String CHILD_TRAININGS= "trainings";
    public static final String CHILD_CHALLENGES = "challenges";
    public static final String CHILD_FEED = "feed";

    //User Field Names
    public static final String USER_REQUEST_TO = "friends_request_to";
    public static final String USER_REQUEST_FROM = "friends_request_from";
    public static final String USER_FRIENDS = "friends";
    public static final String USER_FACEBOOK_ID = "facebookId";
    public static final String USER_USER_NAME = "userName";
    public static final String USER_PROFILE_IMAGE_URL = "profileImageURL";

    //Challenges Field Names
    public static final String CHALLENGE_NAME = "name";
    public static final String CHALLENGE_DESCRIPTION = "description";
    public static final String CHALLENGE_DATE = "date";
    public static final String CHALLENGE_RESULT = "result";
    public static final String CHALLENGE_USER_FROM = "user_from";
    public static final String CHALLENGE_USER_TO = "user_to";
    public static final String CHALLENGE_REWARD = "reward";
    public static final String CHALLENGE_REWARD_COMMENT = "reward_comment";

    //Feed Fields Names
    public static final String FEED_MODIFIED = "modified";
    public static final String FEED_ACTIVITY_TYPE = "activityType";
    public static final String FEED_REFERENCE_SOURCE_ID = "refSourceId";

    //Ids
    public static final String USER_ID = "userId";
    public static final String SPORT_ID = "sportId";

    //Class Names
    public static final String CLASS_NAME_CHALLENGE_FIREBASE = "ChallengeFireabse";
    public static final String CLASS_NAME_USER_FIREBASE = "UserFireabse";
    public static final String CLASS_NAME_TRAINING_FIREBASE = "TrainingFirebase";

    //Feed type
    public static final int FEED_CHALLENGE = 1;
    public static final int FEED_TRAINING = 3;
}
