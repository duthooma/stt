package labs.altomobile.com.stt.constants;

/**
 * Created by MaxenceD on 6/1/15.
 */
public class ParseConstants {

    //Class names
    public static final String CLASS_NAME_CHALLENGE = "Challenge";
    public static final String CLASS_NAME_FRIENDREQUEST = "FriendRequest";
    public static final String CLASS_NAME_FEED = "Feed";

    public static final String CLASS_NAME_GROUP = "Group";
    public static final String CLASS_NAME_GROUPVSUSER = "GroupVSUser";
    public static final String CLASS_NAME_PLACE = "Place";
    public static final String CLASS_NAME_TRAINING = "Training";
    public static final String CLASS_NAME_USERLEVEL = "UserLevel";

    public static final String CLASS_NAME_COMMENT = "Comment";

    public static final String CLASS_NAME_NOTIFICATION = "Notification";


    //Field names
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String SPORTS = "sports";
    public static final String PICTURE = "picture";
    public static final String ACCEPTATION_STATUS = "acceptation_status";
    public static final String DATE = "date";
    public static final String END_DATE = "end_date";
    public static final String RESULT = "result";
    public static final String USER_FROM = "user_from";
    public static final String USER_TO = "user_to";
    public static final String ROLE = "role";
    public static final String LEVEL = "level";
    public static final String REWARD = "reward";
    public static final String REWARD_COMMENT = "reward_comment";

    //Join fields
    public static final String PLACE_ID = "placeId";
    public static final String GROUP_ID = "groupId";
    public static final String OBJECT_ID = "objectId";
    public static final String USER_ID = "userId";
    public static final String SPORT_ID = "sportId";

    //Date fields
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";


    //Pointers
    public static final String USER_REFERENCE = "userReference";


}
