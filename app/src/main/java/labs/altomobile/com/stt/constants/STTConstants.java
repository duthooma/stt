package labs.altomobile.com.stt.constants;

/**
 * Created by Maxence on 2/26/2015.
 */
public class STTConstants {

    public static boolean isLogActivated = true;
    public static boolean USE_FIREBASE = false;

    public static String LOG_TAG = "labs.altomobile.com.stt";

    public static String DISPLAY_OPTION = "DISPLAY_OPTION";
    public static String FACEBOOK_ID = "facebookId";
    public static String FACEBOOK_KEY_PREFIX = "facebook:";

    //Display options
    public static final int DISPLAY_OPTION_CREATE_GROUP = 0;
    public static final int DISPLAY_OPTION_CREATE_CHALLENGE = 1;
    public static final int DISPLAY_OPTION_SEE_CHALLENGE = 2;

    //Resource types
    public static final String RESOURCE_DRAWABLE = "drawable";

    //File name
    public static final String FILE_NAME_ICONSPORT = "ic_sport_";
    public static final String FILE_NAME_ICONREWARD = "ic_reward_";
    public static final String FILE_NAME_IMAGE_TEMP = "tempImage";
    public static final String FOLDER_IMAGES_TEMP = "tempImages";
    public static final String FILE_NAME_GCM_CONFIG = "GCM_Info";
    public static final String FILE_NAME_NOTIFICATION_COUNT = "notCount";

    //Parameter file info
    public static final String GCM_ID = "gcmId";

    //Notifications ids
    public static final int NOTIFICATION_ID_FRIEND_REQUEST   = 1;
    public static final int NOTIFICATION_ID_CHALLENGE       = 2;

    //GCM parameters
    public static final String GCM_PARAMETER_TYPE           = "type";
    public static final String GCM_PARAMETER_NOTIFICATION   = "notification";
    public static final String GCM_PARAMETER_FROM_USER      = "fromUser";
    public static final String GCM_PARAMETER_MESSAGE        = "message";

    //Local notification parameters
    public static final String BADGET_COUNT = "badgeCount";

    //Notificaitons types
    public static final String NOTIF_TYPE_CHALLENGE = "challenge";
    public static final String EMAIL_CONTACT = "contact@altomobile.co";

}
