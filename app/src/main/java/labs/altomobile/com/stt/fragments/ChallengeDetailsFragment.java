package labs.altomobile.com.stt.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.login.widget.ProfilePictureView;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.activities.MainMenuActivity;
import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.interfaces.OnSaveCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Comment;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.utils.Debug;
import labs.altomobile.com.stt.utils.STTUtils;

public class ChallengeDetailsFragment extends Fragment implements View.OnClickListener, OnSaveCallback, OnRequestCallback<Comment>, Firebase.CompletionListener {

    private Challenge mChallenge = null;
    private ChallengeFirebase mFirebaseChallenge = null;
    private User mFirebaseToUser = null;
    private User mFirebaseFromUser = null;
    private ParseUser mToUser = null;
    private ParseUser mFromUser = null;
    private int mDisplayOption;

//    private RecyclerView mRecyclerView;
    private Button mAcceptBtn;
    private Button mTurndownBtn;
    private Button mSucceedBtn;
    private Button mFailedBtn;
    private Button mAddRewardButton;

//    private CommentAdapter mCommentAdapter;
//    private EditText mAddCommentText;

    public interface OnAddRewardListener {
        void onAddRewardListener();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null) {

            mDisplayOption = arguments.getInt(STTConstants.DISPLAY_OPTION,-1);

            if(STTConstants.USE_FIREBASE){
                mFirebaseChallenge = arguments.getParcelable(FirebaseConstants.CLASS_NAME_CHALLENGE_FIREBASE);
                mFirebaseToUser = arguments.getParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE);
                mFirebaseFromUser = arguments.getParcelable(FirebaseConstants.CLASS_NAME_USER_FIREBASE+"from");
            } else {
                String challengeId = arguments.getString(ParseConstants.CLASS_NAME_CHALLENGE);
                mChallenge = (Challenge)ParseObjectMap.getInstance().get(challengeId);
                mFromUser = mChallenge.getFrom();
                mToUser = mChallenge.getTo();
            }
        }else{
            Log.w(STTConstants.LOG_TAG, "WARNING :: Using " + ChallengeDetailsFragment.class.getSimpleName() + " without argument");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;


        switch (mDisplayOption)
        {
            case STTConstants.DISPLAY_OPTION_SEE_CHALLENGE:
                view = inflater.inflate(R.layout.fragment_challenge_details, container, false);
                break;
            case STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE:
                view = inflater.inflate(R.layout.fragment_challenge_preview_select_reward, container, false);
                mAddRewardButton = (Button)view.findViewById(R.id.button_add_reward);
                mAddRewardButton.setOnClickListener(this);
                break;
            default:
                Debug.Error("display option not support : it will crash");
        }

        if(mChallenge != null && mToUser != null && mFromUser != null) {

            ProfilePictureView toProfilePictureView = (ProfilePictureView) view.findViewById(R.id.imageview_friend_to_challenge);
            toProfilePictureView.setProfileId(mToUser.getString(STTConstants.FACEBOOK_ID));

            TextView toNameTV = (TextView)view.findViewById(R.id.textview_to_name);
            String text = "<b>" + mToUser.getUsername() + "</b> " + getActivity().getString(R.string.is_challenged_by) + " <b>" + mFromUser.getUsername() + "</b>";
            toNameTV.setText(Html.fromHtml(text));

           // view.findViewById(R.id.button_send_comment).setOnClickListener(this);

            ImageView sportImageView = (ImageView) view.findViewById(R.id.imageView_sport_ic);
            String resourceId = STTConstants.FILE_NAME_ICONSPORT + mChallenge.getSport();
            Glide.with(this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(sportImageView);

            TextView nameTV = (TextView) view.findViewById(R.id.textview_challenge_name);
            nameTV.setText(mChallenge.getName());

            TextView descTV = (TextView) view.findViewById(R.id.textview_challenge_desc);
            descTV.setText(mChallenge.getDesc());

          //  mAddCommentText = (EditText) view.findViewById(R.id.editTextAddComment);

            TextView dateTV = (TextView) view.findViewById(R.id.textview_challenge_date);
            dateTV.setText(mChallenge.getTimeLeftStr(getActivity()));

            if (mChallenge.getReward() >= 0)
            {

                ImageView rewardIV = (ImageView) view.findViewById(R.id.imageview_reward_icon);

                String rewardResourceId = STTConstants.FILE_NAME_ICONREWARD + mChallenge.getReward();
                Glide.with(this).load(STTUtils.getResourceId(getActivity(), rewardResourceId, STTConstants.RESOURCE_DRAWABLE)).into(rewardIV);

                View separatorTV = view.findViewById(R.id.textview_separator2_challenge);
                separatorTV.setVisibility(View.VISIBLE);
                rewardIV.setVisibility(View.VISIBLE);

                if (!mChallenge.getRewardComment().isEmpty())
                {
                    TextView rewardCommentTV = (TextView) view.findViewById(R.id.textview_reward_comment);
                    rewardCommentTV.setText(mChallenge.getRewardComment());
                    rewardCommentTV.setVisibility(View.VISIBLE);
                }

            }

            if((mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE) && (mChallenge.getReward() >= 0)) {
                //hide the "add a reward" layout
                RelativeLayout addRewardLayout = (RelativeLayout)view.findViewById(R.id.layout_add_reward);
                addRewardLayout.setVisibility(View.GONE);
            }

            if(mDisplayOption == STTConstants.DISPLAY_OPTION_SEE_CHALLENGE && mChallenge.isTheChallenged(ParseUser.getCurrentUser())) {

                if (mChallenge.isPendingForAcceptation()) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_option_buttons_pending);
                    layout.setVisibility(View.VISIBLE);

                    mAcceptBtn = (Button) view.findViewById(R.id.accept_challenge_button);
                    mTurndownBtn = (Button) view.findViewById(R.id.turndown_challenge_button);
                    mAcceptBtn.setOnClickListener(this);
                    mTurndownBtn.setOnClickListener(this);
                } else if (mChallenge.isAccepted() && mChallenge.isOpen()) {
                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_option_buttons_accepted);
                    layout.setVisibility(View.VISIBLE);

                    mSucceedBtn = (Button) view.findViewById(R.id.succeed_challenge_button);
                    mFailedBtn = (Button) view.findViewById(R.id.failed_challenge_button);
                    mSucceedBtn.setOnClickListener(this);
                    mFailedBtn.setOnClickListener(this);
                }
            }
        }else if (mFirebaseChallenge != null){
            ProfilePictureView toProfilePictureView = (ProfilePictureView) view.findViewById(R.id.imageview_friend_to_challenge);
            toProfilePictureView.setProfileId(mFirebaseToUser.getFacebookId());

            TextView toNameTV = (TextView)view.findViewById(R.id.textview_to_name);
            String text = "<b>" + mFirebaseToUser.getUserName() + "</b> " + getActivity().getString(R.string.is_challenged_by) + " <b>" + mFirebaseFromUser.getUserName() + "</b>";
            toNameTV.setText(Html.fromHtml(text));

            // view.findViewById(R.id.button_send_comment).setOnClickListener(this);

            ImageView sportImageView = (ImageView) view.findViewById(R.id.imageView_sport_ic);
            String resourceId = STTConstants.FILE_NAME_ICONSPORT + mFirebaseChallenge.getSport();
            Glide.with(this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(sportImageView);

            TextView nameTV = (TextView) view.findViewById(R.id.textview_challenge_name);
            nameTV.setText(mFirebaseChallenge.getName());

            TextView descTV = (TextView) view.findViewById(R.id.textview_challenge_desc);
            descTV.setText(mFirebaseChallenge.getDesc());

            //  mAddCommentText = (EditText) view.findViewById(R.id.editTextAddComment);

            TextView dateTV = (TextView) view.findViewById(R.id.textview_challenge_date);
            dateTV.setText(mFirebaseChallenge.getTimeLeftStr(getActivity()));

            if (mFirebaseChallenge.getReward() >= 0)
            {

                ImageView rewardIV = (ImageView) view.findViewById(R.id.imageview_reward_icon);

                String rewardResourceId = STTConstants.FILE_NAME_ICONREWARD + mFirebaseChallenge.getReward();
                Glide.with(this).load(STTUtils.getResourceId(getActivity(), rewardResourceId, STTConstants.RESOURCE_DRAWABLE)).into(rewardIV);

                View separatorTV = view.findViewById(R.id.textview_separator2_challenge);
                separatorTV.setVisibility(View.VISIBLE);
                rewardIV.setVisibility(View.VISIBLE);

                if (!mFirebaseChallenge.getRewardComment().isEmpty())
                {
                    TextView rewardCommentTV = (TextView) view.findViewById(R.id.textview_reward_comment);
                    rewardCommentTV.setText(mFirebaseChallenge.getRewardComment());
                    rewardCommentTV.setVisibility(View.VISIBLE);
                }

            }

            if((mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE) && (mFirebaseChallenge.getReward() >= 0)) {
                //hide the "add a reward" layout
                RelativeLayout addRewardLayout = (RelativeLayout)view.findViewById(R.id.layout_add_reward);
                addRewardLayout.setVisibility(View.GONE);
            }

            if(mDisplayOption == STTConstants.DISPLAY_OPTION_SEE_CHALLENGE && mFirebaseChallenge.isTheChallenged()) {

                if (mFirebaseChallenge.isPendingForAcceptation()) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_option_buttons_pending);
                    layout.setVisibility(View.VISIBLE);

                    mAcceptBtn = (Button) view.findViewById(R.id.accept_challenge_button);
                    mTurndownBtn = (Button) view.findViewById(R.id.turndown_challenge_button);
                    mAcceptBtn.setOnClickListener(this);
                    mTurndownBtn.setOnClickListener(this);
                } else if (mFirebaseChallenge.isAccepted() && mFirebaseChallenge.isOpen()) {
                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_option_buttons_accepted);
                    layout.setVisibility(View.VISIBLE);

                    mSucceedBtn = (Button) view.findViewById(R.id.succeed_challenge_button);
                    mFailedBtn = (Button) view.findViewById(R.id.failed_challenge_button);
                    mSucceedBtn.setOnClickListener(this);
                    mFailedBtn.setOnClickListener(this);
                }
            }

        }else if (mChallenge == null){
            Log.e(STTConstants.LOG_TAG, "ERROR :: mChallenge object is NULL");
        }else if (mToUser == null){
            Log.e(STTConstants.LOG_TAG, "ERROR :: mToUser object is NULL");
        }else if (mFromUser == null){
            Log.e(STTConstants.LOG_TAG, "ERROR :: mFromUser object is NULL");
        }
/*
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView_comments);
        if(mRecyclerView!=null) {
            mRecyclerView.setLayoutManager(layoutManager);
            mCommentAdapter = new CommentAdapter();
            mRecyclerView.setAdapter(mCommentAdapter);
        }

        */
       if(mChallenge != null && mChallenge.getObjectId()!=null)
        {

            refresh();
        }

        if (mFirebaseChallenge != null){
            refresh();
        }

        return view;
    }



    @Override
    public void onClick(View v) {
        if (mFirebaseChallenge != null){
            if (v == mAcceptBtn) {
                setAndSaveChallengeResult(mFirebaseChallenge, ChallengeFirebase.CHALLENGE_RESULT_ACCEPTED);
            } else if (v == mTurndownBtn) {
                setAndSaveChallengeResult(mFirebaseChallenge, ChallengeFirebase.CHALLENGE_RESULT_REFUSED);
            } else if (v == mSucceedBtn) {
                setAndSaveChallengeResult(mFirebaseChallenge, ChallengeFirebase.CHALLENGE_RESULT_SUCCEED);
            } else if (v == mFailedBtn) {
                setAndSaveChallengeResult(mFirebaseChallenge, ChallengeFirebase.CHALLENGE_RESULT_FAILED);
            } else if (v == mAddRewardButton) {
                Debug.v("CreateChallenge : showing the addReward screen ");
                OnAddRewardListener listener = (CreateChallengeActivity) getActivity();
                listener.onAddRewardListener();
            }
        } else {
            if (v == mAcceptBtn) {
                setAndSaveChallengeResult(mChallenge, Challenge.CHALLENGE_RESULT_ACCEPTED);
            } else if (v == mTurndownBtn) {
                setAndSaveChallengeResult(mChallenge, Challenge.CHALLENGE_RESULT_REFUSED);
            } else if (v == mSucceedBtn) {
                setAndSaveChallengeResult(mChallenge, Challenge.CHALLENGE_RESULT_SUCCEED);
            } else if (v == mFailedBtn) {
                setAndSaveChallengeResult(mChallenge, Challenge.CHALLENGE_RESULT_FAILED);
            } else if (v == mAddRewardButton) {
                Debug.v("CreateChallenge : showing the addReward screen ");
                OnAddRewardListener listener = (CreateChallengeActivity) getActivity();
                listener.onAddRewardListener();
            }
        }
       /* else if(v.getId()==R.id.button_send_comment) {
            addComment();
        }
        */
    }

    private void refresh()
    {
     //   RequestManager.GET_COMMENTS_OF_CURRENT_CHALLENGE.onBackground(RequestParams.create().add(ParseConstants.CLASS_NAME_CHALLENGE, mChallenge), this);

    }

    private void addComment()
    {
      /*  String text = mAddCommentText.getText().toString();
        mAddCommentText.setText("");
        if(!text.isEmpty()) {
            hideSoftKeyboard(getActivity());
            Comment comment = new Comment();
            comment.setUser(ParseUser.getCurrentUser());
            comment.setChallenge(mChallenge);
            comment.setContent(text);
            comment.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    refresh();

                }
            });
        }
        */
    }
    @Override
    public void onSaveObject(ParseObject parseObject) {

        if (parseObject.getInt(Challenge.PARSEFIELD_RESULT) == Challenge.CHALLENGE_RESULT_PENDING) {
            // the result hasn't been updated so the user just accepted/refused the challenge
            if (parseObject.getInt(Challenge.PARSEFIELD_ACCEPTATION_STATUS) == Challenge.CHALLENGE_RESULT_ACCEPTED) {
                Toast.makeText(getActivity(), getString(R.string.challenge_accepted), Toast.LENGTH_LONG).show();
            } else if (parseObject.getInt(Challenge.PARSEFIELD_ACCEPTATION_STATUS) == Challenge.CHALLENGE_RESULT_REFUSED) {
                Toast.makeText(getActivity(), getString(R.string.challenge_turned_down), Toast.LENGTH_LONG).show();
            }
        }else{
            //the result is set. it means the user just set it
            if (parseObject.getInt(Challenge.PARSEFIELD_RESULT) == Challenge.CHALLENGE_RESULT_SUCCEED) {
                Toast.makeText(getActivity(), getString(R.string.challenge_succeed), Toast.LENGTH_LONG).show();
            } else if (parseObject.getInt(Challenge.PARSEFIELD_RESULT) == Challenge.CHALLENGE_RESULT_FAILED) {
                Toast.makeText(getActivity(), getString(R.string.challenge_failed), Toast.LENGTH_LONG).show();
            }
        }

        Intent intent = new Intent(getActivity(), MainMenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void onComplete(FirebaseError firebaseError, Firebase firebase) {


        //TODO:check this, maybe a new activity is created and it is not needed
        Intent intent = new Intent(getActivity(), MainMenuActivity.class);
        startActivity(intent);
    }

    private void setAndSaveChallengeResult(ChallengeFirebase challenge, int result){
        challenge.setResult(result);
        FirebaseHelper.saveChallenge(challenge,this);
    }
    private void setAndSaveChallengeResult(Challenge challenge, int result){
        challenge.setResult(result);
        ParseHelper.saveChallenge(challenge,this);
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<Comment> list, ParseObjectMap parseObjectMapmap) {

//        mCommentAdapter.updateInfo(list);
 //       mCommentAdapter.notifyDataSetChanged();

    }
}
