package labs.altomobile.com.stt.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.common.DateUtil;
import labs.altomobile.com.stt.objects.Challenge;

public class ChallengeSetDateFragment extends Fragment implements View.OnClickListener{

    private RadioButton mOneWeekButton;
    private RadioButton mTwoWeeksButton;
    private RadioButton mCustomDayBtn;
    private Button mActionButton;

    private DatePickerDialog mCustomDatePickerDialog;
    private SimpleDateFormat mSdf;

    private long mDateSelected;

    public interface OnDateSelectedListener {
        void onDateSelected(long date);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSdf = new SimpleDateFormat("MMM dd", Locale.US);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_set_challenge_date, container, false);

        mOneWeekButton = (RadioButton)view.findViewById(R.id.button_one_week);
        mTwoWeeksButton = (RadioButton)view.findViewById(R.id.button_two_weeks);
        mCustomDayBtn = (RadioButton)view.findViewById(R.id.button_custom_date);
        mActionButton = ((CreateChallengeActivity) getActivity()).mActionButton;

        mOneWeekButton.setChecked(true);

        mOneWeekButton.setOnClickListener(this);
        mTwoWeeksButton.setOnClickListener(this);
        mCustomDayBtn.setOnClickListener(this);
        mActionButton.setOnClickListener(this);

        Calendar cal = Calendar.getInstance();
        mDateSelected = cal.getTime().getTime() + 7L*24L*3600L*1000L; //segmented control default value is in 7 days

        mSdf = new SimpleDateFormat("MMM dd", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        mCustomDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mCustomDayBtn.setText(mSdf.format(newDate.getTime()));
                mDateSelected = newDate.getTime().getTime();
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)+1);

        mCustomDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() + Challenge.MAX_END_DATE);
        mCustomDatePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTime().getTime() + (long)24*3600*1000/2); //minDate is in 12h*/

        return view;
    }

    @Override
    public void onClick(View v) {

        if(v == mOneWeekButton){
            Calendar cal = Calendar.getInstance();
            mDateSelected = cal.getTime().getTime() + 7L*24L*3600L*1000L;
        }else if(v == mTwoWeeksButton){
            Calendar cal = Calendar.getInstance();
            mDateSelected = cal.getTime().getTime() + 14L*24L*3600L*1000L;
        }else if(v == mCustomDayBtn){
            mCustomDatePickerDialog.show();
        }else if(v == mActionButton){
            //Get parent Activity and send notification
            OnDateSelectedListener listener = (OnDateSelectedListener) getActivity();
            listener.onDateSelected(mDateSelected);
        }
    }

}
