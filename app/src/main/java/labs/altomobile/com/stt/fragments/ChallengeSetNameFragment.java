package labs.altomobile.com.stt.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.constants.STTConstants;

public class ChallengeSetNameFragment extends Fragment {

    private int mDisplayOption;

    private EditText mNameET;
    private EditText mDescET;
    private Button mActionButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null) {
            mDisplayOption = arguments.getInt(STTConstants.DISPLAY_OPTION, -1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_challenge_set_name, container, false);

        mNameET = (EditText)view.findViewById(R.id.nameET);

        mDescET = (EditText)view.findViewById(R.id.descriptionET);
        mActionButton = ((CreateChallengeActivity) getActivity()).mActionButton;

        if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_GROUP) {
            mActionButton.setText(getString(R.string.create));
            mNameET.setHint(getString(R.string.group_name));
            mDescET.setHint(getString(R.string.group_description));
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }else if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE){
            mNameET.setHint(getString(R.string.challenge_name));
            mDescET.setHint(getString(R.string.challenge_description));
        }

        //final LinearLayout faderLayout = (LinearLayout)view.findViewById(R.id.faderLayout);

        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = mNameET.getText().toString();

                if (!name.isEmpty()) {

                    //greyOutScreenAndDisplayLoading(faderLayout);
                    hideSoftKeyboard(view);

                    String desc = mDescET.getText().toString();

                    if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_GROUP) {

                        /*ParseHelper.createGroupAndRelatedObjects(name, desc, new OnSaveCallback<Group>() {
                            @Override
                            public void onSaveObject(Group group) {

                                Toast.makeText(getActivity(), getResources().getText(R.string.group_created), Toast.LENGTH_LONG).show();

                                //Get parent Activity and send notification
                                OnGroupCreatedListener listener = (OnGroupCreatedListener) getActivity();
                                listener.onGroupCreated(group);
                            }
                        });*/
                    }else if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE){

                        //Get parent Activity and send notification
                        OnNameSelectedListener listener = (OnNameSelectedListener) getActivity();
                        listener.onNameSelected(name, desc);
                    }else{
                        Log.w(STTConstants.LOG_TAG, "WARNING :: using " + this.getClass().getSimpleName() + " with an invalid DISPLAY OPTION");
                    }
                } else {
                    showError(mNameET);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mNameET = (EditText)getActivity().findViewById(R.id.nameET);
        mNameET.requestFocus();
        InputMethodManager mgr =      (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(mNameET, InputMethodManager.SHOW_IMPLICIT);
    }

    public interface OnNameSelectedListener {
        void onNameSelected(String name, String desc);
    }

    /*
    public interface OnGroupCreatedListener {
        void onGroupCreated(Group group);
    }*/

    private void showError(EditText editText) {
        Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        editText.startAnimation(shake); }

    /*private void greyOutScreenAndDisplayLoading(LinearLayout faderLayout){
        faderLayout.setVisibility(View.VISIBLE);
    }*/

    private void hideSoftKeyboard(View view){

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }
}