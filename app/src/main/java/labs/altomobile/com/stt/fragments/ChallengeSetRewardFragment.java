package labs.altomobile.com.stt.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.utils.Debug;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeSetRewardFragment extends Fragment implements View.OnClickListener{

    //UI elements
    ImageAdapter mAdapter;
    ImageView mRewardIconIV;
    TextInputEditText mRewardCommentET;
    TextInputLayout mInputLayout;
    Button mActionBtn;

    int mRewardId = Challenge.NO_REWARD;
    CreateChallengeActivity mActivity;

    private TypedArray mRewardIconIds;

    private GridView mGridview;

    public interface OnRewardSelectedListener {
        void onRewardSelectedListener(int rewardId, String rewardComment);
    }

    public ChallengeSetRewardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mActionBtn = ((CreateChallengeActivity) getActivity()).mActionButton;
        mActionBtn.setText(R.string.next);
        mActionBtn.setVisibility(View.GONE);
        mActionBtn.setOnClickListener(this);

        View view = inflater.inflate(R.layout.fragment_challenge_set_reward, container, false);

        mRewardIconIds = getResources().obtainTypedArray(R.array.rewardIconIds);

        mRewardIconIV = (ImageView)view.findViewById(R.id.imageview_reward_icon);
        mRewardCommentET = (TextInputEditText)view.findViewById(R.id.edittext_reward_comment);
        mInputLayout = (TextInputLayout)view.findViewById(R.id.inputlayout_reward_description);

        mGridview = (GridView) view.findViewById(R.id.gridview);
        mAdapter = new ImageAdapter(getActivity());
        mGridview.setAdapter(mAdapter);

        if (getActivity() == null)
            Debug.trash("onCreateView ::: getActivity() NULL");

        return view;
    }

    @Override
    public void onClick(View view)
    {
        mActivity.onRewardSelectedListener(mRewardId, mRewardCommentET.getText().toString());
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mRewardIconIds.length();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            Glide.with(ChallengeSetRewardFragment.this).load(mRewardIconIds.getResourceId(position, -1)).into(imageView);

            imageView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    mRewardId = position;

                    //hide gridview
                    mGridview.setVisibility(View.GONE);

                    //set ImageView of reward and show it
                    Glide.with(ChallengeSetRewardFragment.this).load(mRewardIconIds.getResourceId(position, -1)).into(mRewardIconIV);
                    mRewardIconIV.setVisibility(View.VISIBLE);

                    mActionBtn.setVisibility(View.VISIBLE);
                    mInputLayout.setVisibility(View.VISIBLE);

                    InputMethodManager mgr =      (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.showSoftInput(mRewardCommentET, InputMethodManager.SHOW_IMPLICIT);

                }
            });

            return imageView;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Debug.trash("### onAttach");
        if (context instanceof CreateChallengeActivity){
            Debug.trash("context instanceof CreateChallengeActivity");
            mActivity =(CreateChallengeActivity) context;
        }else{
            Debug.trash("context NOT instanceof CreateChallengeActivity");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CreateChallengeActivity){
            Debug.trash("acti instanceof CreateChallengeActivity");
            mActivity =(CreateChallengeActivity) activity;
        }else{
            Debug.trash("acti NOT instanceof CreateChallengeActivity");
        }
    }
}
