package labs.altomobile.com.stt.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.adapters.ChallengeAdapter;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;

public class ChallengesFragment extends Fragment implements OnRequestCallback<Challenge>, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Challenge> mList = new ArrayList<>();

    private ProgressBar progressBar;
    private TextView mEmptyListTV;

    private ChallengeAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_challenges, container, false);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.recycler_challenges);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if(mAdapter == null) {
            mAdapter = new ChallengeAdapter(mList);
        }

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setContext(getActivity());
        mAdapter.setViewAsOwner(true);

        progressBar = (ProgressBar) view.findViewById(R.id.loading);
        progressBar.setVisibility(View.VISIBLE);

        mEmptyListTV = (TextView) view.findViewById(R.id.tv_no_challenge_yet);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        RequestManager.GET_CHALLENGES_OF_CURRENT_USER.onBackground(this);
        return view;
    }

    @Override
    public void onRefresh() {
        RequestManager.GET_CHALLENGES_OF_CURRENT_USER.onBackground(this);

    }

    @Override
    public void onRequestBegin() {
    }

    @Override
    public void onRequestFinished(List<Challenge> list, ParseObjectMap parseObjectMapmap) {

        if(list.size() > 0){

            mList.clear();
            mList.addAll(list);
            mAdapter.notifyDataSetChanged();
            mAdapter.setParseObjectMap(parseObjectMapmap);
        }else{
            mEmptyListTV.setVisibility(View.VISIBLE);
        }

        progressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

}
