package labs.altomobile.com.stt.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.File;
import java.io.IOException;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateGroupActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.serializationUtils.ImageResult;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;
import labs.altomobile.com.stt.utils.ImageDownloadAsyncTask;
import labs.altomobile.com.stt.utils.STTUtils;

public class CreateGroupSetPictureFragment extends Fragment {

    private Group mGroup;
    private ImageView  groupImageView;
    private ProgressBar loadingImageProgressBar;
    private Uri mMakePhotoUri;

    //private int ht_px;
    //private int wt_px;

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_GALLERY = 1;
    private static final int PICK_FROM_INTERNET = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //so the icons size is dp
        /*ht_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));
        wt_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));*/

        Bundle arguments = getArguments();

        if (arguments != null) {
            SerializableParseObject sgroup = (SerializableParseObject) arguments.get(CreateGroupActivity.GROUP_SERIALIZABLE);
            if (sgroup != null) {
                mGroup = (Group) sgroup.getParseObject(ParseConstants.CLASS_NAME_GROUP);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_group_set_picture, container, false);

        groupImageView = (ImageView) view.findViewById(R.id.ic_group_default);
        loadingImageProgressBar = (ProgressBar)view.findViewById(R.id.progressBarGroupImage);

        //customize the actionBar
        TextView textViewGroupName = (TextView)getActivity().findViewById(R.id.action_bar_custom_title);
        Button actionBarBtn = (Button)getActivity().findViewById(R.id.action_bar_custom_button);
        actionBarBtn.setText(R.string.save);

        if (mGroup != null) {
            textViewGroupName.setText(mGroup.getName());

            Button savePictureBtn = (Button)getActivity().findViewById(R.id.action_bar_custom_button);
            savePictureBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mGroup.getPictureBitmap() != null) {
                        mGroup.saveInBackground(
                                new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {

                                        if (e == null) {
                                            CreateGroupSetPictureFragment.OnPictureSelected listener = (CreateGroupSetPictureFragment.OnPictureSelected) getActivity();
                                            listener.onPictureSelected();
                                        } else {
                                            Log.e(STTConstants.LOG_TAG, "ERROR :: User [" + ParseUser.getCurrentUser() + "] " +
                                                    "couldn´t save the picture file for the Group [" + mGroup.getObjectId() + " - " + mGroup.getName() + "]");
                                        }
                                    }
                                }
                        );
                    }
                }
            });

            Button addPictureBtn = (Button)view.findViewById(R.id.add_a_group_picture_button);
            addPictureBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //show a dialog to select the source of the picture
                    AlertDialog.Builder selectPicDialog = new AlertDialog.Builder(getActivity());
                    final CharSequence[] optionsChar = {
                            getActivity().getString(R.string.pick_gallery),
                            getActivity().getString(R.string.pick_camera),
                            getActivity().getString(R.string.pick_internet)};
                    selectPicDialog.setTitle(getActivity().getResources().getString(R.string.add_a_group_picture));
                    selectPicDialog.setItems(optionsChar, dialogClickListener);
                    selectPicDialog.show();
                }
            });

            Button notNowBtn = (Button)view.findViewById(R.id.not_now_button);
            notNowBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CreateGroupSetPictureFragment.OnPictureSelected listener = (CreateGroupSetPictureFragment.OnPictureSelected) getActivity();
                    listener.onPictureSelected();
                }
            });

        }else{

            Log.w(STTConstants.LOG_TAG,"WARNING :: " + this.getClass().getSimpleName() + " :: GROUP is NULL");

            STTUtils.showSomethingWentWrongAlert(getActivity());

        }
        return view;
    }

    public interface OnPictureSelected {
        void onPictureSelected();
    }

    final android.content.DialogInterface.OnClickListener dialogClickListener = new android.content.DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface dialog, int selected) {
            if(selected == 0){
                //SELECT FROM GALLERY
                Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT, null);
                pickPhoto.setType("image/*");
                startActivityForResult(pickPhoto, PICK_FROM_GALLERY);
            }else if(selected == 1){
                File file = STTUtils.createTempImageFile();
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(file != null)
                {
                    //add the path to save the temporal image file
                    mMakePhotoUri = Uri.fromFile(file);
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT,mMakePhotoUri);
                }
                startActivityForResult(takePicture, PICK_FROM_CAMERA);
            }else if(selected == 2){
                //TODO: [MIGRATION_2016]
                //Intent pickPhoto = new Intent(getActivity(), SearchImageActivity.class);
                //startActivityForResult(pickPhoto , PICK_FROM_INTERNET);
            }
            dialog.dismiss();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case PICK_FROM_CAMERA:
            case PICK_FROM_GALLERY:
                if(resultCode == Activity.RESULT_OK){
                    //get the uri from the intent data
                    Uri selectedImage;
                    if(requestCode == PICK_FROM_CAMERA  && mMakePhotoUri != null)
                        //for the camera a temporal image file will be used
                        selectedImage = mMakePhotoUri;
                    else
                        selectedImage = imageReturnedIntent.getData();
                    try {
                        //thumbnail Bitmap
                        int THUMBNAIL_SIZE = getResources().getInteger(R.integer.picture_group_thumbnail_size);
                        Bitmap thumbnailBitmap = STTUtils.getBitmapFromUriUsingDescriptor(getActivity() , selectedImage, THUMBNAIL_SIZE);
                        if(thumbnailBitmap == null)
                            STTUtils.showSomethingWentWrongAlert(getActivity(), getActivity().getString(R.string.image_error));
                        else
                        {
                            //circle Bitmap
                            Bitmap circleBitmap = STTUtils.getCircleClip(thumbnailBitmap, 0, 0, null);
                            //saving bitmap image
                            mGroup.setPictureFile(circleBitmap);
                            //set bitmap to imageView
                            groupImageView.setImageBitmap(circleBitmap);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        STTUtils.showSomethingWentWrongAlert(getActivity(), getActivity().getString(R.string.image_error));
                    }
                }
                break;
            case PICK_FROM_INTERNET:
                if(resultCode == Activity.RESULT_OK)
                {
                    //download the image from the URL selected
                    ImageResult selectedImage = (ImageResult)imageReturnedIntent.getSerializableExtra("result");
                    loadingImageProgressBar.setVisibility(View.VISIBLE);
                    ImageDownloadAsyncTask.ImageDownloaderCallback callback = new ImageDownloadAsyncTask.ImageDownloaderCallback(){
                        @Override
                        public void setBitmapResult(Bitmap bitmapResult)
                        {
                            //thumbnail Bitmap
                            int THUMBNAIL_SIZE = getResources().getInteger(R.integer.picture_group_thumbnail_size);
                            //resizing bitmap
                            Bitmap newBitmap = STTUtils.scaleBitmap(bitmapResult,THUMBNAIL_SIZE);
                            //circle Bitmap
                            Bitmap circleBitmap = STTUtils.getCircleClip(newBitmap,0,0,null);
                            //saving bitmap image
                            mGroup.setPictureFile(circleBitmap);
                            //set bitmap to imageView
                            groupImageView.setImageBitmap(circleBitmap);

                            loadingImageProgressBar.setVisibility(View.INVISIBLE);
                    }
                    };
                    new ImageDownloadAsyncTask(callback).execute(selectedImage.getFullUrl());
                }
                break;
        }
    }


}
