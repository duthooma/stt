package labs.altomobile.com.stt.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.adapters.FeedAdapter;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Feed;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.requests.RequestManager;

public class FeedFragment extends Fragment implements OnRequestCallback<Feed>, SwipeRefreshLayout.OnRefreshListener{
    private RecyclerView mRecyclerView;
    private FeedAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private final List<Feed> mList = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgressBar;
    private TextView mEmptyFeed;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View viewFeed =inflater.inflate(R.layout.frag_feed,container,false);
        mRecyclerView = (RecyclerView)viewFeed.findViewById(R.id.recycler_feed);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if(mAdapter == null) {
            mAdapter = new FeedAdapter(mList);
        }
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setContext(getActivity());

        mSwipeRefreshLayout = (SwipeRefreshLayout)viewFeed.findViewById(R.id.swipeRefreshLayout_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mProgressBar = (ProgressBar) viewFeed.findViewById(R.id.loading);
        mProgressBar.setVisibility(View.VISIBLE);

        mEmptyFeed =(TextView)viewFeed.findViewById(R.id.empty_feed);
        if(!STTConstants.USE_FIREBASE)
            RequestManager.GET_FEED.onBackground(this);
        return viewFeed;
    }

    @Override
    public void onRefresh() {
        if(!STTConstants.USE_FIREBASE)
            RequestManager.GET_FEED.onBackground(this);

    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<Feed> list, ParseObjectMap parseObjectMapmap) {
        mList.clear();

        if(list.size()==0){
            mEmptyFeed.setVisibility(View.VISIBLE);
        }
        else
        {
            mEmptyFeed.setVisibility(View.GONE);

        }
        mList.addAll(list);
        mAdapter.setParseObjectMap(parseObjectMapmap);
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.notifyDataSetChanged();
        mProgressBar.setVisibility(View.INVISIBLE);

    }
}
