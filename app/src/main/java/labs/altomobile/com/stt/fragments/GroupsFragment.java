package labs.altomobile.com.stt.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateGroupActivity;
import labs.altomobile.com.stt.activities.SeeGroupDetailActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.utils.STTUtils;

public class GroupsFragment extends Fragment implements OnRequestCallback<Group>, SwipeRefreshLayout.OnRefreshListener {

    private ListView myGroupsLV;
    private ProgressBar loading;
    private TextView noGroupYet;

    private GroupsAdapter adapter;

    private List<Group> myGroups;

    private List<Object> mGroupSports;

    private int ic_sport_height_px;
    private int ic_sport_width_px;

    final private int IMAGE_PADDING_PX = 8;
    final private int ICON_SPORT_DP = 40;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //so the icons size is dp
        ic_sport_height_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ICON_SPORT_DP, getResources().getDisplayMetrics()));
        ic_sport_width_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ICON_SPORT_DP, getResources().getDisplayMetrics()));

        myGroups = new ArrayList<>();
        adapter = new GroupsAdapter(getActivity(), R.layout.row_groups, myGroups);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_groups, container, false);

        myGroupsLV = (ListView) view.findViewById(R.id.lv_my_groups);
        loading = (ProgressBar) view.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        noGroupYet = (TextView) view.findViewById(R.id.tv_no_group_yet);

        myGroupsLV.setAdapter(adapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        RequestManager.GET_GROUPS_OF_CURRENT_USER.onBackground(this);
        return view;
    }

    @Override
    public void onRefresh() {
        RequestManager.GET_GROUPS_OF_CURRENT_USER.onBackground(this);

    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<Group> list, ParseObjectMap parseObjectMapmap) {

        if(list.size() > 0){
            myGroups.clear();
            myGroups.addAll(list);
            adapter.notifyDataSetChanged();
        }else{
            noGroupYet.setVisibility(View.VISIBLE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
        loading.setVisibility(View.GONE);
    }

    private class GroupsAdapter extends ArrayAdapter<Group> {

        public GroupsAdapter(Context context, int resource,
                             List<Group> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_groups, parent, false);
            }

            final Group group = getItem(position);

            //Elements of the row
            ImageView groupTypeIcon = (ImageView) view.findViewById(R.id.imageview_ic_group);
            TextView groupNameTV = (TextView) view.findViewById(R.id.tv_group_name);
            TextView groupDescTV = (TextView) view.findViewById(R.id.tv_group_desc);
            RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout_full_row);
            LinearLayout gridViewContainer = (LinearLayout)view.findViewById(R.id.layout_gridview_container);

            mGroupSports = group.getSports();

            ViewGroup.LayoutParams params = gridViewContainer.getLayoutParams();
            params.width=(ic_sport_width_px +2* IMAGE_PADDING_PX)*mGroupSports.size(); //we use this trick in order to resize the layout to match exactly the size of X icons plus 2*padding of each icon
            //this trick is needed in order to align the layout on the right

            GridView sportsGridView = (GridView)view.findViewById(R.id.gridview_sports);
            sportsGridView.setAdapter(new ImageAdapter(getActivity()));

            //if there is not group picture it draws the default
            if(group.getPictureBitmap() == null)
                Glide.with(GroupsFragment.this).load(R.drawable.ic_group_default).into(groupTypeIcon);
            else
                Glide.with(GroupsFragment.this).load(group.getPictureBitmap()).into(groupTypeIcon);

            groupNameTV.setText(group.getName());
            groupDescTV.setText(group.getDescription());

            layout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    redirectToGroup(group);
                }
            });

            return view;
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mGroupSports.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {

                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);

                imageView.setLayoutParams(new GridView.LayoutParams(ic_sport_height_px, ic_sport_width_px));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(IMAGE_PADDING_PX, IMAGE_PADDING_PX, IMAGE_PADDING_PX, IMAGE_PADDING_PX);
            } else {
                imageView = (ImageView) convertView;
            }

            String resourceId = STTConstants.FILE_NAME_ICONSPORT + mGroupSports.get(position);

            Glide.with(GroupsFragment.this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(imageView);
            return imageView;
        }
    }

    private void redirectToGroup(Group group){
        Intent intent = new Intent(getActivity(), SeeGroupDetailActivity.class);
        SerializableParseObject sgroup = new SerializableParseObject();
        sgroup.setParseObject(group);
        intent.putExtra(CreateGroupActivity.GROUP_SERIALIZABLE, sgroup);

        startActivity(intent);
    }
}
