package labs.altomobile.com.stt.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.activities.CreateGroupActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.firebase.User;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetFriendsFirebase;
import labs.altomobile.com.stt.utils.Debug;


public class InviteFriendsFragment extends Fragment implements OnRequestCallback<ParseUser>,OnFirebaseRequestCallback<User> {

    //private int mDisplayOption;

    //private Group mGroup;
    private ListView mFriendsLV;
    private FriendsAdapter mAdapter;
    private FirebaseFriendsAdapter mFirebaseAdapter;
    private List<ParseUser> mAllFriends;
    private List<User> mAllFirebaseFriends;

    private TextView mNoFriendToInviteTV;
    //private TextView mSeparatorTV;
    //private TextView mNotNowTV;
    private ProgressBar mLoading;
    //private HashSet<String> mFriendsToInvite = new HashSet<>();

    public final static String DISPLAY_OPTION = "DISPLAY_OPTION";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle arguments = getArguments();

        //if (arguments != null) {
            //mDisplayOption = arguments.getInt(DISPLAY_OPTION,-1);

            //if(mDisplayOption == AppSettingsConstants.DISPLAY_OPTION_CREATE_GROUP) {
            if(getActivity() instanceof CreateGroupActivity) {

                /*Log.i(AppSettingsConstants.LOG_TAG,this.getClass().getSimpleName() + " :: mDisplayOption == DISPLAY_OPTION_CREATE_GROUP");

                SerializableParseObject sgroup = (SerializableParseObject) arguments.get(CreateGroupActivity.GROUP_SERIALIZABLE);
                if (sgroup != null) {
                    mGroup = (Group) sgroup.getParseObject(ParseConstants.CLASS_NAME_GROUP);
                }*/
            }else if (getActivity() instanceof CreateChallengeActivity) {
                Log.i(STTConstants.LOG_TAG,this.getClass().getSimpleName() + " :: mDisplayOption == DISPLAY_OPTION_CREATE_CHALLENGE");

            }else{
                Debug.Error("WARNING :: using " + this.getClass().getSimpleName() + " from an unsupported activity");
            }
        //}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invite_friends, container, false);

        mNoFriendToInviteTV = (TextView) view.findViewById(R.id.textview_no_friend_to_invite);

        mLoading = (ProgressBar) view.findViewById(R.id.loading);

        /*
        Button inviteFriendsBtn = (Button) getActivity().findViewById(R.id.action_bar_custom_button);
        inviteFriendsBtn.setText(R.string.invite);

        inviteFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mFriendsToInvite.size() > 0) {

                    mLoading.setVisibility(View.VISIBLE);

                    Log.i(AppSettingsConstants.LOG_TAG, "The user [" + ParseUser.getCurrentUser().getObjectId() + "] " +
                            "is inviting [" + mFriendsToInvite.size() + "] Friends to Group [" + mGroup.getObjectId() + " - " + mGroup.getName() + "]");

                    List<GroupVsUser> friendsToInviteGVU = new ArrayList<>();

                    for (String friendToInvite : mFriendsToInvite) {
                        GroupVsUser groupVsUser = ParseObject.create(GroupVsUser.class);

                        groupVsUser.setGroupId(mGroup.getObjectId());
                        groupVsUser.setUserId(friendToInvite);
                        groupVsUser.setAcceptationStatus(Group.INVITATION_ACCEPTED);

                        friendsToInviteGVU.add(groupVsUser);
                    }

                    ParseObject.saveAllInBackground(friendsToInviteGVU, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                mLoading.setVisibility(View.GONE);
                                Log.i(AppSettingsConstants.LOG_TAG, "The user [" + ParseUser.getCurrentUser().getObjectId() + "] " +
                                        "successfully invited [" + mFriendsToInvite.size() + "] Friends to Group [" + mGroup.getObjectId() + " - " + mGroup.getName() + "]");
                                Toast.makeText(getActivity(), getResources().getText(R.string.friends_invited), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), SeeGroupDetailActivity.class);

                                SerializableParseObject sgroup = new SerializableParseObject();
                                sgroup.setParseObject(mGroup);
                                intent.putExtra(CreateGroupActivity.GROUP_SERIALIZABLE, sgroup);

                                startActivity(intent);
                            } else {
                                Log.e(AppSettingsConstants.LOG_TAG, "ERROR:: the user [" + ParseUser.getCurrentUser().getObjectId() + "] tried to invite Friends " +
                                        "to the Group [" + mGroup.getObjectId() + " - " + mGroup.getName() + "] but it failed : " + e.getMessage());
                            }
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), getText(R.string.you_need_to_select_at_least_one_friend), Toast.LENGTH_LONG).show();
                }
            }
        });*/


        mFriendsLV = (ListView) view.findViewById(R.id.list_view_friends);
        mAllFriends = new ArrayList<>();
        mAllFirebaseFriends = new ArrayList<>();


        if (getActivity() instanceof CreateGroupActivity) {
            mAdapter = new FriendsAdapter(getActivity(), R.layout.row_invite_friends_with_checkbox, mAllFriends);
            mFirebaseAdapter = new FirebaseFriendsAdapter(getActivity(),R.layout.row_invite_friends_with_checkbox,mAllFirebaseFriends);
            if (STTConstants.USE_FIREBASE)
                mFriendsLV.setAdapter(mFirebaseAdapter);
            else
                mFriendsLV.setAdapter(mAdapter);
            /*
            TextView textViewGroupName = (TextView) getActivity().findViewById(R.id.action_bar_custom_title);
            TextView inviteFriendsLabelTV = (TextView)view.findViewById(R.id.textview_header);
            inviteFriendsLabelTV.setVisibility(View.VISIBLE);

            mSeparatorTV = (TextView) view.findViewById(R.id.text_view_separator);
            mNotNowTV = (TextView) view.findViewById(R.id.not_now_button);


            if (mGroup != null) {
                textViewGroupName.setText(mGroup.getName());

                //TODO display the group icon if set

                RequestManager.GET_FRIENDS_TO_INVITE_A_GROUP.onBackground(RequestParams.create().add(Group.GROUP_ID, mGroup.getObjectId()), this);

                Button notNowBtn = (Button) view.findViewById(R.id.not_now_button);

                notNowBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), SeeGroupDetailActivity.class);

                        SerializableParseObject sgroup = new SerializableParseObject();
                        sgroup.setParseObject(mGroup);
                        intent.putExtra(CreateGroupActivity.GROUP_SERIALIZABLE, sgroup);

                        startActivity(intent);
                    }
                });

            } else {

                Log.w(AppSettingsConstants.LOG_TAG,"WARNING :: " + this.getClass().getSimpleName() + " :: GROUP is NULL");

                STTUtils.showSomethingWentWrongAlert(getActivity());

            }*/
        } else if (getActivity() instanceof CreateChallengeActivity) {
            mAdapter = new FriendsAdapter(getActivity(), R.layout.row_invite_friends, mAllFriends);
            mFirebaseAdapter = new FirebaseFriendsAdapter(getActivity(),R.layout.row_invite_friends,mAllFirebaseFriends);

            if (STTConstants.USE_FIREBASE) {
                mFriendsLV.setAdapter(mFirebaseAdapter);
                new GetFriendsFirebase().onBackground(this);
            } else {
                mFriendsLV.setAdapter(mAdapter);
                RequestManager.GET_FRIENDS_OF_CURRENT_USER.onBackground(this);
            }
        }else {
            Log.w(STTConstants.LOG_TAG, "WARNING :: using " + this.getClass().getSimpleName() + " with an invalid DISPLAY OPTION");
        }
        return view;
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<ParseUser> list, ParseObjectMap parseObjectMapmap) {

        mLoading.setVisibility(View.GONE);

        if(list.size() == 0){
            mNoFriendToInviteTV.setVisibility(View.VISIBLE);

        }else{
            if (getActivity() instanceof CreateGroupActivity) {
                /*mSeparatorTV.setVisibility(View.VISIBLE);
                mNotNowTV.setVisibility(View.VISIBLE);*/
            }
        }

        mAllFriends.clear();
        mAllFriends.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestFinished(List<User> list) {
        mLoading.setVisibility(View.GONE);

        if(list.size() == 0){
            mNoFriendToInviteTV.setVisibility(View.VISIBLE);

        }else{
            if (getActivity() instanceof CreateGroupActivity) {
                /*mSeparatorTV.setVisibility(View.VISIBLE);
                mNotNowTV.setVisibility(View.VISIBLE);*/
            }
        }

        mAllFirebaseFriends.clear();
        mAllFirebaseFriends.addAll(list);
        mFirebaseAdapter.notifyDataSetChanged();
    }

    public interface OnFriendSelected {
        void onFriendSelected(ParseUser parseUser);
        void onFirebaseFriendSelected(User user);
    }

    private class FriendsAdapter extends ArrayAdapter<ParseUser> {

        int resource;

        public FriendsAdapter(Context context, int resource,
                              List<ParseUser> objects) {
            super(context, resource, objects);

            this.resource = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(resource, parent, false);
            }

            final ParseUser user = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.friendtoinvite_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.friendtoinvite_picture);

            profilePictureView.setProfileId(user.getString(STTConstants.FACEBOOK_ID));
            nameTextV.setText(user.getUsername());

            if (getActivity() instanceof CreateGroupActivity) {
                /*
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
                checkBox.setVisibility(View.VISIBLE);
                checkBox.setClickable(false);

                final RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.layout_row_invite_friends);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int id = checkBox.getId();

                        if (mFriendsToInvite.contains(mAllFriends.get(position).getObjectId())){
                            mFriendsToInvite.remove(mAllFriends.get(position).getObjectId());
                            checkBox.setChecked(false);
                            layout.setBackgroundColor(0x00000000);
                        }else{
                            mFriendsToInvite.add(mAllFriends.get(position).getObjectId());
                            checkBox.setChecked(true);
                            layout.setBackgroundColor(getResources().getColor(R.color.very_light_blue));
                        }
                    }
                });
                */

            } else if (getActivity() instanceof CreateChallengeActivity) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Get parent Activity and send notification
                        OnFriendSelected listener = (OnFriendSelected) getActivity();
                        listener.onFriendSelected(user);
                    }
                });
            }
            return view;
        }
    }

    private class FirebaseFriendsAdapter extends ArrayAdapter<User> {

        int resource;

        public FirebaseFriendsAdapter(Context context, int resource,
                              List<User> objects) {
            super(context, resource, objects);

            this.resource = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(resource, parent, false);
            }

            final User user = getItem(position);

            TextView nameTextV = (TextView) view.findViewById(R.id.friendtoinvite_name);
            ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.friendtoinvite_picture);

            profilePictureView.setProfileId(user.getFacebookId());
            nameTextV.setText(user.getUserName());

            if (getActivity() instanceof CreateGroupActivity) {
                /*
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
                checkBox.setVisibility(View.VISIBLE);
                checkBox.setClickable(false);

                final RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.layout_row_invite_friends);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int id = checkBox.getId();

                        if (mFriendsToInvite.contains(mAllFriends.get(position).getObjectId())){
                            mFriendsToInvite.remove(mAllFriends.get(position).getObjectId());
                            checkBox.setChecked(false);
                            layout.setBackgroundColor(0x00000000);
                        }else{
                            mFriendsToInvite.add(mAllFriends.get(position).getObjectId());
                            checkBox.setChecked(true);
                            layout.setBackgroundColor(getResources().getColor(R.color.very_light_blue));
                        }
                    }
                });
                */

            } else if (getActivity() instanceof CreateChallengeActivity) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Get parent Activity and send notification
                        OnFriendSelected listener = (OnFriendSelected) getActivity();
                        listener.onFirebaseFriendSelected(user);
                    }
                });
            }
            return view;
        }
    }
}