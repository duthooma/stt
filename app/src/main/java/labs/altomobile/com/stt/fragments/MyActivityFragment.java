package labs.altomobile.com.stt.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.objects.firebase.TrainingFirebase;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.firebase.GetMyTrainingFirebase;
import labs.altomobile.com.stt.utils.Debug;
import labs.altomobile.com.stt.utils.STTUtils;

public class MyActivityFragment extends Fragment  implements OnRequestCallback<Training> , SwipeRefreshLayout.OnRefreshListener, OnFirebaseRequestCallback<TrainingFirebase>{


    private ProgressBar mProgressBar;
    private ListView mListView;
    private TextView mNoTrainingYet;

    private List<Training> mTrainings;
    private List<TrainingFirebase> mTrainingsFirebase;
    private TrainingAdapter mAdapter;
    private TrainingFirebaseAdapter mFirebaseAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTrainings = new ArrayList<>();
        mTrainingsFirebase = new ArrayList<>();
        mAdapter = new TrainingAdapter(getActivity(), R.layout.row_friend_trainings, mTrainings);
        mFirebaseAdapter = new TrainingFirebaseAdapter(getActivity(), R.layout.row_friend_trainings, mTrainingsFirebase);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_activity, container, false);


        mListView = (ListView) view.findViewById(R.id.listView);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mNoTrainingYet = (TextView) view.findViewById(R.id.textview_no_training_yet);
        if(STTConstants.USE_FIREBASE)
            mListView.setAdapter(mFirebaseAdapter);
        else
            mListView.setAdapter(mAdapter);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mProgressBar.setVisibility(View.VISIBLE);
        if(!STTConstants.USE_FIREBASE)
            RequestManager.GET_TRAINING_OF_MYSELF.onBackground(this);
        else
            new GetMyTrainingFirebase().onBackground(this);
        return view;
    }

    @Override
    public void onRequestBegin() {

    }

    @Override
    public void onRequestFinished(List<Training> list, ParseObjectMap parseObjectMapmap) {

        if(list.size() > 0){
            mTrainings.clear();
            mTrainings.addAll(list);
            mAdapter.notifyDataSetChanged();
        }else{
            mNoTrainingYet.setVisibility(View.VISIBLE);
        }

        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        if(!STTConstants.USE_FIREBASE)
            RequestManager.GET_TRAINING_OF_MYSELF.onBackground(this);
        else
            new GetMyTrainingFirebase().onBackground(this);
    }

    @Override
    public void onRequestFinished(List<TrainingFirebase> list) {
        Debug.d("Firebase", "training list size ="+list.size());
        if(list.size() > 0){
            mTrainingsFirebase.clear();
            mTrainingsFirebase.addAll(list);
            mFirebaseAdapter.notifyDataSetChanged();
        }else{
            mNoTrainingYet.setVisibility(View.VISIBLE);
        }

        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class TrainingAdapter extends ArrayAdapter<Training> {

        public TrainingAdapter(Context context, int resource,
                                      List<Training> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_my_trainings, parent, false);
            }
            Training training = mTrainings.get(position);

            TextView dateTV = (TextView) view.findViewById(R.id.tv_date);
            ImageView image = (ImageView)view.findViewById(R.id.imageView_sport_ic);

            dateTV.setText(training.getDateStr(getContext()));

            String resourceId = STTConstants.FILE_NAME_ICONSPORT + training.getSportId();
            Glide.with(MyActivityFragment.this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(image);

            return view;
        }
    }

    private class TrainingFirebaseAdapter extends ArrayAdapter<TrainingFirebase> {

        public TrainingFirebaseAdapter(Context context, int resource,
                               List<TrainingFirebase> objects) {
            super(context, resource, objects);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_my_trainings, parent, false);
            }
            TrainingFirebase training = mTrainingsFirebase.get(position);

            TextView dateTV = (TextView) view.findViewById(R.id.tv_date);
            ImageView image = (ImageView)view.findViewById(R.id.imageView_sport_ic);

            dateTV.setText(training.getDateStr(getContext()));

            String resourceId = STTConstants.FILE_NAME_ICONSPORT + training.getSportId();
            Glide.with(MyActivityFragment.this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(image);

            return view;
        }
    }
}
