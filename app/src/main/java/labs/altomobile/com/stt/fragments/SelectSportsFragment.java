package labs.altomobile.com.stt.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateChallengeActivity;
import labs.altomobile.com.stt.activities.CreateGroupActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.serializationUtils.SerializableParseObject;


public class SelectSportsFragment extends Fragment {

    private int mDisplayOption;

    private ImageAdapter adapter;

    private TypedArray sportIconIds;
    private HashSet<Integer> mSportsSelectedHashset;
    private Group mGroup;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sportIconIds = getResources().obtainTypedArray(R.array.sportIconIds);
        mSportsSelectedHashset = new HashSet<>();

        Bundle arguments = getArguments();

        if (arguments != null) {

            mDisplayOption = arguments.getInt(STTConstants.DISPLAY_OPTION,-1);

            if(mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_GROUP) {

                Log.i(STTConstants.LOG_TAG,this.getClass().getSimpleName() + " :: mDisplayOption == DISPLAY_OPTION_CREATE_GROUP");

                SerializableParseObject sgroup = (SerializableParseObject) arguments.get(CreateGroupActivity.GROUP_SERIALIZABLE);
                if (sgroup != null) {
                    mGroup = (Group) sgroup.getParseObject(ParseConstants.CLASS_NAME_GROUP);
                }
            }else if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE) {
                Log.i(STTConstants.LOG_TAG,this.getClass().getSimpleName() + " :: mDisplayOption == DISPLAY_OPTION_CREATE_CHALLENGE");



            }else{
                Log.w(STTConstants.LOG_TAG,"WARNING :: using " + this.getClass().getSimpleName() + " without any DISPLAY OPTION");
            }
        }else{
            Log.w(STTConstants.LOG_TAG,"WARNING :: using " + this.getClass().getSimpleName() + " without any argument");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_select_sports, container, false);

        //customize the actionBar
        TextView textViewGroupName = (TextView)getActivity().findViewById(R.id.action_bar_custom_title);
        //Button actionBarBtn = (Button)getActivity().findViewById(R.id.action_bar_custom_button);
        ((CreateChallengeActivity) getActivity()).mActionButton.setText(R.string.select);

        if (mDisplayOption != -1) {

            GridView gridview = (GridView) view.findViewById(R.id.gridview);
            adapter = new ImageAdapter();
            gridview.setAdapter(adapter);

            if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_GROUP && mGroup != null) {
                textViewGroupName.setText(mGroup.getName());



                Button selectSportsBtn = (Button) getActivity().findViewById(R.id.action_bar_custom_button);

                selectSportsBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        List<Integer> sportsSelected = new ArrayList<>(mSportsSelectedHashset);

                        mGroup.setSports(sportsSelected);
                        //TODO to improve
                        mGroup.saveInBackground(
                                new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {

                                        if (e == null) {
                                            //TODO how to let the user know about the success? Here Toast doesn´t work since the getActivity() returns null since we switched of fragment
                                            //Toast.makeText(getActivity(), getResources().getText(R.string.sports_selected), Toast.LENGTH_SHORT).show();
                                        } else {
                                            //Toast.makeText(getActivity(), getResources().getText(R.string.sports_couldnt_be_saved), Toast.LENGTH_SHORT).show();
                                            Log.e(STTConstants.LOG_TAG, "ERROR :: User [" + ParseUser.getCurrentUser() + "] " +
                                                    "couldn´t save the sports for the Group [" + mGroup.getObjectId() + " - " + mGroup.getName() + "]");
                                        }
                                    }
                                }
                        );

                        //Get parent Activity and send notification
                        OnSportsSelectedListener listener = (OnSportsSelectedListener) getActivity();
                        listener.onSportsSelected(sportsSelected);

                    }
                });
            } else if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE){
                //TODO to improve

            }else{
                Log.w(STTConstants.LOG_TAG, "WARNING :: using " + this.getClass().getSimpleName() + " with an invalid DISPLAY OPTION");
            }
        }else{
            Log.w(STTConstants.LOG_TAG, "WARNING :: using " + this.getClass().getSimpleName() + " without a DISPLAY OPTION");
        }
        return view;
    }

    public interface OnSportsSelectedListener {
        public void onSportsSelected(List<Integer> sportsSelected);
    }

    public interface OnSportSelectedListener {
        public void onSportSelected(int sportId);
    }

    public class ImageAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ImageAdapter() {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return sportIconIds.length();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View view = convertView;

            if (view == null) {
                holder = new ViewHolder();
                view = mInflater.inflate(
                        R.layout.row_gridview_selectsports, null);
                holder.imageview = (ImageView) view.findViewById(R.id.thumbImage);
                holder.imageview.setLayoutParams(new RelativeLayout.LayoutParams(250, 250));
                holder.checkbox = (CheckBox) view.findViewById(R.id.itemCheckBox);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_GROUP) {

                holder.checkbox.setId(position);
                holder.imageview.setId(position);
                holder.checkbox.setVisibility(View.VISIBLE);
                holder.checkbox.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        CheckBox cb = (CheckBox) v;
                        int id = cb.getId();

                        if (mSportsSelectedHashset.contains(position)) {
                            mSportsSelectedHashset.remove(position);
                            //cb.setChecked(false);
                        } else {
                            mSportsSelectedHashset.add(position);
                        }
                    }
                });
            }else if (mDisplayOption == STTConstants.DISPLAY_OPTION_CREATE_CHALLENGE) {

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Get parent Activity and send notification
                        OnSportSelectedListener listener = (OnSportSelectedListener) getActivity();
                        listener.onSportSelected(position);
                    }
                });

            }

            Glide.with(SelectSportsFragment.this).load(sportIconIds.getResourceId(position, -1)).centerCrop().into(holder.imageview);
            //holder.imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.id = position;
            return view;
        }
    }

    class ViewHolder {
        ImageView imageview;
        CheckBox checkbox;
        int id;
    }
}
