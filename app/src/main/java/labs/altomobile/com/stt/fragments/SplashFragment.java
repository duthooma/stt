package labs.altomobile.com.stt.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

import labs.altomobile.com.stt.R;

public class SplashFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash,
                container, false);
        LoginButton authButton = (LoginButton) view.findViewById(R.id.login_button);
        authButton.setReadPermissions(Arrays.asList("user_friends"));

        return view;
    }

}
