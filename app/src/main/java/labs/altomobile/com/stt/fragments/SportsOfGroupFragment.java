package labs.altomobile.com.stt.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.CreateGroupActivity;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.utils.STTUtils;


public class SportsOfGroupFragment extends Fragment {

    private ArrayList<Integer> mSports;
    private int ht_px;
    private int wt_px;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //so the icons size is dp
        ht_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));
        wt_px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));

        Bundle arguments = getArguments();

        if (arguments != null) {
            mSports = arguments.getIntegerArrayList(CreateGroupActivity.SPORTS_SELECTED);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sports_of_group, container, false);

        //customize the actionBar
        TextView textViewGroupName = (TextView)getActivity().findViewById(R.id.action_bar_custom_title);
        Button actionBarBtn = (Button)getActivity().findViewById(R.id.action_bar_custom_button);
        actionBarBtn.setText(R.string.save);


        if (mSports != null) {

            GridView sportsGridView = (GridView) view.findViewById(R.id.gridview_sports);
            sportsGridView.setAdapter(new ImageAdapter(getActivity()));

        }else{
            Log.w(STTConstants.LOG_TAG,"WARNING :: " + this.getClass().getSimpleName() + " :: mSports is NULL");

            STTUtils.showSomethingWentWrongAlert(getActivity());

        }
        return view;
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mSports.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);

                imageView.setLayoutParams(new GridView.LayoutParams(ht_px, wt_px));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            String resourceId = STTConstants.FILE_NAME_ICONSPORT + mSports.get(position);

            Glide.with(SportsOfGroupFragment.this).load(STTUtils.getResourceId(getActivity(), resourceId, STTConstants.RESOURCE_DRAWABLE)).into(imageView);
            return imageView;
        }
    }

}
