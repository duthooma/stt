package labs.altomobile.com.stt.helpers;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;
import labs.altomobile.com.stt.objects.firebase.FeedFirebase;
import labs.altomobile.com.stt.objects.firebase.TrainingFirebase;
import labs.altomobile.com.stt.objects.firebase.User;

/**
 * Created by Flavio on 10/03/2016.
 * This contains some simple Firebase queries and processes
 */
public class FirebaseHelper {

    private static Firebase sharedFirebase = null;
    private static String userUid = "";
    private static User currentUser;

    public static Firebase getSharedFirebase() {
        if(sharedFirebase == null)
            return   new Firebase("https://scorching-heat-8813.firebaseio.com/");
        else
            return sharedFirebase;
    }

    public static void setLoggedUser(AuthData authData, Firebase.CompletionListener listener){
        userUid = authData.getUid();
        Firebase userRef = getSharedFirebase().child(FirebaseConstants.CHILD_USERS).child(userUid+"/"+FirebaseConstants.CHILD_USER_PROFILE);
        Map providerData = authData.getProviderData();

        String userName = providerData.get("displayName").toString();
        String facebookId = providerData.get("id").toString();
        String profileImageURL = providerData.get("profileImageURL").toString();
        currentUser = new User(userUid,userName,facebookId,profileImageURL);
        userRef.updateChildren(currentUser.getMapToSave(),listener);
    }

    public static String getUserUid(){
        return userUid;
    }

    public static User getCurrentUser(){
        return currentUser;
    }

    public static void createTraining(int sportId, long date, String placeId, Firebase.CompletionListener listener){
        TrainingFirebase trianing = new TrainingFirebase(date,placeId,sportId,userUid);
        getSharedFirebase().child(FirebaseConstants.CHILD_TRAININGS+"/"+userUid).push().setValue(trianing, listener);
    }

    /**
     * Create a friend request adding the currentUser Uid and the friends Uids to friends_request_from and friends_request_to.
     * @param friendsToInvite List of facebook Id's Who will get the request.
     */
    public static void createFriendRequest(List<String> friendsToInvite){
        for(String friendFacebookId : friendsToInvite){
            //in Firebase the user Id is saved with the prefix 'facebook:' plus the facebook Id
            String friendUid = STTConstants.FACEBOOK_KEY_PREFIX+friendFacebookId;
            getSharedFirebase().child(FirebaseConstants.USER_REQUEST_FROM+"/"+userUid+"/"+friendUid).setValue(true);
            getSharedFirebase().child(FirebaseConstants.USER_REQUEST_TO+"/"+friendUid+"/"+userUid).setValue(true);
        }
    }
    /**
     * Accept a friend request by removen the users's Uid from friends_request_from and friends_request_to.
     * Then adds theirs Uids to 'users/friends/' in order to index and set each other as a friend.
     * @param friendUid Uid Who did the friend request. Example facebook:45664636.
     */
    public static void acceptFriendRequest(String friendUid){
        getSharedFirebase().child(FirebaseConstants.CHILD_USERS+"/"+userUid+"/"+FirebaseConstants.USER_FRIENDS+"/"+friendUid).setValue(true);
        getSharedFirebase().child(FirebaseConstants.CHILD_USERS+"/"+friendUid+"/"+FirebaseConstants.USER_FRIENDS+"/"+userUid).setValue(true);
        getSharedFirebase().child(FirebaseConstants.USER_REQUEST_FROM+"/"+friendUid+"/"+userUid).removeValue();
        getSharedFirebase().child(FirebaseConstants.USER_REQUEST_TO+"/"+userUid+"/"+friendUid).removeValue();
    }

    /**
     * Create a new challenge with a listener to call when the process is completed
     * @param challengeFirebase challenge object to be created
     * @param listener interface to be called with the result when the Challenge is saved
     */
    public static void saveChallenge(final ChallengeFirebase challengeFirebase, final Firebase.CompletionListener listener){
        Firebase userRef = getSharedFirebase().child(FirebaseConstants.CHILD_CHALLENGES).child(challengeFirebase.getTo());
        final Firebase.CompletionListener localListener = new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if(firebaseError == null){
                    String key = firebase.getKey();
                    FeedFirebase feed = new FeedFirebase(key, FirebaseConstants.FEED_CHALLENGE, challengeFirebase.getTo());
                    feed.feedFriends(getSharedFirebase());
                    //getSharedFirebase().child(FirebaseConstants.CHILD_FEED+"/"+challengeFirebase.getFrom()).push().setValue(feed.getMapToSave());
                }
                listener.onComplete(firebaseError,firebase);
            }
        };
        userRef.push().setValue(challengeFirebase.getMapToSave(),localListener);
    }
}
