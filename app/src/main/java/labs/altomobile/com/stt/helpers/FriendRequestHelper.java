package labs.altomobile.com.stt.helpers;
import com.parse.ParseObject;

import org.json.JSONObject;

import java.util.List;

import labs.altomobile.com.stt.constants.FriendRequestConstants;
import labs.altomobile.com.stt.objects.FriendRequest;

/**
 * Created by danielmonterocervantes on 01/04/15.
 */
public class FriendRequestHelper {
    private static FriendRequestHelper sInstance = new FriendRequestHelper();

    public static FriendRequestHelper getInstance() {
        return sInstance;
    }

    public void sendFriendRequest(String from, String to) {
        FriendRequest friendRequest = ParseObject.create(FriendRequest.class);
        friendRequest.initialize(from, to, true);
        ParseHelper.save(friendRequest);

     }

    public  void acceptDuplicatedFriendRequest(String user1, String user2)
    {
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setHasAccepted(1);
        friendRequest.setIsSender(false);
        friendRequest.setUser1(user2);
        friendRequest.setUser2(user1);
        ParseHelper.save(friendRequest);

    }
    public void sendFriendRequestAutoAccepted(String from, String to) {
        FriendRequest friendRequest = ParseObject.create(FriendRequest.class);
        friendRequest.initialize(from, to, true);
        friendRequest.setHasAccepted(FriendRequest.FRIEND_REQUEST_ACCEPTED);
        ParseHelper.save(friendRequest);

     }

    public void sendFriendRequest(String from, List<JSONObject> to) {
        for (JSONObject friend : to) {
            sendFriendRequest(from, friend.optString("id"));
        }
    }

    public void sendFriendRequestList(String from, List<String> to) {
        for (String friend : to) {
            sendFriendRequest(from, friend);
        }
    }

}
