package labs.altomobile.com.stt.helpers;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.interfaces.OnSaveCallback;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.Notification;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Training;
import labs.altomobile.com.stt.objects.UserLevel;
import labs.altomobile.com.stt.requests.RequestManager;
import labs.altomobile.com.stt.requests.RequestParams;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */
public class ParseHelper {
    public static void save(ParseObject parseObject) {
        parseObject.saveInBackground();

    }

    public static void saveChallenge(final Challenge challenge, final OnSaveCallback<Challenge> callback) {

        challenge.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                callback.onSaveObject(challenge);


                saveNotification(challenge.getTo(), Notification.Type.CHALLENGE);
            }
        });
    }



    public static boolean saveInForeground(ParseObject parseObject) {
        try {
            parseObject.save();
            return true;
        } catch (ParseException pe) {
            return false;
        }

    }


    public static void fletch(ParseObject parseObject) {
        parseObject.fetchInBackground();
    }

    public static void delete(ParseObject parseObject) {
        parseObject.deleteInBackground();
    }


    public static void createTrainingInBackground(int sportId, long date, String placeId, final OnSaveCallback<Training> callback) {

        final Training trainingParseObject = new Training();

        trainingParseObject.setUser(ParseUser.getCurrentUser());
        trainingParseObject.setLongDate(date);
        trainingParseObject.setSportId(sportId);
        if (placeId != null)
            trainingParseObject.setPlaceId(placeId);
        trainingParseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                callback.onSaveObject(trainingParseObject);
                //update UserLevel
                RequestParams requestParams = RequestParams.create();
                requestParams.add("user_idd", ParseUser.getCurrentUser().getObjectId()); //TODO fix it. This is TERRIBLE

               
                RequestManager.UPDATE_USER_LEVEL_OF_USER.onBackground(requestParams, new OnRequestCallback<UserLevel>() {
                    @Override
                    public void onRequestBegin() {

                    }

                    @Override
                    public void onRequestFinished(List<UserLevel> list, ParseObjectMap parseObjectMapmap) {

                        //TODO log something?
                    }
                });
            }
        });


    }

    public static void createGroupInvitation(String parseUserId, String groupId) {
        GroupVsUser groupVsUser = ParseObject.create(GroupVsUser.class);
        groupVsUser.setGroupId(groupId);
        groupVsUser.setUserId(parseUserId);
        groupVsUser.setAcceptationStatus(Group.INVITATION_PENDING);
        groupVsUser.setRole(Group.ROLE_OWNER);
        ParseHelper.save(groupVsUser);

    }

    public static void createGroupAndRelatedObjects(String groupName, String groupDescription, final OnSaveCallback<Group> callback) {

        final Group group = ParseObject.create(Group.class);
        group.setName(groupName);
        group.setDescription(groupDescription);
        group.setSports( new ArrayList<Integer>());

        group.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                // 1: create GroupVSUSer objects
                //1.1: automatically add the owner to the Group

                GroupVsUser groupVSUser = ParseObject.create(GroupVsUser.class);
                groupVSUser.setGroupId(group.getObjectId());
                groupVSUser.setUserId(ParseUser.getCurrentUser().getObjectId());
                ParseHelper.save(groupVSUser);

                callback.onSaveObject(group);
            }
        });
    }

    public static void sortByUpdateDate(final List<? extends ParseObject> list)
    {
        Collections.sort(list,DateComparator);
    }
    public static Comparator<ParseObject> DateComparator = new Comparator<ParseObject>() {

        public int compare(ParseObject p1, ParseObject p2) {

            Date d1 = p1.getUpdatedAt();
            Date d2 = p2.getUpdatedAt();
            return d2.compareTo(d1);

        }};

    public static void saveNotification(final ParseUser  parseUser, final Notification.Type type)
    {
        ParseQuery<Notification> notificationParseQuery = ParseQuery.getQuery(Notification.class);
        notificationParseQuery.whereEqualTo(ParseConstants.USER_REFERENCE,parseUser);
        notificationParseQuery.whereEqualTo(Notification.NOTIFICATION_TYPE, type.getValue());
        notificationParseQuery.getFirstInBackground(new GetCallback<Notification>() {
            @Override
            public void done(Notification notification, ParseException e) {
                if (notification == null) {
                    notification = new Notification();
                    notification.setType(type);
                    notification.initCounter();
                    notification.setUser(parseUser);
                }
                else {
                    notification.increment();
                }
                ParseHelper.save(notification);

            }
        });

    }

}
