package labs.altomobile.com.stt.interfaces;

import java.util.List;

import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by Flavio on 10/03/2016.
 */
public interface OnFirebaseRequestCallback<FObject extends Object> {
    void onRequestFinished(List<FObject> list);
}
