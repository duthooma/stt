package labs.altomobile.com.stt.interfaces;

import com.parse.ParseObject;

import java.util.List;

import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
public interface OnRequestCallback<PObject extends ParseObject> {
    void onRequestBegin();
    void onRequestFinished(List<PObject> list, ParseObjectMap parseObjectMapmap);
}
