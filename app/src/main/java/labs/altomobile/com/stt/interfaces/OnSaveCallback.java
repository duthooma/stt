package labs.altomobile.com.stt.interfaces;

import com.parse.ParseObject;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */
public interface OnSaveCallback<PObject extends ParseObject> {
    void onSaveObject(PObject pObject);
}
