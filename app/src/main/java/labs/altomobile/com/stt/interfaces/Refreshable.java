package labs.altomobile.com.stt.interfaces;

/**
 * Created by danielmonterocervantes on 27/03/15.
 */
public interface Refreshable {
    void refresh();
}
