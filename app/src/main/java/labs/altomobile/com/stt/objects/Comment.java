package labs.altomobile.com.stt.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 09/06/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_COMMENT)
public class Comment extends ParseObject{


    public static final String COMMENT_CONTENT = "content";

    public void setUser(ParseUser parseUser)
    {
        put(ParseConstants.USER_REFERENCE,parseUser);
    }


    public ParseUser getUser()
    {
        return getParseUser(ParseConstants.USER_REFERENCE);
    }

    public void setContent(String content)
    {
        put(COMMENT_CONTENT,content);
    }

    public String getContent()
    {
        return getString(COMMENT_CONTENT);
    }

    public void setChallenge(Challenge challenge)
    {
        put(Feed.REFERENCE_NAME,challenge.getClassName());
        put(Feed.CHALLENGE_REF,challenge);
    }
    public Challenge getChallenge()
    {
        return (Challenge)get(Feed.CHALLENGE_REF);
    }

    public void setTraining(Training training)
    {
        put(Feed.REFERENCE_NAME,training.getClassName());
        put(Feed.CHALLENGE_REF,training);
    }
    public Training getTraining()
    {
        return (Training)get(Feed.TRAINING_REF);
    }
}
