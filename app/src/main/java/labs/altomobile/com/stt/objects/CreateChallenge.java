package labs.altomobile.com.stt.objects;

import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by maxence on 3/11/16.
 */
public class CreateChallenge implements Serializable{

    private ParseUser toUser = null;
    private int sportId = -1;
    private String title = "";
    private String description = "";
    private long endDate = -1;
    private ChallengeReward reward = null;

    CreateChallenge(ParseUser toUser){
        this.toUser = toUser;
    }

}
