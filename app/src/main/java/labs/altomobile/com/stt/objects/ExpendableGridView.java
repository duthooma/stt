package labs.altomobile.com.stt.objects;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by maxence on 1/3/16.
 * http://stackoverflow.com/questions/6005245/how-to-have-a-gridview-that-adapts-its-height-when-items-are-added
 * Needed so the gridview expands automatically (with wrapcontent). A regular Gridview only displays one line if height = wrapcontent
 */

public class ExpendableGridView extends GridView {
    public ExpendableGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpendableGridView(Context context) {
        super(context);
    }

    public ExpendableGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
