package labs.altomobile.com.stt.objects;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.helpers.ParseHelper;

/**
 * Created by danielmonterocervantes on 05/06/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_FEED)
public class Feed extends ParseObject {

    public static final String REFERENCE_NAME = "referenceName";
    public static final String MODIFIED = "modified";

    public static final String CHALLENGE_REF = "challengeReference";
    public static final String TRAINING_REF = "trainingReference";

    public void modify()
    {
        put(MODIFIED,true);
    }



     public ParseUser getUser()
    {
        return getParseUser(ParseConstants.USER_REFERENCE);
    }



    public String getRelatedObjectClassName()
    {
        return getString(REFERENCE_NAME);
    }


    public Challenge getChallenge()
    {
        return (Challenge)get(CHALLENGE_REF);
    }


    public Training getTraining()
    {
        return (Training)get(TRAINING_REF);
    }


}
