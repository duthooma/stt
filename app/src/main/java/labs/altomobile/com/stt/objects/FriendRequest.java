package labs.altomobile.com.stt.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import labs.altomobile.com.stt.constants.FriendRequestConstants;
import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 30/03/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_FRIENDREQUEST)
public class FriendRequest extends ParseObject {

    //FIELDS of FriendRequest
    public final static String PARSEFIELD_USER1 = "user1";
    public final static String PARSEFIELD_USER2 = "user2";
    public final static String PARSEFIELD_ACCEPTATION_STATUS = ParseConstants.ACCEPTATION_STATUS;
    public final static String PARSEFIELD_IS_SENDER = "is_sender";

    public static final int FRIEND_REQUEST_ACCEPTED = 1;
    public static final int FRIEND_REQUEST_DENIED = -1;
    public static final int FRIEND_REQUEST_PENDING = 0;

    public static final boolean FRIEND_REQUEST_IS_NOT_CLONE = false;
    public static final boolean FRIEND_REQUEST_IS_CLONE = true;

    public void initialize(String from, String to, boolean isSender) {
        put(PARSEFIELD_USER1, from);
        put(PARSEFIELD_USER2, to);
        put(PARSEFIELD_ACCEPTATION_STATUS, FRIEND_REQUEST_PENDING);
        put(PARSEFIELD_IS_SENDER, isSender);
    }

    public String getUser1()
    {
        return getString(PARSEFIELD_USER1);
    }

    public String getUser2()
    {
        return getString(PARSEFIELD_USER2);
    }

    public void setUser1(String facebookId)
    {
        put(PARSEFIELD_USER1,facebookId);
    }

    public void setUser2(String facebookId)
    {
        put(PARSEFIELD_USER2,facebookId);
    }


    public void setIsSender(boolean isSender){
        put(PARSEFIELD_IS_SENDER,isSender);
    }

    public void setHasAccepted(int hasAccepted){
        put(PARSEFIELD_ACCEPTATION_STATUS,hasAccepted);
    }

}
