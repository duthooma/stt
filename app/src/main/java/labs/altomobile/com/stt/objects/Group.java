package labs.altomobile.com.stt.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.ByteArrayOutputStream;
import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 27/03/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_GROUP)
public class Group extends ParseObject {

    //FIELDS of Challenge
    public final static String PARSEFIELD_NAME = ParseConstants.NAME;
    public final static String PARSEFIELD_DESCRIPTION = ParseConstants.DESCRIPTION;
    public final static String PARSEFIELD_SPORTS = ParseConstants.SPORTS;
    public final static String PARSEFIELD_PICTURE = ParseConstants.PICTURE;

    public final static String GROUP_ID = ParseConstants.GROUP_ID;

    public static final int INVITATION_ACCEPTED = 1;
    public static final int INVITATION_PENDING = 0;
    public static final int INVITATION_REFUSED = -1;
    public static int ROLE_OWNER = 0;

    public String getName() {
        return getString(PARSEFIELD_NAME);
    }

    public void setName(String name) {
        put(PARSEFIELD_NAME, name);
    }

    public String getDescription() {
        return getString(PARSEFIELD_DESCRIPTION);
    }

    public void setDescription(String description) {
        put(PARSEFIELD_DESCRIPTION, description);
    }

    public void setSports(List<Integer> sports){
        put(PARSEFIELD_SPORTS, sports);
    }

    public List<Object> getSports(){return getList(PARSEFIELD_SPORTS);}


    public void setPictureFile(Bitmap bitmap)
    {
        //compress and convert the bitmap to Stream to get the bytes
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        //create a new ParseFile from the bitmap bytes
        ParseFile file  = new ParseFile("picture.png",byteArray);
        put(PARSEFIELD_PICTURE,file);
    }

    public Bitmap getPictureBitmap()
    {
        ParseFile file = getParseFile(PARSEFIELD_PICTURE);
        Bitmap result  = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true; //for future modifications it needs to be mutable

        //if there is a picture file it will try to decode to bitmap
        if(file != null) {
            try {
                result = BitmapFactory.decodeByteArray(file.getData(), 0, file.getData().length, options);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
