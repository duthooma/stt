package labs.altomobile.com.stt.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 03/04/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_GROUPVSUSER)
public class GroupVsUser extends ParseObject {



    //Parse Fields
    public static final String ACCEPTATION_STATUS = ParseConstants.ACCEPTATION_STATUS;
    public static final String ROLE = ParseConstants.ROLE;



    public String getGroupId() {
        return getString(Group.GROUP_ID);
    }

    public void setGroupId(String groupId) {
        put(Group.GROUP_ID, groupId);
    }

    public String getUserId() {
        return getString(ParseConstants.USER_ID);
    }

    public void setUserId(String userId) {
        put(ParseConstants.USER_ID, userId);
    }

    public int getRole() {
        return getInt(ROLE);
    }

    public void setRole(int role) {
        put(ROLE, role);
    }

    public int getAcceptationStatus() {
        return getInt(ACCEPTATION_STATUS);
    }

    public void setAcceptationStatus(int acceptationStatus) {
        put(ACCEPTATION_STATUS, acceptationStatus);
    }
}
