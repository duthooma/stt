package labs.altomobile.com.stt.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by flavioreyes on 6/12/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_NOTIFICATION)
public class Notification extends ParseObject {

    public enum Type {
        CHALLENGE(1), FRIEND_INVITATION(2), GROUP_INVITATION(3);

        private final int value;
        private Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }}

    public static final String NOTIFICATION_COUNTER = "notificationCounter";
    public static final String NOTIFICATION_TYPE = "type";

    public void setUser(ParseUser parseUser)
    {
        put(ParseConstants.USER_REFERENCE,parseUser);
    }

    public void setType(Type type)
    {
        put(NOTIFICATION_TYPE, type.getValue());
    }

    /*public void resetCounter()
    {
        put(NOTIFICATION_COUNTER,0);
    }*/

    public void initCounter()
    {
        put(NOTIFICATION_COUNTER,1);
    }

    public void increment()
    {
        increment(NOTIFICATION_COUNTER);
    }

    public int getCounter()
    {
        return getInt(NOTIFICATION_COUNTER);
    }

}
