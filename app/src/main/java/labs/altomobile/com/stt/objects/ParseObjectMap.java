package labs.altomobile.com.stt.objects;

import android.util.Log;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
public class ParseObjectMap {
    private HashMap<String, ParseObject> mMap;

    public ParseObjectMap() {
        mMap = new HashMap<>();
    }
    private static ParseObjectMap instance = new ParseObjectMap();
    public static ParseObjectMap getInstance()
    {
        return instance;
    }

    public FriendRequest getAsFriendRequest(String id) {
        return (FriendRequest) mMap.get(id);
    }

    public Place getAsPlaces(String id) {
        return (Place) mMap.get(id);
    }

    public Group getAsGroup(String id) {
        return (Group) mMap.get(id);
    }


    public void put(String id, ParseObject parseObject) {
        mMap.put(id, parseObject);
    }

    public ParseObject get(String id) {
        try {
            return mMap.get(id);
        }
        catch (Exception e)
        {
            Log.i("LALALA","Exception trying to recover object with id "+id);
            throw  e;
        }
    }

    public void add(ParseObject parseObject)
    {
        put(parseObject.getObjectId(),parseObject);

    }
    public void remove(String id)
    {
        mMap.remove(id);
    }
    
    public void addAll(Collection<ParseObject> parseObjectCollection)
    {
        for(ParseObject parseObject:parseObjectCollection)
        {
            add(parseObject);        
        }
    }

    public int size(){
        return mMap.size();
    }

    public void printForDebugPurpose(){
        Log.d(STTConstants.LOG_TAG, "ParseObjectMap size ==  " + size());
        List<ParseObject> list = new ArrayList<>(mMap.values());
        for (ParseObject p : list){
            Log.d(STTConstants.LOG_TAG, "ParseConstants.OBJECT_ID == " + p.getObjectId());
        }
    }
}
