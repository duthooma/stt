package labs.altomobile.com.stt.objects;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_PLACE)
public class Place extends ParseObject {

    //Parse Fields
    public static final String NAME = ParseConstants.NAME;

    //Join Fields
    public static final String PLACE_ID = ParseConstants.PLACE_ID;

    public String getName() {
        return getString(NAME);
    }

    public void setName(String name) {
        put(NAME, name);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public void setLocation(ParseGeoPoint location) {
        put("location", location);
    }

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");
    }

    public void setLocation(LatLng latLng) {
        setLocation(new ParseGeoPoint(latLng.latitude, latLng.longitude));
    }

    public LatLng getLatLng() {
        return new LatLng(getLocation().getLatitude(), getLocation().getLongitude());
    }

    public void setUserId(String userId) {
        put(ParseConstants.USER_ID, userId);
    }

    public void getUserId(String userId) {
        put(ParseConstants.USER_ID, userId);
    }


}
