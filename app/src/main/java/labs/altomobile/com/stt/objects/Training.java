package labs.altomobile.com.stt.objects;

import android.content.Context;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.common.DateUtil;
import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_TRAINING)
public class Training extends ParseObject {
    public static final String PARSE_DB_FIELD_DATE = ParseConstants.DATE;
    public static final String PARSE_DB_FIELD_SPORTID = ParseConstants.SPORT_ID;
    public static final String PARSE_DB_FIELD_PLACEID = ParseConstants.PLACE_ID;

    public long getDate() {
        return getLong(PARSE_DB_FIELD_DATE);
    }

    public void setLongDate(long date) {
        put(PARSE_DB_FIELD_DATE, date);
    }

    public int getSportId() {
        return getInt(PARSE_DB_FIELD_SPORTID);
    }

    public void setSportId(int id) {
        put(PARSE_DB_FIELD_SPORTID, id);
    }
    
    public ParseUser getUser(){
        return getParseUser(ParseConstants.USER_REFERENCE);

    }
    public void setUser(ParseUser parseUser){
        put(ParseConstants.USER_REFERENCE,parseUser);
    }

    public String getPlaceId() {
        return getString(PARSE_DB_FIELD_PLACEID);
    }

    public void setPlaceId(String placeId) {
        put(PARSE_DB_FIELD_PLACEID, placeId);
    }

    public void initialize(ParseUser user, int sportId, long date, String placeId){
        this.setUser(user);
        this.setSportId(sportId);
        this.setLongDate(date);
        if (placeId != null)
            setPlaceId(placeId);
    }

    public String getDateStr(Context context){

        String dateStr;
        DateUtil dateUtil = new DateUtil();
        long trainingDate = this.getDate();
        dateUtil.toMidnight();

        if (trainingDate > dateUtil.getTime().getTime()){
            //Training date = Today
            dateStr = context.getString(R.string.today);
        }else if (1 > dateUtil.getDaysSince(new Date(trainingDate))){
            //Training date = Yesterday
            dateStr = context.getString(R.string.yesterday);
        }else if (10 > dateUtil.getDaysSince(new Date(trainingDate))){
            //Training date > last 10 days. So we display: X days ago
            int daysAgo = dateUtil.getDaysSince(new Date(trainingDate)) + 1;
            dateStr = daysAgo + " " + context.getString(R.string.days_ago);
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.US);
            dateStr = "on " + sdf.format(trainingDate);
        }

        return dateStr;
    }

}
