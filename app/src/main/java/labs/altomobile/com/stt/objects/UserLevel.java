package labs.altomobile.com.stt.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.helpers.ParseHelper;

/**
 * Created by MaxenceD on 6/3/15.
 */
@ParseClassName(ParseConstants.CLASS_NAME_USERLEVEL)
public class UserLevel extends ParseObject{

    public static final int INITIAL_LEVEL = 0;
    public static final String PARSE_DB_FIELD_USERID = ParseConstants.USER_ID;
    public static final String PARSE_DB_FIELD_LEVEL = ParseConstants.LEVEL;

    public UserLevel(){}

    public UserLevel(String userId){
        setUserId(userId);
    }

    public void setUserId(String userId) {
        put(PARSE_DB_FIELD_USERID, userId);
    }
    public void setLevel(int level) {
        put(PARSE_DB_FIELD_LEVEL, level);
    }
    public int getLevel(){return getInt(PARSE_DB_FIELD_LEVEL);}

    public void UpdateLevel(){
        setLevel(getLevel()+1);
        ParseHelper.save(this);
    }
}
