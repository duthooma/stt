package labs.altomobile.com.stt.objects.firebase;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;

/**
 * Created by flavioreyes on 4/7/16.
 */
public class ChallengeFirebase implements Parcelable {

    //Reward
    public final static int NO_REWARD = -2;
    public final static int CUSTOM_REWARD = -1;
    //Result
    public final static int CHALLENGE_RESULT_PENDING = 0;
    public final static int CHALLENGE_RESULT_REFUSED = -1;
    public final static int CHALLENGE_RESULT_ACCEPTED = 1;
    public final static int CHALLENGE_RESULT_FAILED = -2;
    public final static int CHALLENGE_RESULT_EXPIRED = 2;
    public final static int CHALLENGE_RESULT_SUCCEED = 3;
    public final static long MAX_END_DATE = 120L * 24L * 3600L * 1000L;

    static long MN_IN_MILLISECONDS = 60000;
    static long HR_IN_MILLISECONDS = 3600000;
    static long DAY_IN_MILLISECONDS = 86400000;
    static long WEEK_IN_MILLISECONDS = 604800000;

    private String id = "";
    private String userFrom = "";
    private String userTo = "";
    private String name = "";
    private String desc = "";
    private String rewardComment = "";
    private int sportId = 0;
    private int rewardId = 0;
    private int result = 0;
    private long date = 0;


    public ChallengeFirebase (DataSnapshot dataSnapshot){
        id = dataSnapshot.getKey();
        userFrom = (String) dataSnapshot.child(FirebaseConstants.CHALLENGE_USER_FROM).getValue();
        userTo = (String) dataSnapshot.child(FirebaseConstants.CHALLENGE_USER_TO).getValue();
        name = (String) dataSnapshot.child(FirebaseConstants.CHALLENGE_NAME).getValue();
        desc = (String) dataSnapshot.child(FirebaseConstants.CHALLENGE_DESCRIPTION).getValue();
        rewardComment = (String) dataSnapshot.child(FirebaseConstants.CHALLENGE_REWARD_COMMENT).getValue();
        sportId = dataSnapshot.child(FirebaseConstants.SPORT_ID).getValue(Integer.class);
        rewardId =  dataSnapshot.child(FirebaseConstants.CHALLENGE_REWARD).getValue(Integer.class);
        result =  dataSnapshot.child(FirebaseConstants.CHALLENGE_RESULT).getValue(Integer.class);
        date = dataSnapshot.child(FirebaseConstants.CHALLENGE_DATE).getValue(Long.class);
    }

    public ChallengeFirebase (String userFrom, String userTo, int sportId, String name, String desc, long date, int rewardId, String rewardComment){
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.sportId = sportId;
        this.name =  name;
        this.desc = desc;
        this.date = date;
        this.rewardId = rewardId;
        this.rewardComment = rewardComment;
        this.result = CHALLENGE_RESULT_PENDING;
    }

    public ChallengeFirebase (String userFrom, String userTo, int sportId, String name, String desc, long date, int rewardId, String rewardComment, int result){
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.sportId = sportId;
        this.name =  name;
        this.desc = desc;
        this.date = date;
        this.rewardId = rewardId;
        this.rewardComment = rewardComment;
        this.result = result;
    }

    /*public void setFrom(ParseUser from){
        put(PARSEFIELD_FROM, from);
    }
    public void setTo(ParseUser to){
        put(PARSEFIELD_TO, to);
    }
    public void setSport(int sportId){put(PARSEFIELD_SPORT, sportId);}
    public void setName(String name){put(PARSEFIELD_NAME, name);}
    public void setDesc(String desc){put(PARSEFIELD_DESC, desc);}
    public void setEndDate(long endDate){put(PARSEFIELD_END_DATE, endDate);}
    public void setReward(int rewardId){put(PARSEFIELD_REWARD, rewardId);}
    public void setRewardComment(String rewardComment){put(PARSEFIELD_REWARD_COMMENT, rewardComment);}
    */
    public void setResult(int result){
        this.result = result;
    }

    public String getFrom() {
        return userFrom;
    }
    public String getTo() {
        return userTo;
    }
    public int getSport() {
        return sportId;
    }
    public String getName() {
        return name;
    }
    public String getDesc() {
        return desc;
    }

    public long getEndDate() {
        return date;
    }
    public int getReward() {
        return rewardId;
    }
    public String getRewardComment() {
        return rewardComment;
    }
    public int getResult() {
        return result;
    }

    public void initialize(String userFrom, String userTo, int sportId, String name, String desc, long date, int rewardId, String rewardComment, int result) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.sportId = sportId;
        this.name =  name;
        this.desc = desc;
        this.date = date;
        this.rewardId = rewardId;
        this.rewardComment = rewardComment;
        this.result = result;
    }

    public boolean isTheChallenged(){

        if (FirebaseHelper.getUserUid().equals(getTo()))
            return true;

        return false;
    }

    public long getTimeLeft(){
        Calendar cal = Calendar.getInstance();

        return getEndDate() - cal.getTime().getTime();
    }

    public String getTimeLeftStr(Context context){

        long timeLeft = getTimeLeft();

        long weeks = getWeeks(timeLeft);

        String timeLeftStr = null;

        if (timeLeft > 0){
            if(weeks > 0){
                //CASE: more than a week. So if more than 2 weeks, display only the number of weeks. If less than 2 weeks, display 1 week and number of days
                if (weeks == 1) {
                    timeLeft -= WEEK_IN_MILLISECONDS;
                    long days = getDays(timeLeft);
                    timeLeftStr = "1 week";
                    if (days > 0){
                        if (days == 1)
                            timeLeftStr += " " + context.getString(R.string.and_1_day);
                        else
                            timeLeftStr += " " + days + " " + context.getString(R.string.days);
                    }
                }else{
                    timeLeftStr = weeks + " weeks";
                }
            }else{
                //CASE: less than a week
                long days = getDays(timeLeft);

                if (days > 0){
                    if (days == 1) {
                        timeLeftStr = context.getString(R.string.one_day);
                        timeLeft -= DAY_IN_MILLISECONDS;
                        long hours = getHours(timeLeft);
                        if (hours > 0){
                            if (hours == 1){
                                timeLeftStr += " " +context.getString(R.string.one_hour);
                            }else{
                                timeLeftStr += " " + hours + " " + context.getString(R.string.hours);
                            }
                        }
                    }else {
                        // case: less then a week but more than a day
                        timeLeft -= days * DAY_IN_MILLISECONDS;

                        //if X days left and more than 12 hours, then display X+1
                        if (timeLeft > 12 * HR_IN_MILLISECONDS) {
                            timeLeftStr = (days+1) + " " + context.getString(R.string.days);
                        }else{
                            timeLeftStr = days + " " + context.getString(R.string.days);
                        }

                    }
                }else{
                    //CASE: less than a day
                    long hours = getHours(timeLeft);
                    if (hours > 0){
                        if (hours == 1){
                            timeLeftStr = 1 + " " +context.getString(R.string.hour);;
                            timeLeft -= HR_IN_MILLISECONDS;
                            long minutes = getMinutes(timeLeft);
                            if (minutes > 0)
                                timeLeftStr += " " + minutes + " " +context.getString(R.string.mn);
                        }else{
                            timeLeftStr = hours + " " +context.getString(R.string.hours);
                        }
                    }else{
                        //CASE: less than an hour
                        long minutes = getMinutes(timeLeft);
                        if (minutes > 30){
                            timeLeftStr = minutes + " " +context.getString(R.string.mn);
                        }else if (minutes > 20){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 30);
                        }else if (minutes > 15){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 20);
                        }else if (minutes > 10){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 15);
                        }else if (minutes > 5){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 10);
                        }else if (minutes > 2){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 5);
                        }else if (minutes > 1){
                            timeLeftStr = context.getString(R.string.less_than_X_mns, 2);
                        }else if (minutes > 0){
                            timeLeftStr = "less than 1mn";
                        }
                    }
                }
            }
            timeLeftStr += " " + context.getString(R.string.left);

        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.US);
            timeLeftStr = context.getString(R.string.expired_on) + " " + sdf.format(new Date(getEndDate()));
        }
        return timeLeftStr;
    }

    public void printForDebugPurpose(){
        Log.d(STTConstants.LOG_TAG, ChallengeFirebase.class.getSimpleName() + " :: printForDebugPurpose()");
        Log.d(STTConstants.LOG_TAG,"from :: " + getFrom());
        Log.d(STTConstants.LOG_TAG,"to :: " + getTo());
        Log.d(STTConstants.LOG_TAG,"sport :: " + getSport());
        Log.d(STTConstants.LOG_TAG,"name :: " + getName());
        Log.d(STTConstants.LOG_TAG,"desc :: " + getDesc());
        Log.d(STTConstants.LOG_TAG,"endDate :: " + getEndDate());
    }

    static long getWeeks(long timeLeft){
        if (timeLeft > WEEK_IN_MILLISECONDS){

            return timeLeft / WEEK_IN_MILLISECONDS;
        }else{
            return 0;
        }
    }

    static long getDays(long timeLeft){
        if (timeLeft > DAY_IN_MILLISECONDS){

            return timeLeft / DAY_IN_MILLISECONDS;
        }else{
            return 0;
        }
    }

    static long getHours(long timeLeft){
        if (timeLeft > HR_IN_MILLISECONDS){

            return timeLeft / HR_IN_MILLISECONDS;
        }else{
            return 0;
        }
    }

    static long getMinutes(long timeLeft){
        if (timeLeft > MN_IN_MILLISECONDS){

            return timeLeft / MN_IN_MILLISECONDS;
        }else{
            return 0;
        }
    }

    public boolean isAccepted(){
        if(getResult() == CHALLENGE_RESULT_ACCEPTED)
            return true;
        return false;
    }

    /*
    * True is the result hasn't been set yet and if the endDate hasn't expired
    * */
    public boolean isOpen(){

        if ( (getResult() == ChallengeFirebase.CHALLENGE_RESULT_PENDING || getResult() == CHALLENGE_RESULT_ACCEPTED) && !hasExpired())
            return true;

        return false;
    }

    public boolean hasExpired(){
        Calendar cal = Calendar.getInstance();

        if (getEndDate() < cal.getTime().getTime())
            return true;

        return false;
    }

    public boolean isPendingForAcceptation(){

        if (getResult() == CHALLENGE_RESULT_PENDING)
            return true;

        return false;
    }

    public Map<String, Object> getMapToSave(){
        Map<String, Object> challengeMap = new HashMap<>();
        challengeMap.put(FirebaseConstants.CHALLENGE_USER_FROM,userFrom);
        challengeMap.put(FirebaseConstants.CHALLENGE_USER_TO,userTo);
        challengeMap.put(FirebaseConstants.CHALLENGE_NAME,name);
        challengeMap.put(FirebaseConstants.CHALLENGE_DESCRIPTION,desc);
        challengeMap.put(FirebaseConstants.CHALLENGE_REWARD_COMMENT,rewardComment);
        challengeMap.put(FirebaseConstants.SPORT_ID,sportId);
        challengeMap.put(FirebaseConstants.CHALLENGE_REWARD,rewardId);
        challengeMap.put(FirebaseConstants.CHALLENGE_RESULT,result);
        challengeMap.put(FirebaseConstants.CHALLENGE_DATE,date);
        return challengeMap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userFrom);
        dest.writeString(userTo);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeString(rewardComment);
        dest.writeInt(sportId);
        dest.writeInt(rewardId);
        dest.writeInt(result);
        dest.writeLong(date);
    }

    public static final Parcelable.Creator<ChallengeFirebase> CREATOR
            = new Parcelable.Creator<ChallengeFirebase>() {
        public ChallengeFirebase createFromParcel(Parcel in) {
            return new ChallengeFirebase(in);
        }

        public ChallengeFirebase[] newArray(int size) {
            return new ChallengeFirebase[size];
        }
    };

    private ChallengeFirebase(Parcel in) {
        userFrom = in.readString();
        userTo = in.readString();
        name = in.readString();
        desc = in.readString();
        rewardComment = in.readString();
        sportId = in.readInt();
        rewardId = in.readInt();
        result = in.readInt();
        date = in.readLong();
    }
}
