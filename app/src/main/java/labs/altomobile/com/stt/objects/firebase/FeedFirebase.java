package labs.altomobile.com.stt.objects.firebase;

import com.firebase.client.Firebase;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.Map;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Training;

/**
 * Created by Flavio on 08/04/2016.
 */
public class FeedFirebase {
    public static final String REFERENCE_NAME = "referenceName";
    public static final String MODIFIED = "modified";

    public static final String CHALLENGE_REF = "challengeReference";
    public static final String TRAINING_REF = "trainingReference";


    String userId = "";
    boolean modified = false;
    String refSourceId = "";
    int activityType = -1;

    public FeedFirebase(String refSourceId,int activityType,String userId){
        this.userId = userId;
        this.refSourceId = refSourceId;
        this.activityType = activityType;
    }

    public void setModified()
    {
        modified = true;
    }

    public String getUserId()
    {
        return userId;
    }

    public int getActivityType() {
        return activityType;
    }

    public String getRelatedSourceId()
    {
        return refSourceId;
    }

    public Map<String, Object> getMapToSave(){
        Map<String, Object> userData = new HashMap<>();
        userData.put(FirebaseConstants.FEED_MODIFIED,modified);
        userData.put(FirebaseConstants.FEED_ACTIVITY_TYPE,activityType);
        userData.put(FirebaseConstants.FEED_REFERENCE_SOURCE_ID,refSourceId);
        return userData;
    }

    public void feedFriends(Firebase firebase){

    }

     
}
