package labs.altomobile.com.stt.objects.firebase;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.common.DateUtil;
import labs.altomobile.com.stt.constants.ParseConstants;

/**
 * Created by Flavio on 11/03/2016.
 */
public class TrainingFirebase {
    private long date;
    private String place;
    private int sportId;

    public TrainingFirebase(){}

    public TrainingFirebase(long date, String place,int sportId,String userUid){
        this.date = date;
        this.sportId = sportId;
        this.place = place;
    }

    public long getDate() {
        return date;
    }

    public int getSportId() {
        return sportId;
    }

    public String getPlace() {
        return place;
    }

    public String getDateStr(Context context){

        String dateStr;
        DateUtil dateUtil = new DateUtil();
        long trainingDate = this.getDate();
        dateUtil.toMidnight();

        if (trainingDate > dateUtil.getTime().getTime()){
            //Training date = Today
            dateStr = context.getString(R.string.today);
        }else if (1 > dateUtil.getDaysSince(new Date(trainingDate))){
            //Training date = Yesterday
            dateStr = context.getString(R.string.yesterday);
        }else if (10 > dateUtil.getDaysSince(new Date(trainingDate))){
            //Training date > last 10 days. So we display: X days ago
            int daysAgo = dateUtil.getDaysSince(new Date(trainingDate)) + 1;
            dateStr = daysAgo + " " + context.getString(R.string.days_ago);
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd", Locale.US);
            dateStr = "on " + sdf.format(trainingDate);
        }

        return dateStr;
    }
}
