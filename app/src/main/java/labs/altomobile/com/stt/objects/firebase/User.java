package labs.altomobile.com.stt.objects.firebase;

import android.os.Parcel;
import android.os.Parcelable;

import com.firebase.client.DataSnapshot;

import java.util.HashMap;
import java.util.Map;

import labs.altomobile.com.stt.constants.FirebaseConstants;


/**
 * Created by Flavio on 11/03/2016.
 * Class used to contains the user data. Most of the information is a copy from the Firebase /users/ childrend objects.
 */
public class User implements Parcelable {
    private String Uid;
    private String userName;
    private String facebookId;
    private String profileImageURL;

    public User() {}
    public User(String Uid,String userName,String facebookId, String profileImageURL) {
        this.Uid = Uid;
        this.userName = userName;
        this.facebookId = facebookId;
        this.profileImageURL = profileImageURL;
    }

    public String getUid(){
        return Uid;
    }

    public String getUserName() {
        return userName;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    /**
     * Create a new User object using the DataSnapshot of Firebase.
     * @param dataSnapshot An DataSnapshot instance that contains data from a Firebase location.
     */
    public User (DataSnapshot dataSnapshot){
        Uid = dataSnapshot.getRef().getParent().getKey();
        userName = dataSnapshot.child(FirebaseConstants.USER_USER_NAME).getValue(String.class);

        //if in the future we want to add other social networks, a social Id can be null
        if(dataSnapshot.hasChild(FirebaseConstants.USER_FACEBOOK_ID))
            facebookId = dataSnapshot.child(FirebaseConstants.USER_FACEBOOK_ID).getValue(String.class);

        profileImageURL = dataSnapshot.child(FirebaseConstants.USER_PROFILE_IMAGE_URL).getValue(String.class);
    }

    public Map<String, Object> getMapToSave(){
        Map<String, Object> userData = new HashMap<>();
        userData.put(FirebaseConstants.USER_USER_NAME,userName);
        userData.put(FirebaseConstants.USER_FACEBOOK_ID,facebookId);
        userData.put(FirebaseConstants.USER_PROFILE_IMAGE_URL,profileImageURL);
        return userData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Uid);
        dest.writeString(userName);
        dest.writeString(facebookId);
        dest.writeString(profileImageURL);
    }

    public static final Parcelable.Creator<User> CREATOR
            = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private User(Parcel in) {
        Uid = in.readString();
        userName = in.readString();
        facebookId = in.readString();
        profileImageURL = in.readString();
    }
}
