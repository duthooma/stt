package labs.altomobile.com.stt.objects.serializationUtils;

import com.parse.ParseGeoPoint;

import java.io.Serializable;

/**
 * Created by danielmonterocervantes on 27/03/15.
 */
public class SerializableParseGeoPoint implements Serializable {
    private static final long serialVersionUID = 1L;
    private double latitude;
    private double longitude;

    public ParseGeoPoint getParseGeoPoint() {
        return new ParseGeoPoint(latitude, longitude);
    }

    public void setParseGeoPoint(ParseGeoPoint parseGeoPoint) {
        latitude = parseGeoPoint.getLatitude();
        longitude = parseGeoPoint.getLongitude();
    }

}
