package labs.altomobile.com.stt.objects.serializationUtils;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

import labs.altomobile.com.stt.common.SerializableParseException;

/**
 * Created by danielmonterocervantes on 27/03/15.
 * Based on janakagamini / ParseProxyObject.java
 */
public class SerializableParseObject implements Serializable {
    private static final long serialVersionUID = 1L;
    private HashMap<String, Object> mValues = new HashMap<String, Object>();

    private String mObjectId;

    public void setParseObject(ParseObject parseObject) {
        mObjectId = parseObject.getObjectId();
        Set<String> keySet = parseObject.keySet();
        for (String key : keySet) {
            if (parseObject.get(key) instanceof Serializable) {
                mValues.put(key, parseObject.get(key)); // we have the work done
            } else if (parseObject.get(key).getClass() == ParseGeoPoint.class) {
                SerializableParseGeoPoint serializableParseGeoPoint = new SerializableParseGeoPoint();
                serializableParseGeoPoint.setParseGeoPoint(parseObject.getParseGeoPoint(key));
                mValues.put(key, serializableParseGeoPoint);
            } else if(parseObject.get(key) instanceof  ParseFile) {
                try {
                    //try to get the byte array from the ParseFile and set it in the values map
                    mValues.put(key, parseObject.getParseFile(key).getData());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                //int x = 0;
                //x = 0 / x;
                throw new SerializableParseException("Object not serializable supported: "+parseObject.get(key).getClass());
            }

        }
    }

    public ParseObject getParseObject(String className) {
        ParseObject parseObject;
        if (mObjectId == null) {
            parseObject = ParseObject.create(className);

        } else {
            parseObject = ParseObject.createWithoutData(className, mObjectId);

        }
        Set<String> keySet = mValues.keySet();
        for (String key : keySet) {
            if (mValues.get(key).getClass() == SerializableParseGeoPoint.class) {
                parseObject.put(key, ((SerializableParseGeoPoint) mValues.get(key)).getParseGeoPoint());
            } else if (mValues.get(key) instanceof  byte[] ) {
                //converting from byte array to ParseFile
                parseObject.put(key, new ParseFile((byte[]) mValues.get(key)));
            } else {
                parseObject.put(key, mValues.get(key));
            }
        }
        return parseObject;
    }


}
