package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.List;

import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by maxence on 5/1/15.
 */
public class GetChallengesOfCurrentUser extends ParseRequest<Challenge> {

    @Override
    protected void request(final List<Challenge> challengesList, final ParseObjectMap parseObjectMap) {

        Calendar cal = Calendar.getInstance();

        //get challenges of current User
        ParseQuery<Challenge> challengesQuery = ParseQuery.getQuery(Challenge.class);
        challengesQuery.whereEqualTo(Challenge.PARSEFIELD_TO, ParseUser.getCurrentUser());
        challengesQuery.whereGreaterThan(Challenge.PARSEFIELD_END_DATE, cal.getTime().getTime());
        challengesQuery.include(Challenge.PARSEFIELD_FROM);
        challengesList.addAll(request(challengesQuery));



    }
}
