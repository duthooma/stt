package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Comment;
import labs.altomobile.com.stt.objects.Feed;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 09/06/15.
 */
public class GetCommentsOfCurrentChallenge extends ParseRequestWithParams<Comment> {
    @Override
    protected void request(RequestParams params, List<Comment> comments, ParseObjectMap parseObjectMap) {
        Challenge challenge = (Challenge)params.get(ParseConstants.CLASS_NAME_CHALLENGE);

        ParseQuery<Comment> parseQuery = ParseQuery.getQuery(Comment.class);
        parseQuery.whereEqualTo(Feed.CHALLENGE_REF,challenge);
        parseQuery.include(ParseConstants.USER_REFERENCE);
        parseQuery.addDescendingOrder(ParseConstants.UPDATED_AT);
        parseQuery.setLimit(50);
        comments.addAll(request(parseQuery));



    }
}
