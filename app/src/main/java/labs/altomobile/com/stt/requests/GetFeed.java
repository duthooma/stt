package labs.altomobile.com.stt.requests;


import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;

import labs.altomobile.com.stt.helpers.ParseHelper;
import labs.altomobile.com.stt.objects.Challenge;
import labs.altomobile.com.stt.objects.Feed;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;


/**
 * Created by danielmonterocervantes on 29/04/15.
 */

public class GetFeed extends ParseRequest<Feed>{


    @Override
    protected void request(List<Feed> parseObjects, ParseObjectMap parseObjectMap) {
        //first get all the friends
        List<ParseUser> friends = RequestManager.GET_FRIENDS_OF_CURRENT_USER.onForeground().list;
        ArrayList<String> userIds = new ArrayList<>();
        for(ParseUser parseUser:friends)
        {
            userIds.add(parseUser.getObjectId());
            parseObjectMap.put(parseUser.getObjectId(),parseUser);
        }
        userIds.add(ParseUser.getCurrentUser().getObjectId());
        parseObjectMap.put(ParseUser.getCurrentUser().getObjectId(), ParseUser.getCurrentUser());

        //Get feed of friends
        ParseQuery<Feed> feedParseQueryOfFriends = ParseQuery.getQuery(Feed.class);
        feedParseQueryOfFriends.whereMatchesQuery(ParseConstants.USER_REFERENCE, RequestManager.GET_FRIENDS_OF_CURRENT_USER.getQuery());

        //Get feed of current user
        ParseQuery<Feed> feedParseQueryOfCurrentUser = ParseQuery.getQuery(Feed.class);
        feedParseQueryOfCurrentUser.whereEqualTo(ParseConstants.USER_REFERENCE,ParseUser.getCurrentUser());

        ArrayList<ParseQuery<Feed>> list = new ArrayList<>();
        list.add(feedParseQueryOfCurrentUser);
        list.add(feedParseQueryOfFriends);

        //get both and add order
        ParseQuery<Feed> feedParseQuery = ParseQuery.or(list);
        feedParseQuery.include(Feed.CHALLENGE_REF);
        feedParseQuery.include(Feed.CHALLENGE_REF+"."+ Challenge.PARSEFIELD_FROM);
        feedParseQuery.include(Feed.CHALLENGE_REF+"."+ Challenge.PARSEFIELD_TO);
        feedParseQuery.include(Feed.TRAINING_REF);
        feedParseQuery.include(Feed.TRAINING_REF+"."+ParseConstants.USER_REFERENCE);
        feedParseQuery.orderByDescending(ParseConstants.UPDATED_AT);
        feedParseQuery.setLimit(10);


        parseObjects.addAll(request(feedParseQuery));

    }



}
