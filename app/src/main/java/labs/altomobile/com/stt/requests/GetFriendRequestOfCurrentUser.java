package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;


/**
 * Created by Flavio on 12/05/2015.
 */
public class GetFriendRequestOfCurrentUser extends ParseRequest<FriendRequest> {

    @Override
    protected void request(final List<FriendRequest> friendRequests, final ParseObjectMap parseObjectMap) {

        ParseQuery<FriendRequest> friendRequestParseQuery = ParseQuery.getQuery(FriendRequest.class);
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_USER1, ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID));
        friendRequests.addAll(request(friendRequestParseQuery));
    }

}
