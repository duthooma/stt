package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 02/04/15.
 */
public class GetFriendsOfCurrentUser extends ParseRequest<ParseUser> {

    @Override
    protected void request(final List<ParseUser> parseUsers, final ParseObjectMap parseObjectMap) {
        parseUsers.addAll(request(getQuery()));
    }

    public  ParseQuery<ParseUser> getQuery()
    {
        ParseQuery<FriendRequest> friendRequestUser = ParseQuery.getQuery(FriendRequest.class);
        friendRequestUser.whereEqualTo(FriendRequest.PARSEFIELD_USER1, ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID));
        friendRequestUser.whereEqualTo(FriendRequest.PARSEFIELD_ACCEPTATION_STATUS, 1);

        ParseQuery<ParseUser> friendsQuery = ParseQuery.getQuery(ParseUser.class);
        friendsQuery.whereMatchesKeyInQuery(STTConstants.FACEBOOK_ID,FriendRequest.PARSEFIELD_USER2,friendRequestUser);
        return friendsQuery;

    }
}
