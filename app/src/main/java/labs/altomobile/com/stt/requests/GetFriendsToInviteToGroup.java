package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 03/04/15.
 */
public class GetFriendsToInviteToGroup extends ParseRequestWithParams<ParseUser> {


    @Override
    protected void request(RequestParams params, final List<ParseUser> parseUsers, final ParseObjectMap parseObjectMap) {

        String groupId = (String) params.get(Group.GROUP_ID);

        ParseQuery<GroupVsUser> groupVsUserParseQuery = ParseQuery.getQuery(GroupVsUser.class); //users already in the group
        groupVsUserParseQuery.whereEqualTo(Group.GROUP_ID, groupId);

        ParseQuery<FriendRequest> friendRequestParseQuery = ParseQuery.getQuery(FriendRequest.class);//friends that I send request to
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_USER1, ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID));
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_ACCEPTATION_STATUS, FriendRequest.FRIEND_REQUEST_ACCEPTED);


        ParseQuery<ParseUser> parseUserParseQuery = ParseQuery.getQuery(ParseUser.class);
        parseUserParseQuery.whereMatchesKeyInQuery(STTConstants.FACEBOOK_ID, FriendRequest.PARSEFIELD_USER2, friendRequestParseQuery);
        //parseUserParseQuery.whereMatchesKeyInQuery(ParseConstants.OBJECT_ID,PARSEFIELD_USER1,friendRequestParseQuery2);//TODO check some way to check all friends
        parseUserParseQuery.whereDoesNotMatchKeyInQuery(ParseConstants.OBJECT_ID, ParseConstants.USER_ID, groupVsUserParseQuery);

        parseUsers.addAll(request(parseUserParseQuery));

    }
}
