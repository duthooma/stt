package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 03/04/15.
 */
public class GetGroupsOfCurrentUser extends ParseRequest<Group> {

    @Override
    protected void request(final List<Group> groups, final ParseObjectMap parseObjectMap) {

        ParseQuery<GroupVsUser> groupVsUserParseQuery = ParseQuery.getQuery(GroupVsUser.class);
        groupVsUserParseQuery.whereEqualTo(ParseConstants.USER_ID, ParseUser.getCurrentUser().getObjectId());

        ParseQuery<Group> groupParseQuery = ParseQuery.getQuery(Group.class);
        groupParseQuery.whereMatchesKeyInQuery(ParseConstants.OBJECT_ID, Group.GROUP_ID, groupVsUserParseQuery);

        groups.addAll(request(groupParseQuery));

    }
}
