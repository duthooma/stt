package labs.altomobile.com.stt.requests;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.objects.FriendRequest;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 30/03/15.
 */
public class GetPendingFriendRequestOfCurrentUser extends ParseRequest<FriendRequest> {

    @Override
    protected void request(final List<FriendRequest> friendRequests, final ParseObjectMap parseObjectMap) {

        ParseQuery<FriendRequest> friendRequestParseQuery = ParseQuery.getQuery(FriendRequest.class);
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_USER2, ParseUser.getCurrentUser().getString(STTConstants.FACEBOOK_ID));
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_ACCEPTATION_STATUS, FriendRequest.FRIEND_REQUEST_PENDING);
        friendRequestParseQuery.whereEqualTo(FriendRequest.PARSEFIELD_IS_SENDER, true);
        friendRequests.addAll(request(friendRequestParseQuery));

        ParseQuery<ParseUser> friendsPendingQuery = ParseQuery.getQuery(ParseUser.class);
        friendsPendingQuery.whereMatchesKeyInQuery(STTConstants.FACEBOOK_ID, FriendRequest.PARSEFIELD_USER1, friendRequestParseQuery);
        List<ParseObject> parseUsers = customRequest(friendsPendingQuery);
        for (ParseObject user : parseUsers) {
            parseObjectMap.put(user.getString(STTConstants.FACEBOOK_ID), user);
        }
    }
}
