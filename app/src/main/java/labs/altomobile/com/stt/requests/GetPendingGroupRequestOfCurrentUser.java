package labs.altomobile.com.stt.requests;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 03/04/15.
 */
public class GetPendingGroupRequestOfCurrentUser extends ParseRequest<Group> {

    @Override
    protected void request(final List<Group> groupsList, final ParseObjectMap parseObjectMap) {

        ParseQuery<GroupVsUser> pendingGroupRequest = ParseQuery.getQuery(GroupVsUser.class);
        pendingGroupRequest.whereEqualTo(ParseConstants.USER_ID, ParseUser.getCurrentUser().getObjectId());
        pendingGroupRequest.whereEqualTo(GroupVsUser.ACCEPTATION_STATUS, Group.INVITATION_PENDING);

        ParseQuery<Group> groups = ParseQuery.getQuery(Group.class);
        groups.whereMatchesKeyInQuery(ParseConstants.OBJECT_ID, Group.GROUP_ID, pendingGroupRequest);
        groupsList.addAll(request(groups));

        List<ParseObject> usersVSGroups = customRequest(pendingGroupRequest);
        GroupVsUser groupVsUser;
        for (ParseObject uvt : usersVSGroups) {
            groupVsUser = (GroupVsUser) uvt;

            parseObjectMap.put(groupVsUser.getGroupId(), uvt);
        }

    }
}
