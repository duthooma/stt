package labs.altomobile.com.stt.requests;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Place;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
public class GetPlacesOfCurrentUser extends ParseRequest<Place> {


    @Override
    protected void request(final List<Place> places, final ParseObjectMap parseObjectMap) {
        ParseQuery<Place> query = ParseQuery.getQuery(Place.class);
        query.whereEqualTo(ParseConstants.USER_ID, ParseUser.getCurrentUser().getObjectId());
        query.addAscendingOrder(Place.NAME);
        List<Place> list = null;
        try {
            list = query.find();
            if (list != null)
                places.addAll(list);
        } catch (ParseException e) {

        }

    }
}
