package labs.altomobile.com.stt.requests;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.Training;

/**
 * Created by maxence on 4/24/15.
 */
public class GetTrainingOfMyself extends ParseRequest<Training>{

    @Override
    protected void request(final List<Training> trainings, final ParseObjectMap parseObjectMap) {

        //get trainings
        ParseQuery<Training> trainingParseQuery = ParseQuery.getQuery(Training.class);
        trainingParseQuery.whereEqualTo(ParseConstants.USER_REFERENCE, ParseUser.getCurrentUser());
        trainingParseQuery.addDescendingOrder(Training.PARSE_DB_FIELD_DATE);
        trainingParseQuery.setLimit(50);
        trainings.addAll(request(trainingParseQuery));
    }
}
