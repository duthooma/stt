package labs.altomobile.com.stt.requests;

import android.util.Log;

import com.parse.ParseQuery;

import java.util.List;

import labs.altomobile.com.stt.constants.STTConstants;
import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.ParseObjectMap;
import labs.altomobile.com.stt.objects.UserLevel;

/**
 * Created by MaxenceD on 6/4/15.
 */
public class GetUserLevelOfUser extends ParseRequestWithParams<UserLevel>{

    @Override
    protected void request(RequestParams params, List<UserLevel> userLevels, ParseObjectMap parseObjectMap) {

        String userId = (String)params.get("user_idd");

        ParseQuery<UserLevel> userLevelParseQuery = ParseQuery.getQuery(UserLevel.class);
        userLevelParseQuery.whereEqualTo(ParseConstants.USER_ID, userId);
        List<UserLevel> result = request(userLevelParseQuery);
        if(result.size() == 1){
            userLevels.add(result.get(0));
            Log.d(STTConstants.LOG_TAG, "UserLevel of User [" + userId + "] was successfully found and the level is:  " + result.get(0).getLevel());
        }else if (result.size() == 0){
            Log.w(STTConstants.LOG_TAG,"WARNING :: we found more than 0 UserLevel for the User [" + userId + "]");

        }else{
            Log.w(STTConstants.LOG_TAG,"WARNING :: we found more than 1 UserLevel for the User [" + userId + "]");
        }
    }
}