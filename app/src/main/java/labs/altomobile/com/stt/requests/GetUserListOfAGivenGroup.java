package labs.altomobile.com.stt.requests;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import labs.altomobile.com.stt.constants.ParseConstants;
import labs.altomobile.com.stt.objects.Group;
import labs.altomobile.com.stt.objects.GroupVsUser;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 03/04/15.
 */
public class GetUserListOfAGivenGroup extends ParseRequestWithParams<ParseUser> {


    @Override
    protected void request(RequestParams params, List<ParseUser> parseUsers, ParseObjectMap parseObjectMap) {

        String groupId = (String) params.get(Group.GROUP_ID);
        ParseQuery<GroupVsUser> groupVsUserParseQuery = ParseQuery.getQuery(GroupVsUser.class);
        groupVsUserParseQuery.whereEqualTo(Group.GROUP_ID, groupId);
        ParseQuery<ParseUser> userParseQuery = ParseQuery.getQuery(ParseUser.class);
        userParseQuery.whereMatchesKeyInQuery(ParseConstants.OBJECT_ID, ParseConstants.USER_ID, groupVsUserParseQuery);
        parseUsers.addAll(request(userParseQuery));

    }


}
