package labs.altomobile.com.stt.requests;

import android.os.AsyncTask;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.interfaces.OnRequestCallback;
import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 26/03/15.
 */
public abstract class ParseRequestWithParams<PObject extends ParseObject> {


    protected abstract void request(RequestParams params, final List<PObject> pObjectList, final ParseObjectMap parseObjectMap);


    public final void onBackground(RequestParams params, OnRequestCallback<PObject> callback) {
        new AsyncRequest(params, callback).execute();
    }

    protected final RequestResponse<PObject> onForeground(RequestParams params) {
        List<PObject> list = new ArrayList<>();
        ParseObjectMap parseObjectMap = new ParseObjectMap();
        request(params, list, parseObjectMap);
        return new RequestResponse<>(list, parseObjectMap);

    }

    protected List<PObject> request(ParseQuery<PObject> query) {
        List<PObject> list;
        try {
            list = query.find();
        } catch (ParseException e) {
            list = new ArrayList<>();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    protected List<ParseObject> customRequest(ParseQuery query) {
        List<ParseObject> list;
        try {
            list = query.find();
        } catch (ParseException e) {
            list = new ArrayList<>();
        }
        return list;
    }

    private class AsyncRequest extends AsyncTask<Void, Void, Void> {
        private final List<PObject> mListObjects;
        private final ParseObjectMap mParseObjectMap;
        private final RequestParams mRequestParams;
        private final OnRequestCallback<PObject> mCallback;

        public AsyncRequest(RequestParams params, OnRequestCallback<PObject> callback) {
            mListObjects = new ArrayList<>();
            mParseObjectMap = new ParseObjectMap();
            mCallback = callback;
            mRequestParams = params;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mCallback.onRequestBegin();

        }

        @Override
        protected Void doInBackground(Void... params) {
            request(mRequestParams, mListObjects, mParseObjectMap);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mCallback.onRequestFinished(mListObjects, mParseObjectMap);
        }
    }


}
