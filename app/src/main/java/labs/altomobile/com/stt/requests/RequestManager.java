package labs.altomobile.com.stt.requests;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */
public class RequestManager {

    public static final CheatsDeleteAllFriendRequestsAndAddDummies CHEATS_DELETE_ALL_FRIEND_REQUESTS_AND_ADD_DUMMIES = new CheatsDeleteAllFriendRequestsAndAddDummies();
    public static final CheatsDeleteAllChallengesAndAddDummies CHEATS_DELETE_ALL_CHALLENGES_AND_ADD_DUMMIES = new CheatsDeleteAllChallengesAndAddDummies();
    public static final GetFriendsOfCurrentUser GET_FRIENDS_OF_CURRENT_USER = new GetFriendsOfCurrentUser();
    public static final GetFriendsToInviteToGroup GET_FRIENDS_TO_INVITE_A_GROUP = new GetFriendsToInviteToGroup();
    public static final GetFriendRequestOfCurrentUser GET_FRIEND_REQUEST_OF_CURRENT_USER = new GetFriendRequestOfCurrentUser();
    public static final GetPendingFriendRequestOfCurrentUser GET_PENDING_FRIEND_REQUEST_OF_CURRENT_USER = new GetPendingFriendRequestOfCurrentUser();
    public static final GetPlacesOfCurrentUser GET_PLACES_OF_CURRENT_USER = new GetPlacesOfCurrentUser();
    public static final GetGroupsOfCurrentUser GET_GROUPS_OF_CURRENT_USER = new GetGroupsOfCurrentUser();
    public static final GetUserListOfAGivenGroup GET_USER_LIST_OF_A_GIVEN_GROUP = new GetUserListOfAGivenGroup();
    public static final GetPendingGroupRequestOfCurrentUser GET_PENDING_GROUP_REQUEST_OF_CURRENT_USER = new GetPendingGroupRequestOfCurrentUser();
    public static final GetTrainingOfMyself GET_TRAINING_OF_MYSELF = new GetTrainingOfMyself();
    public static final GetChallengesOfCurrentUser GET_CHALLENGES_OF_CURRENT_USER = new GetChallengesOfCurrentUser();
    public static final GetFeed GET_FEED = new GetFeed();
    public static final UpdateUserLevelOfUser UPDATE_USER_LEVEL_OF_USER = new UpdateUserLevelOfUser();
    public static final GetCommentsOfCurrentChallenge GET_COMMENTS_OF_CURRENT_CHALLENGE = new GetCommentsOfCurrentChallenge();
    public static final GetUserLevelOfUser GET_USER_LEVEL_OF_USER = new GetUserLevelOfUser();


    //Firebase
    //public static final GetTrainingOfMyselfFirebase GET_TRAINING_OF_MYSELF_FIREBASE = new GetTrainingOfMyselfFirebase();
}
