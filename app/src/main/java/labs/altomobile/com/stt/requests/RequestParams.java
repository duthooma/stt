package labs.altomobile.com.stt.requests;

import java.util.HashMap;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */
public class RequestParams {
    HashMap<String, Object> mMap;

    private RequestParams() {
        mMap = new HashMap<>();
    }

    public static RequestParams create() {
        return new RequestParams();
    }

    public RequestParams add(String key, Object obj) {
        mMap.put(key, obj);
        return this;
    }

    public Object get(String key) {
        if (mMap.containsKey(key))
            return mMap.get(key);
        throw new RuntimeException("This request requires param " + key + "");
    }


}
