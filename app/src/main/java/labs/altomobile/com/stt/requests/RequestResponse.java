package labs.altomobile.com.stt.requests;

import com.parse.ParseObject;

import java.util.List;

import labs.altomobile.com.stt.objects.ParseObjectMap;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */

//Just a wrapper for return types of foreground methods
public class RequestResponse<PObject extends ParseObject> {
    final public List<PObject> list;
    final public ParseObjectMap parseObjectMap;

    public RequestResponse(List<PObject> list, ParseObjectMap parseObjectMap) {
        this.list = list;
        this.parseObjectMap = parseObjectMap;
    }
}
