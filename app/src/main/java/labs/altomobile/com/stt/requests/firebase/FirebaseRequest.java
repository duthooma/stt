package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.FirebaseException;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.interfaces.OnFirebaseRequestCallback;

/**
 * Created by Flavio on 10/03/2016.
 */
public abstract class FirebaseRequest<FObject> {

    Class<FObject> mClass;
    protected OnFirebaseRequestCallback<FObject> mCallback;
    protected Firebase mFirebaseUrl = FirebaseHelper.getSharedFirebase();

    public FirebaseRequest(Class<FObject> tClass) {
        this.mClass = tClass;
    }

    protected abstract void request();

    public final void onBackground(OnFirebaseRequestCallback<FObject> callback) {
        mCallback = callback;
        request();
    }

    protected void request(Query query) {
        query.addListenerForSingleValueEvent(mEventListener);
    }

    protected void request(Firebase query) {
        query.addListenerForSingleValueEvent(mEventListener);
    }

    protected List<FObject> parseDataSnapshot(DataSnapshot dataSnapshot){
        List<FObject> list = new ArrayList<>();
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            try {
                FObject post = postSnapshot.getValue(mClass);
                list.add(post);
            }catch (FirebaseException e){
                e.printStackTrace();
            }
        }
        return list;
    }

    final ValueEventListener mEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if(mCallback != null){
                mCallback.onRequestFinished(parseDataSnapshot(dataSnapshot));
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            if(mCallback != null){
                //TODO:add error
                mCallback.onRequestFinished(new ArrayList<FObject>());
            }
        }
    };
}
