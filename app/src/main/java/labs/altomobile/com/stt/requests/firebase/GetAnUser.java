package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.objects.firebase.User;

/**
 * Created by Flavio on 09/04/2016.
 */
public class GetAnUser extends FirebaseRequest<User> {
    private Query query;
    private List<User> userList = new ArrayList<>();

    public GetAnUser(String userId) {
        super(User.class);
        query = mFirebaseUrl.child(FirebaseConstants.CHILD_USERS+"/"+ userId+"/"+FirebaseConstants.CHILD_USER_PROFILE);
    }

    @Override
    protected void request() {
        query.addListenerForSingleValueEvent(mEventListener);
    }

    final ValueEventListener mEventListener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            userList.add(new User(dataSnapshot));
            mCallback.onRequestFinished(userList);
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            mCallback.onRequestFinished(userList);
        }

    };
}
