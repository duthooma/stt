package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;

/**
 * Created by flavioreyes on 4/1/16.
 */
public class GetFriendsWithRequestsFirebase extends FirebaseRequest<String> {

    private boolean didSecondRequest = false;
    List<String> resultList = new ArrayList<>();

    public GetFriendsWithRequestsFirebase() {
        super(String.class);
    }

    @Override
    protected void request() {
        Query query = mFirebaseUrl.child(FirebaseConstants.USER_REQUEST_FROM+"/" + FirebaseHelper.getUserUid());
        query.addListenerForSingleValueEvent(mEventListener);
    }

    final ValueEventListener mEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot data:dataSnapshot.getChildren()){
                resultList.add(data.getKey());
            }
            processCallback();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
                processCallback();
        }
    };

    private void processCallback(){
        if(didSecondRequest){
            if(mCallback != null){
                mCallback.onRequestFinished(resultList);
            }
        } else {
            didSecondRequest = true;
            secondRequest();
        }
    }

    private void secondRequest() {
        Query query = mFirebaseUrl.child(FirebaseConstants.USER_REQUEST_TO+"/" + FirebaseHelper.getUserUid());
        query.addListenerForSingleValueEvent(mEventListener);
    }
}
