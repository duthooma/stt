package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.objects.firebase.ChallengeFirebase;

/**
 * Created by flavioreyes on 4/7/16.
 */
public class GetMyChallenges extends FirebaseRequest<ChallengeFirebase>{

    List<ChallengeFirebase> challengesList = new ArrayList<>();

    public GetMyChallenges() {
        super(ChallengeFirebase.class);
    }

    @Override
    protected void request() {
        Query query = mFirebaseUrl.child(FirebaseConstants.CHILD_CHALLENGES+"/"+ FirebaseHelper.getUserUid());
        query.addListenerForSingleValueEvent(mEventListener);
    }

    final ValueEventListener mEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            final long dataChildrenCount = dataSnapshot.getChildrenCount();
            if(dataChildrenCount > 0) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    challengesList.add(new ChallengeFirebase(data));
                }
            }
            mCallback.onRequestFinished(challengesList);
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            mCallback.onRequestFinished(challengesList);
        }
    };
}
