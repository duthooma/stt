package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.objects.firebase.User;

/**
 * Created by flavioreyes on 4/1/16.
 */
public class GetMyFriendsRequest extends FirebaseRequest<User> {

    private int friendsCounter = 0;
    List<User> userList = new ArrayList<>();

    public GetMyFriendsRequest() {
        super(User.class);
    }

    @Override
    protected void request() {
        Query query = mFirebaseUrl.child(FirebaseConstants.USER_REQUEST_TO+"/" + FirebaseHelper.getUserUid());
        query.addListenerForSingleValueEvent(mEventListener);
    }

    final ValueEventListener mEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            final long dataChildrenCount = dataSnapshot.getChildrenCount();
            if(dataChildrenCount > 0) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                     mFirebaseUrl.child(FirebaseConstants.CHILD_USERS+"/" + data.getKey()+"/"+FirebaseConstants.CHILD_USER_PROFILE).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                userList.add(new User(dataSnapshot));
                                friendsCounter++;
                                if (friendsCounter == dataChildrenCount) {
                                    mCallback.onRequestFinished(userList);
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                                mCallback.onRequestFinished(new ArrayList<User>());
                            }
                        });

                }
            }else {
                mCallback.onRequestFinished(new ArrayList<User>());
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            mCallback.onRequestFinished(new ArrayList<User>());
        }
    };
}