package labs.altomobile.com.stt.requests.firebase;

import com.firebase.client.Query;

import labs.altomobile.com.stt.constants.FirebaseConstants;
import labs.altomobile.com.stt.helpers.FirebaseHelper;
import labs.altomobile.com.stt.objects.firebase.TrainingFirebase;

/**
 * Created by Flavio on 10/03/2016.
 */

public class GetMyTrainingFirebase extends FirebaseRequest<TrainingFirebase> {
    public GetMyTrainingFirebase() {
        super(TrainingFirebase.class);
    }

    protected void request(){
        Query query = mFirebaseUrl.child(FirebaseConstants.CHILD_TRAININGS+"/"+FirebaseHelper.getUserUid())
                //.orderByChild("date")
                .limitToLast(50);
        request(query);
    }
}
