package labs.altomobile.com.stt.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationTrackerService extends Service implements LocationListener{

    public LocationTrackerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }

    private LocationManager getLocationManager()
    {
        return (LocationManager) getSystemService(LOCATION_SERVICE);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        getLocationManager().requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300000, 20f, this); //5 minutes 20 meters

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLocationManager().removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("STT","onLocationChanged "+location.toString());
        //TODO compare current location to a list of saved places

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
