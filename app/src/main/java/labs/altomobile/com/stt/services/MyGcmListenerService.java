package labs.altomobile.com.stt.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.LandingActivity;
import labs.altomobile.com.stt.constants.STTConstants;

/**
 * Created by flavioreyes on 6/12/15.
 */
public class MyGcmListenerService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {

        //checks the data has the basic information
        if(!data.containsKey(STTConstants.GCM_PARAMETER_MESSAGE) ||
           !data.containsKey(STTConstants.GCM_PARAMETER_TYPE))
            return;

        String message = data.getString(STTConstants.GCM_PARAMETER_MESSAGE);
        String type = data.getString(STTConstants.GCM_PARAMETER_TYPE);

        //check a notification is needed
        if(data.containsKey(STTConstants.GCM_PARAMETER_NOTIFICATION)) {

            //String showNotification = data.getString("notification");
            //if (showNotification.equals("1")) {
            String title = getString(R.string.notification_challenge_title);
            //String fromUserName = data.getString(Constants.GCM_PARAMETER_FROM_USER);
            //title += fromUserName + ".";
            sendNotification(message, title, getNotificationID(type));
            //}
        }
        showLaunchIconCount(type);

        Log.d(STTConstants.LOG_TAG, "From: " + from);
        Log.d(STTConstants.LOG_TAG, "Message: " + message);
    }

    @Override
    public void onDeletedMessages() {

    }

    @Override
    public void onMessageSent(String msgId) {

    }

    @Override
    public void onSendError(String msgId, String error) {

    }

    private void sendNotification(String message, String title, int notificationID) {
        Intent resultIntent = new Intent(this, LandingActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.ic_stat_challenge)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setDefaults(NotificationCompat.DEFAULT_LIGHTS | NotificationCompat.DEFAULT_SOUND)
                        .setContentIntent(resultPendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(notificationID, mBuilder.build());
    }

    private int getNotificationID(String typeNotification)
    {
        switch(typeNotification)
        {
            case STTConstants.NOTIF_TYPE_CHALLENGE:
                return STTConstants.NOTIFICATION_ID_CHALLENGE;
            default:
                return 0;
        }
    }

    private void showLaunchIconCount(String type)
    {
        SharedPreferences sharedpreferences = getSharedPreferences(STTConstants.FILE_NAME_NOTIFICATION_COUNT, Context.MODE_PRIVATE);
        int totalNotifications  = sharedpreferences.getInt(STTConstants.BADGET_COUNT,0);
        int amountType          = sharedpreferences.getInt(type,0);
        totalNotifications += 1;
        amountType += 1;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(STTConstants.BADGET_COUNT,totalNotifications);
        editor.putInt(type,amountType);
        editor.commit();

        //TODO: [MIGRATION_2016]
        //ShortcutBadger.with(getApplicationContext()).count(totalNotifications);
    }

}
