package labs.altomobile.com.stt.services;


import android.content.Intent;
import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by flavioreyes on 6/12/15.
 */
public class MyInstanceIDListenerService extends InstanceIDListenerService {
   @Override
    public void onTokenRefresh() {

        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
       startService(new Intent(this, RegistrationIntentService.class));
    }

}
