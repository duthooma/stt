package labs.altomobile.com.stt.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.constants.STTConstants;

/**
 * Created by flavioreyes on 6/17/15.
 */
public class RegistrationIntentService extends IntentService {


    public RegistrationIntentService() {
        super("RegistrationIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            // Initially a network call, to retrieve the token, subsequent calls are local.
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.i(STTConstants.LOG_TAG, "GCM Registration Token: " + token);

            // TODO: send any registration to my app's servers, if applicable.
            // e.g. sendRegistrationToServer(token);

            save_GCM_Id(token);

        } catch (Exception e) {
            Log.d(STTConstants.LOG_TAG, " Failed to complete token refresh", e);
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        //LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(getString(R.string.intent_name_REGISTRATION_COMPLETE)));
    }

    private void save_GCM_Id(String id)
    {
        SharedPreferences sharedpreferences = getSharedPreferences(STTConstants.FILE_NAME_GCM_CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(STTConstants.GCM_ID, id);
        editor.commit();
    }
}