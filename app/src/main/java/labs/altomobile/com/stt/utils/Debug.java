package labs.altomobile.com.stt.utils;

/**
 * Created by maxence on 12/2/15.
 */

import android.util.Log;

import labs.altomobile.com.stt.constants.STTConstants;

public class Debug {

    private static boolean isDebug = STTConstants.isLogActivated;

    public static void Log(String data) {
        if(isDebug)
            Log.d(STTConstants.LOG_TAG, data);
    }
    public static void Error(String data) {
        if(isDebug)
            Log.e(STTConstants.LOG_TAG, data);
    }

    public static void Log(String TAG, String data) {
        if(isDebug)
            Log.d(TAG, data);
    }

    public static void v(String TAG, String data) {
        if(isDebug)
            Log.v(TAG, data);
    }

    public static void v(String data) {
        if(isDebug)
            Log.v(STTConstants.LOG_TAG, data);
    }

    public static void d(String TAG, String data) {
        if(isDebug)
            Log.d(TAG, data);
    }


    public static void w(String TAG, String data) {
        if(isDebug)
            Log.w(TAG, data);
    }

    public static void e(String TAG, String data) {
        if(isDebug)
            Log.e(TAG, data);
    }

    /*

    Debug log what shouldn't be commited
     */
    public static void trash(String data) {
        if(isDebug)
            Log.e(STTConstants.LOG_TAG, data);
    }

}
