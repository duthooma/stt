package labs.altomobile.com.stt.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.parse.ParseObject;

import labs.altomobile.com.stt.interfaces.Refreshable;

/**
 * Created by danielmonterocervantes on 27/03/15.
 */
public class DeleteParseObjectDialog extends AlertDialog.Builder implements DialogInterface.OnClickListener{
    ParseObject mParseObject;
    Refreshable mRefreshable;
    public DeleteParseObjectDialog(Context context, ParseObject parseObject, Refreshable refreshable)
    {
        super(context);
        mParseObject=parseObject;
        mRefreshable =refreshable;
        setPositiveButton("Yes",this);
        setNegativeButton("No",this);
        setTitle("Delete");
        setMessage("Are you sure that you want to delete this?");
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if(which==DialogInterface.BUTTON_POSITIVE)
        {
            mParseObject.deleteInBackground();
            mRefreshable.refresh();
        }
    }
}
