package labs.altomobile.com.stt.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

/**
 * Created by flavioreyes on 5/22/15.
 */
public class ImageDownloadAsyncTask extends AsyncTask<String, Void, Bitmap> {
    ImageDownloaderCallback callback;

    public ImageDownloadAsyncTask(ImageDownloaderCallback _callback) {
        this.callback = _callback;
    }

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap mIcon = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return mIcon;
    }

    protected void onPostExecute(Bitmap result) {
        callback.setBitmapResult(result);
    }

    public interface ImageDownloaderCallback
    {
        void setBitmapResult(Bitmap bitmapResult);
    }
}
