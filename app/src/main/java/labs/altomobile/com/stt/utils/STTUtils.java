package labs.altomobile.com.stt.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import labs.altomobile.com.stt.R;
import labs.altomobile.com.stt.activities.MainMenuActivity;
import labs.altomobile.com.stt.constants.STTConstants;

/**
 * Created by maxence on 5/7/15.
 * Modified by Flavio
 */
public class STTUtils {

    public static int getResourceId(Context context, String pVariableName, String pResourcename)
    {
        try {
            return context.getResources().getIdentifier(pVariableName, pResourcename, context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /** create a thumbnail bitmap given a Uri using InputSreamt */
    public static Bitmap getBitmapFromUri(Context context, Uri uri, int bitmap_size) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        Bitmap bitmapFull = BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        return getBitmapScaled(bitmapFull, onlyBoundsOptions, bitmap_size);
    }

    /** create a thumbnail bitmap given a Uri using FileDescriptor */
    public static Bitmap getBitmapFromUriUsingDescriptor(Context context, Uri uri, int bitmap_size) throws IOException {
        if(uri == null)
            return null;
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional

        Bitmap bitmapFull = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, onlyBoundsOptions);
        parcelFileDescriptor.close();

        return getBitmapScaled(bitmapFull, onlyBoundsOptions, bitmap_size);

    }

    /** giving a bitmap and his options, return a scaled image based in the size pased  */
    private static Bitmap getBitmapScaled(Bitmap originalBitmap,BitmapFactory.Options boundsOptions, int bitmap_size) throws IOException {

        if(originalBitmap == null) return null;

        if ((boundsOptions.outWidth == -1) || (boundsOptions.outHeight == -1))
            return null;

        int originalSize = (boundsOptions.outHeight > boundsOptions.outWidth) ? boundsOptions.outHeight : boundsOptions.outWidth;
        int finalSize = (originalSize > bitmap_size) ? bitmap_size : originalSize;
        //double ratio = (originalSize > bitmap_size) ? (originalSize / bitmap_size) : 1.0;

        /*BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither=true;//optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        //input = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(inputCopy, null, bitmapOptions);
        input.close();*/

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(originalBitmap, finalSize, finalSize, false);

        /*float scaleWidth = ((float) onlyBoundsOptions.outWidth) / bitmap_size;
        float scaleHeight = ((float) onlyBoundsOptions.outHeight) / bitmap_size;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmapFull, 0, 0, bitmap_size, bitmap_size, matrix, false);*/

        return resizedBitmap;
    }

    public static Bitmap scaleBitmap(Bitmap originalBitmap, int new_size)
    {
        int originalSize = (originalBitmap.getHeight() > originalBitmap.getWidth()) ? originalBitmap.getHeight() : originalBitmap.getWidth();
        int finalSize = (originalSize > new_size) ? new_size : originalSize;
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(originalBitmap, finalSize, finalSize, false);
        return resizedBitmap;
    }

    /** Try to save a bitmap in the SD card (png format). */
    public static void saveBitmapToSD(Bitmap pictureBitmap,String fileName, String folderName) throws IOException {
        String path = Environment.getExternalStorageDirectory().toString() + folderName;
        FileOutputStream fOut = null;
        //create the directory
        boolean isPathCreated = createDirIfNotExists(path);
        if(isPathCreated) {
            File file = new File(path, fileName + ".png"); // the File to save to
            fOut = new FileOutputStream(file);

            pictureBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut); // saving the Bitmap to a file compressed as a PNG (100 for quality is ignored).
            fOut.flush();
            fOut.close(); // do not forget to close the stream
        }
    }

    /** given a bitmap, it is converted to a circle with a border */
    public static Bitmap getCircleClip(Bitmap bitmap, int borderColor, float borderWidth, Paint.Style borderStyle) {
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        Bitmap output = Bitmap.createBitmap(bitmapWidth,
                bitmapHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmapWidth, bitmapHeight);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        //get the radio using the lower size
        float radio =  bitmapHeight < bitmapWidth ? bitmapHeight / 2 : bitmapWidth / 2;
        //draw circle
        canvas.drawCircle(bitmapWidth / 2, bitmapHeight / 2,radio, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //drawBitmap behind circle
        canvas.drawBitmap(bitmap, rect, rect, paint);

        //draw circle border
        if(borderColor >= 0 && borderStyle != null && borderWidth > 0) {
            paint.setColor(borderColor);
            paint.setStrokeWidth(radio * borderWidth);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawCircle(bitmapWidth / 2, bitmapHeight / 2, radio, paint);
        }
        return output;
    }

    /** Create a Directory in case it doesn't exist */
    public static boolean createDirIfNotExists(String path) {
        boolean result = true;

        File file = new File(path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e(STTConstants.LOG_TAG, "Problem creating " + path + " directory");
                result = false;
            }
        }
        return result;
    }

    /** Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }

    public static void showSomethingWentWrongAlert(final Context context)
    {
        showSomethingWentWrongAlert(context, null);
    }

    public static void showSomethingWentWrongAlert(final Context context, String message){

        String finalMessage;
        if(message == null)
            finalMessage = context.getString(R.string.something_went_wrong);
        else
            finalMessage = message;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set dialog message
        alertDialogBuilder
                .setMessage(finalMessage)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(context,MainMenuActivity.class);
                        context.startActivity(intent);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    /** create a temporal image file */
    public static File createTempImageFile() {
        File imageFile = null;
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +"/" + STTConstants.FOLDER_IMAGES_TEMP);
        dir.mkdirs();
        if (dir.exists()) {
            try {
                imageFile = File.createTempFile(STTConstants.FILE_NAME_IMAGE_TEMP, ".jpg", dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imageFile;
    }

    public static void showKeyboard(){
        //todo : implement it ..

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) { //prevent nullpointerexception if window has no focus
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
