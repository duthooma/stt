package labs.altomobile.com.stt.utils;

/**
 * Created by danielmonterocervantes on 05/04/15.
 */
public class SportHolder {
    int sportId;
    int frequency;

    public SportHolder(int sportId, int frequency) {
        this.sportId = sportId;
        this.frequency = frequency;
    }

    public int getSportId() {
        return sportId;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}
